$(".update-btn").on('click', function(){
    const newInternId = $(this).attr("new-intern-id");
    const data = {
      arrival: $("#arrival-"+newInternId).val(),
      level: $("#level-"+newInternId).val(),
      enrolled: $("#enrolled-"+newInternId).is(':checked') ? 1 : 0,
      pre_registered: $("#pre_registered-"+newInternId).is(':checked') ? 1 : 0,
      granted: $("#granted-"+newInternId).is(':checked') ? 1 : 0,
      notified: $("#notified-"+newInternId).is(':checked') ? 1 : 0,
  }
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
      },
      method: "put",
      url: "/newIntern/"+newInternId,
      data: data
    })
    .done(function(item) {
        toastFunction("Actualizado")
    })
    .fail(function() {
      toastFunction("Hubo un error al actualizar el estado del usuario.");
    });
});