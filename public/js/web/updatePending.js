
function updateButtons(btn, id) {

    	document.getElementById("update"+id).disabled = false;
    if (btn.id === "yes" + id) {
      changeBtnColor("btn-success", "update"+id);
    	document.getElementById("yes" + id).disabled = true;
    	document.getElementById("no" + id).disabled = false;
    } else if(btn.id ==="no"+id){
      changeBtnColor("btn-danger", "update"+id);
    	document.getElementById("no" + id).disabled = true;
    	document.getElementById("yes" + id).disabled = false;
    	
    }

  }

  function changeBtnColor(color, btn) {
  	
    document.getElementById(btn).className = "";
    document.getElementById(btn).classList.add("btn");
    document.getElementById(btn).classList.add("update-btn");
    document.getElementById(btn).classList.add(color);

  }
$(".update-btn").on('click', function(){

    const prospectId = $(this).attr("prospect-id");
    var classes = $(this).attr('class').split(" ");

    var approved = classes[classes.length-1];

    var app = 0;
    var rej = 1;
    if(approved==="btn-success"){
      app = 1;
      rej = 0;
    }

    const data = {
      has_been_approved : app,
      rejected : rej
  	}



  $(this).attr("disabled", true);

      $.ajax({
        headers: {
            'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
        },
        method: "put",
        url: "prospect/"+prospectId,
        data: data
      })
      .done(function(item) {
        
          location.reload();
          toastFunction(item);
          
      })
      .fail(function() {
        toastFunction("Hubo un error al actualizar el estado del usuario.");
      });

      
});
