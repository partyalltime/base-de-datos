	
function setCalendarHeader(date){
	let end = moment.parseZone(date).format("MMM-YYYY");
	return end;
}
function capitalize(s)
{
    return s && s[0].toUpperCase() + s.slice(1);
}
async function getSchedules(){
	
	const called = await $.ajax({
		url: '/interviews',
		type: 'get',
		dataType: 'json'
	});

	return called;
}
async function getHearquarters(){
	const data =  await $.ajax({
		url: '/headquarter',
		type: 'get',
		dataType: 'json'
	});
	return data;
}
async function getSpecificScheduleList(id){
	const data = await $.ajax({
		url: `/interviews/${id}`,
		type: 'get',
		dataType: 'json'
	});
	return data;
}
function fillHeadquartersSelector(data){
	console.log(data);

	const selector = document.getElementById("headquarter");
	let option = null;
	let text = "";
	option = document.createElement('option');
	text = document.createTextNode("Seleccione una sede");
	option.appendChild(text);
	option.value = "0";
	selector.appendChild(option);
	data.forEach(element => {
		option = document.createElement('option');
		text = document.createTextNode(element.name);
		option.appendChild(text);
		option.value = element.id;
		selector.appendChild(option);
	});

}

  $(document).ready(function(){
  	
  	getHearquarters()
  	.then((response) => {
  		fillHeadquartersSelector(response);
  		//console.log("done ",response);
  	}).catch((error) => {
  		//console.log("fail ", error);
  	});
  	//Declaramos fecha local
  	moment.locale();
  	//iniciamos data time pickers
  	 $('#datetimepicker1').datetimepicker();
  	 $('#datetimepicker2').datetimepicker();
  	 //Iniciamos el calendario
    const calendar = new tui.Calendar('#calendar', {
      defaultView: 'month', // monthly view option
      useCreationPopup: false,    // whether use default detail popup or not
      useDetailPopup: false,
      
      id: 1,
      calendars: [
      {
        id: '1',
        name: 'Entrevistas',
        color: '#0081c6',
        bgColor: '#cccccc',
        dragBgColor: '#cccccc',
        borderColor: '#ffffff'
      },],
      month: {
      	daynames:[
      		"Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"
      	]
      },
      week:{
      	daynames: [
      		"Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"
      	]
      }
    });
    //Aqui asignamos la cabecera del calendario
    let cabecera =  moment(setCalendarHeader(calendar.getDate()._date));
    cabecera.locale('es')
    $("#monthDate").text(capitalize(moment(cabecera).format('MMMM - YYYY')));
    //
    calendar.on({
    	//Al hacer click en un evento aquí asignamos los values
        'clickSchedule': function(e) {
        		$('#secret').val(e.schedule.id);
            $('#datetimepicker1').val(moment.parseZone(e.schedule.start._date).format("MM/DD/YYYY h:mm A"));
            $('#datetimepicker2').val(moment.parseZone(e.schedule.end._date).format("MM/DD/YYYY h:mm A"));
            $('#user').val(e.schedule.raw)
        },
        //Esto nos ayuda a que no se seleccionen los dias de un calendario
        'beforeCreateSchedule': function(e) {
          e.guide.clearGuideElement();
        },
        'beforeUpdateSchedule': function(e) {
            const schedule = e.schedule;
            let startTime = e.start;
            let endTime = e.end;
            calendar.updateSchedule(schedule.id, schedule.calendarId, {
                start: startTime,
                end: endTime
            });
        },
        'beforeDeleteSchedule': function(e) {
            calendar.deleteSchedule(e.schedule.id, e.schedule.calendarId);
        }
    });
    //Apretamos el dia en el nombre del dia cuando estamos en modo semana
    calendar.on('clickDayname', function(event) {
      if (calendar.getViewName() === 'week') {
          calendar.setDate(new Date(event.date));
          calendar.changeView('day', true);
      }

    });

    //Declarar los Schedules
    
    //Insertamos las entrevistas desde la base de datos
    getSchedules()
  	.then((data) => {
  		let schedules = [];
  		for(let [key,value] of Object.entries(data)){
  			schedules.push({
  				id: value.id.toString(),
          calendarId: '1',
          title: value.name.toString(),
          category: 'time',
          start: value.start_date.toString(),
          end: value.end_date.toString(),
          raw: value.user_id.toString(),
          bgColor: "#CCCCCC",
          borderColor: "#CCCCCC"
  			});
  		}
  		calendar.createSchedules(schedules);
  	}).catch((err)=>{
  	});
  	
    
    
    //When clicks a schedule
    calendar.on('clickSchedule', function(event) {
      let schedule = event.schedule;
      
      if (typeof lastClickSchedule !== 'undefined' ) {
          calendar.updateSchedule(lastClickSchedule.id, lastClickSchedule.calendarId, {
              isFocused: false
          });
      }
      calendar.updateSchedule(schedule.id, schedule.calendarId, {
          isFocused: true
      });
      lastClickSchedule = schedule;
      // open detail view
    });
    calendar.on('clickMore', function(event) {
    });
   
    //Hacemos estos metodos para disparar el cambio con apretar un bonton
    $("#month").on('click',function(){
      calendar.changeView('month', true);
    });
    $("#week").on('click',function(){
      calendar.changeView('week', true);
    })  
    $("#day").on('click',function(){
      calendar.changeView('day', true);
    });
    $("#back").on('click',function(){
    	calendar.prev();
    	let cab =  moment(setCalendarHeader(calendar.getDate()._date));
    	cab.locale('es')
    	$("#monthDate").text(capitalize(moment(cab).format('MMMM - YYYY')));
    });
    $("#next").on('click',function(){
    	calendar.next();
    	let cab =  moment(setCalendarHeader(calendar.getDate()._date));
    	cab.locale('es')
    	$("#monthDate").text(capitalize(moment(cab).format('MMMM - YYYY')));
    });
    //Guardar cambio
    $("#save").on('click',function(){
    	if( $('#secret').val().length !== 0){
    		$.ajax({
	    		url: '/process/date/'+$("#secret").val(),
	    		type: 'put',
	    		 headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				  },
	    		data:{
	    			'start_date':moment($('#datetimepicker1').val()).format("YYYY-MM-DD HH:mm:ss"),
	    			'end_date': moment($('#datetimepicker2').val()).format("YYYY-MM-DD HH:mm:ss"),
	    			'user_id': $('#user').val()
	    		}
	    	}).done(function(response){
	    		toastFunction(response);
	    		calendar.clear();
					getSchedules()
			  	.then((data) => {
			  		let schedules = [];
			  		for(let [key,value] of Object.entries(data)){
			  			schedules.push({
			  				id: value.id.toString(),
			          calendarId: '1',
			          title: value.name.toString(),
			          category: 'time',
			          start: value.start_date.toString(),
			          end: value.end_date.toString(),
			          raw: value.user_id.toString(),
			          bgColor: "#CCCCCC",
			          borderColor: "#CCCCCC"
			  			});
			  		}
			  		//El segundo parametro nos permite renderizar el calendario
			  		calendar.createSchedules(schedules,false);
			  	}).catch((err)=>{
			  	});
					calendar.render();

	    	}).fail(function(err){
	    	})	
    	}
    	else{
    		toastFunction("Por favor seleccione una fecha de entrevista");
    	}
    	
    });
    $("#headquarter").on('change',function(){
	  	getSpecificScheduleList($("#headquarter").val())
	  	.then((response) => {
	  		if(response.length > 0){
	  			console.log(response);
	  			calendar.clear();
	  			let schedules = [];
	  			for(let [key,value] of Object.entries(response)){
			  			schedules.push({
			  				id: value.id.toString(),
			          calendarId: '1',
			          title: value.name.toString(),
			          category: 'time',
			          start: value.start_date.toString(),
			          end: value.end_date.toString(),
			          raw: value.user_id.toString(),
			          bgColor: "#CCCCCC",
			          borderColor: "#CCCCCC"
			  			});
			  		}
			  		console.log(schedules);
			  		calendar.createSchedules(schedules,false);
	  		}
	  		else{
	  			toastFunction("No hay entrevistas requeridas para esta sede, se mantendra la vista global");
	  		}
	  	})
	  	.catch((error) => {console.log(error)});
	  });
  });
  