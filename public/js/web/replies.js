$(document).ready(function() {
    $('#submmit-reply').click((e)=>{
        const data_to_submit = {
            body: $('#reply-body').val(),
            comment_id: $('#comment-id').val()
        }
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: "../addReply",
            method: 'post',
            data: data_to_submit,
        }).done((res)=>{
            if(res.errors)
            {
                $('.alert-danger').html('');
                $.each(res.errors, (key, value)=>{
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>'+value+'</li>');
                });
            }
            else
            {
                $('#reply-form').modal('hide');
                window.location.href=window.location.href;
            }
        }).fail((err)=>{
            $('.alert-danger').hide();
            $('#reply-form').modal('hide');
        });
    });
});