const post_id =  window.location.pathname.split('\/')[2];
const user_id = $("#userId").val();

function nextItem(id) {
    $("#slide-item-"+id).addClass("animated fadeIn slower");

    $("#slide-ref-"+id).attr('selected', 'selected');
    $("#slide-ref-"+id).attr('completed', 'completed');
    window.localStorage.setItem('slide_user'+user_id+'_post'+post_id, id);
}
function previousItem(id) {
    $("#slide-item-"+id).removeClass("animated fadeIn slower");
    //$("#slide-item-"+id).addClass("animated fadeIn slower");
    $("#slide-ref-"+id).removeAttr('selected', 'selected');
    $("#slide-ref-"+id).removeAttr('completed', 'completed');
    window.localStorage.setItem('slide_user'+user_id+'_post'+post_id, id);
}
function setLiToHash(id) {
    if(id != null){
        const slide_number = parseInt($("#slide-ref-"+id).attr("number"));
        $("#items li").each(function(i){
            console.log(parseInt($(this).attr('number')) + ", " + slide_number)
            if(parseInt($(this).attr('number'))<=slide_number){
                $(this).attr('selected', 'selected');
                $(this).attr('completed', 'completed');
            }
            else{
                $(this).removeAttr('selected', 'selected');
                $(this).removeAttr('completed', 'completed');
            }
        })
        window.localStorage.setItem('slide_user'+user_id+'_post'+post_id, id);
        location.hash = '#slide-item-'+id;
    }
}
function completeTutorial() {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
        },
        method: 'post',
        url: location.origin+"/completePostUserStatus/"+post_id
    })
    .done((response) => {
        window.location.href=location.origin + "/home";
    });
}
$(document).ready(()=>{
    const slide_id = window.localStorage.getItem('slide_user'+user_id+'_post'+post_id);
    setLiToHash(slide_id);
})