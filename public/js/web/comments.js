$(document).ready(function() {
    $('#submit-comment').click((e)=>{
        const data_to_submit = {
            body: $('#comment-body').val(),
            subject: $('#comment-subject').val(),
            post_id: $('#post-id').val()
        }
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            url: "../addComment",
            method: 'post',
            data: data_to_submit,
        }).done((res)=>{
            if(res.errors)
            {
                $('.alert-danger').html('');
                $.each(res.errors, (key, value)=>{
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>'+value+'</li>');
                });
            }
            else
            {
                $('#comment-form').modal('hide');
                window.location.href=window.location.href;
            }
        }).fail((err)=>{
            $('.alert-danger').hide();
            $('#comment-form').modal('hide');
        });
    });
});