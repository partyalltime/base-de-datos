let valor = 0;
let firstStep;
  $(document).ready(function() {


  	/*This section makes the files name visible on buttons*/
  	$('#cv').change(function() {
		  let i = $(this).prev('label').clone();

		  let file = $('#cv')[0].files[0].name;
		  $(this).next('label').text(file);
		});
		$('#grades').change(function() {
		  let i = $(this).prev('label').clone();

		  let file = $('#grades')[0].files[0].name;
		  $(this).next('label').text(file);
		});
		$('#passport').change(function() {
		  let i = $(this).prev('label').clone();

		  let file = $('#passport')[0].files[0].name;
		  $(this).next('label').text(file);
		});
		$('#identification_document').change(function() {
		  let i = $(this).prev('label').clone();

		  let file = $('#identification_document')[0].files[0].name;
		  $(this).next('label').text(file);
		});
    /* This click function was here when I got into the project
    **  I don't really know what this does, but I will not remove it
     */
    $('.not').click(function() {
      return false;
    });
    /*Time pickers set ups */
    $('#datetimepicker7').datetimepicker({
      "useCurrent": false,
      "format": "YYYY-MM-DD HH:mm:ss",
      "required": true
    });
    $('#datetimepicker8').datetimepicker({
      useCurrent: false,
      "format": "YYYY-MM-DD HH:mm:ss"
    });
    $("#datetimepicker7").on("change.datetimepicker", function(e) {
      $('#datetimepicker8').datetimepicker('minDate', e.date);
    });
    $("#datetimepicker8").on("change.datetimepicker", function(e) {
      $('#datetimepicker7').datetimepicker('maxDate', e.date);
    });

    /* Set interview date */
    $('#test').on('click', function() {

      var startDate = $('#datetimepicker7').data('date');
      var endDate = $('#datetimepicker8').data('date');

      if ((startDate != null) && (endDate != null)) {
        //send data to server
        let post = {
        	initialDate: startDate,
          finishingDate: endDate
        };
        $.ajax({
		      headers: {
		        'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
		      },
		      method: "post",
		      url: "/setDates",
		      data: post
		    })
		    .done(function(item) {
		        toastFunction("Actualizado")
		    })
		    .fail(function() {
		      toastFunction("Hubo un error al actualizar el estado del usuario.");
		    });
      } else {
        toastFunction("Error, favor seleccionar fecha inicial y final");
      }

      //This way to get val
      /*console.log(startDate);
      console.log(endDate);*/
    });

    /* Comienza sección  */
    var navListItems = $('div.setup-panel div a'),
      allWells = $('.setup-content');

    allWells.hide();
    $('.first-step').show();
    firstStep = $('.first-step');
    valor = parseInt($('.first-step').attr('id').split("-")[1]);
      //parseInt(step_id.split("-")[1])
    //console.log(valor);
    navListItems.click(function(e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
        $item = $(this);

      if (!$item.hasClass('disabled')) {
        navListItems.removeClass('btn-primary').addClass('btn-default');
        $item.addClass('btn-primary');
        allWells.hide();
        $target.show();
        $target.find('input:eq(0)').focus();
      }
    });
    

    $('div.setup-panel div a.btn-primary').trigger('click');
  });

  /* Document Ready ends*/


  

	var $container = $('.step');
  var $p = $container.find('p');
  var currentIndex = 0;

  $p.on('click', function(e) {
    var $current = $(e.currentTarget);
    var index = $p.index($current);
    if (index > currentIndex) {
      $container.addClass('forward');
    } else {
      $container.removeClass('forward');
    }
    currentIndex = index;
    $container.attr('data-step', index);
  });






function sendEmail(){
  return $.ajax({
    headers: {
      'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
    },
    method: "POST",
    url: "/sendScholarshipRequestEmail",
  });
};

function nextStep(current_step, direction){
  return $.ajax({
    headers: {
      'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
    },
    data: {
      current_step: current_step,
      direction: direction
    },
    method: "POST",
    url: "/process/nextStage",
  })
}
function goToByScroll(id){
    // Reove "link" from the ID
  id = id.replace("link", "");
    // Scroll
  $('html,body').animate({
      scrollTop: $("#"+id).offset().top},
      'medium');
}

  function allPrevBtn(curStep){
  	//console.log("prev ",curStep);
    var curStepBtn = curStep.attr("id"),
    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

    prevStepWizard.removeAttr('disabled').trigger('click');
  }
  function allNextBtn(curStep) {
  	//console.log("next ",curStep);
    var curStepBtn = curStep.attr("id"),
      nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
      curInputs = curStep.find("input[type='text'],input[type='url']"),
      isValid = true;
      //console.log(nextStepWizard, " ¿Que es esto?");
    $(".form-group").removeClass("has-error");
    for (var i = 0; i < curInputs.length; i++) {
      if (!curInputs[i].validity.valid) {
        isValid = false;
        $(curInputs[i]).closest(".form-group").addClass("has-error");
      }
    }

    if (isValid)
      nextStepWizard.removeAttr('disabled').trigger('click');

  };

  function post(path, params, method = 'post') {

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    const form = document.createElement('form');
    form.method = method;
    form.action = path;

    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = key;
        hiddenField.value = params[key];

        form.appendChild(hiddenField);
      }
    }

    const tokenField = document.createElement('input');
    tokenField.type = 'hidden';
    tokenField.name = "_token";
    tokenField.value = "{{{ csrf_token() }}}";

    form.appendChild(tokenField);

    document.body.appendChild(form);
    form.submit();
}
/*Events Starts */
$(".prevBtn").click(async function(e){
	
	console.log(valor);
  $(this).attr("disabled", "disabled");
  const step_id = $(this).closest(".setup-content").attr("id");
  const step_int = parseInt(step_id.split("-")[1]);
  await nextStep(step_int, "back")
    .done((res)=> {
      if(res.process_state==(step_int-1)){
        allPrevBtn($(this).closest(".setup-content"))
        $(this).removeAttr("disabled")
      }
      else
        alert("Something fails in server")
    })
    .fail(()=> alert("Something fails in server"));
});

$(".nextBtn").click(async function(e) { 
	
	console.log(valor);
  e.preventDefault();

  $(this).attr("disabled", "disabled");
  const step_id = $(this).closest(".setup-content").attr("id");
  
  if(step_id!=undefined){
    //const step_int = parseInt(step_id.split("-")[1]);
    const step_int = valor;
    
    if(step_int===1){
      await sendEmail()
				.done((res) => {
          
        })
        .catch((response)=>{
					
				});
    }
    await nextStep(step_int, "forward").done((res)=> {
        //console.log(res);
        if(res.process_state === (step_int+1)){
          allNextBtn(firstStep);
          firstStep =  firstStep.next();
          $(this).removeAttr("disabled");
          valor++;
        }
      }).fail((response)=> alert(response));
  }
  goToByScroll("anchor");           
});
/**
*
* Event Listener
*
*/
$("#next").click(async function(e){
	
	e.preventDefault();
	//11 Means the final step of the process, We are supposed to not get nothing after this
	if(valor < 11){
		$(this).attr("disabled", "disabled");  
		//At firts on document ready, we set a first Step as the container of the process, and we retrieve that ID
	  const step_id = firstStep.attr("id");
	 
	  if(step_id!=undefined){
	    //const step_int = parseInt(step_id.split("-")[1]);
	    //At the same time we set a value for the process state
	    const step_int = valor;
	    //If the step it's the first, we send and email
	    if(step_int===1){
	      await sendEmail()
					.done((res) => {
	          
	        })
	        .catch((response)=>{
						
					});
	    }
	    //We request the next step and we save it on database
	    await nextStep(step_int, "forward").done((res)=> {
	        //console.log(res);
	        if(res.process_state === (step_int+1)){
	          //Sending the actual DOM element of the step we request the next 
	          allNextBtn(firstStep);
	          //we Asign the next step
	          firstStep =  firstStep.next();
	          $(this).removeAttr("disabled");
	          //We add the new value to the step
	          valor++;
	          //console.log(valor, "avanzando");
	        }
	      }).fail((response)=> alert(response));
	  }	
	}
	
  goToByScroll("anchor");  

});
$("#prev").click(async function(e){
	e.preventDefault();
	
	if(valor < 11 && valor > 2){
		$(this).attr("disabled", "disabled");
	  const step_id = firstStep.attr("id");
	  //console.log(step_id, " id anterior");
	  const step_int = parseInt(valor);
	  await nextStep(step_int, "back")
	    .done((res)=> {
	      if(res.process_state==(step_int-1)){
	        allPrevBtn(firstStep);
	        firstStep = firstStep.prev();
	        //console.log("prev ", firstStep.prev());
	        $(this).removeAttr("disabled");
	        valor--;
	        //console.log(valor, "retrocediendo");
	      }
	      else
	        alert("Something fails in server")
	    })
	    .fail((response)=> alert(response));
	}
	goToByScroll("anchor");
	
});