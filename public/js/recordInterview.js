if (window.addEventListener) {
  window.addEventListener('load', function () {
    const domain = 'meet.jit.si';
    const options = {
      roomName: document.getElementById('room_id').value,
      width: '80em',
      height: '60em',
      parentNode: document.getElementById('video-conference'),
      configOverwrite: {
        enableWelcomePage: true,
        enableClosePage: true,
      },
      interfaceConfigOverwrite: {
        SHOW_JITSI_WATERMARK: false,
        SHOW_WATERMARK_FOR_GUESTS: false,
        TOOLBAR_BUTTONS: [
            'microphone', 
            'camera',
            'desktop', 
            'fullscreen',
            'fodeviceselection', 
            'chat',
            'recording',
            'videoquality',
            'tileview',
        ],
        MOBILE_APP_PROMO: false,
        DISABLE_DOMINANT_SPEAKER_INDICATOR: true,
        LANG_DETECTION: true,
      },
    };

    const api = new JitsiMeetExternalAPI(domain, options);

    api.addEventListeners({
      readyToClose: () => {
        api.dispose();
      },
      participantLeft: () => {
        api.dispose();
      },
    });
  });
};
