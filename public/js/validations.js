/*
* This function verify the character on the input
* @params event
* return boolean
*/
function isNumberInteger(evt) {
    const regExpNumbers = new RegExp("^[0-9]*$");
    let result = true;
    //console.log(evt);
    if (!regExpNumbers.test(evt.key)){
        evt.preventDefault();
        result = false;
    }
    return result;
}
//Email
function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate(val) {
  let result = document.getElementsByClassName("result");
  let email = val;
  result.textContent = "";
  
  if (validateEmail(email)) {
    
    result.textContent = email + " is valid :)";
    for (let i = 0, len = result.length; i < len; i++) {
      result[i].textContent = email + " es valido :)";
      result[i].style.color = '#00FF00';
    }
  } else {
    
    result.textContent = email + " is not valid :(";
    
    for (let i = 0, len = result.length; i < len; i++) {
      result[i].textContent = email + " no es valido :(";
      result[i].style.color = '#FF0000';
    }
    
  }
  return false;
}
//End email functions
//Elements
const numbersElements = document.querySelectorAll(".numbers-validation");
const numbersWithDecimal = document.querySelectorAll(".numbers-with-decimal");
const emailElements = document.querySelectorAll(".email-validation");

//Listeners
//Just Integer numbers
numbersElements.forEach(function(element){
  element.addEventListener("keypress", isNumberInteger, false);
});
//Float Numbers
numbersWithDecimal.forEach(function(elem) {
    elem.addEventListener("change",(event)=>{
  
    const regExpNumbers = new RegExp("a^[0-9](\.[0-9]+)?$")
    let content = event.target.value.replace(/[^.\d]/g, '').replace(/^(\d*\.?)|(\d*)\.?/g, "$1$2");

      if(!regExpNumbers.test(content)){
        event.srcElement.value = content;

      }
    });
});
//email
emailElements.forEach(function(element){
  element.addEventListener('change',(event)=>{validate(event.target.value)});
});
