const RECORD_OPTIONS = { 
  video: true,
  audio: true, 
};

let chunks = [];
let mediaRecorder;

function recordScreen() {

  navigator.mediaDevices.getDisplayMedia(RECORD_OPTIONS).then((stream) => {
    mediaRecorder = new MediaRecorder(stream);
    
    mediaRecorder.ondataavailable = (e) => {
      chunks.push(e.data);
    }

    mediaRecorder.onstop = () => {
      stream.getTracks().forEach(track => track.stop());
      let blob = new Blob(chunks, {type: 'video/webm'});
      chunks = [];
      let url = window.URL.createObjectURL(new File([blob], 'recording-lol'));
      let download = document.createElement('a');
      download.href = url;
      download.download = 'recording.webm';
      download.click();
      window.revokeObjectURL(url);
    }

    mediaRecorder.start();

  }).catch((error) => console.log('Error: ' + error));
}

function stopInterview() {
  mediaRecorder.stop();
  document.getElementById('form-interview').submit();
}

if (window.addEventListener) {
  window.addEventListener('load', recordScreen());
}