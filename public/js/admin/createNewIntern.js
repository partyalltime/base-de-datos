$("#create-btn").on('click', function(){
    const data = {
        arrival: $("#arrival").val(),
        enrolled: $("#enrolled").is(':checked') ? 1 : 0,
        pre_registered: $("#pre_registered").is(':checked') ? 1 : 0,
        level: $("#level").val(),
        granted: $("#granted").is(':checked') ? 1 : 0,
        notified: $('#notified').is(':checked') ? 1 : 0,
        internship: $("#internship").val(),
        prospect_id: $("#prospect_id").val(),
        scholarship: $("#scholarship").val()
    }
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
      },
      method: "post",
      url: "/newIntern",
      data: data
    })
    .done(function(item) {
        location.href = "/newIntern/view"
        //console.log(item);
    })
    .fail(function() {
     toastFunction("Hubo un error al actualizar el estado del usuario.");
    });
})