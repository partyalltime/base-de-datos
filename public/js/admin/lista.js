        const table = $('#tablaUsuarios').DataTable();
        const users = JSON.parse($("#usersVal").val());
        const usersByPost = [];
        let tableStateDefault = true;
        
        $("#change-view-btn").on('click', ()=>{
          if(tableStateDefault){
            $("#titleTable").text("Listado por inscritos");
            updateTableFromJson(usersByPost);
            tableStateDefault = false;
            $("#change-view-btn span").text('Mostrar por cursos');
          }
          else {
            $("#titleTable").text("Listado por cursos");
            updateTableFromJson(users);
            tableStateDefault = true;
            $("#change-view-btn span").text('Mostrar por inscritos');
          }
        })
        
        $(document).ready(function() {
          prepareSecondViewJson();
          new $.fn.dataTable.Buttons( table, {
            buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
            ]
          });
          table.buttons( 0, null ).container().prependTo(
              table.table().container()
          );
        });

        function prepareSecondViewJson(){
          let usersCopy = JSON.parse(JSON.stringify(users));
          usersCopy.forEach(x => {
            let flagToPush = true;
            for(const y of usersByPost){
              if(x.name==y.name){
                flagToPush = true;
                y.post += ", " + x.post;
                flagToPush = false;
                y.completed = `<span class="badge badge-pill badge-warning">Vista de multiples cursos</span>`;
                break;
              }
            }
            if(flagToPush)
              usersByPost.push(x);
          });
        }

        function updateTableFromJson(jsonData){
          table.clear();
          jsonData.forEach(x => {
            let row = [];
            row.push(Object.values(x));
            table.rows.add(row);
          })
          table.draw();
        }