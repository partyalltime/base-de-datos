$(document).ready(()=>{
    $('#item-body').summernote({
        placeholder: 'Hello bootstrap 4',
        tabsize: 2,
        height: 200
    });
    $('#add-item-button').click((e)=>{
        $("#submit-item").css("display", "block");
        $("#footer-modify").css("display", "none");
        $('#item-modal-title').text("Add Item");
        $('#item-body').summernote('code', "");
        $('#item-time').val("");
        $('#item-title').val("");
    });
    $('#submit-item').click((e)=>{
        const data_to_submit = {
            body: $('#item-body').summernote('code'),
            time: $('#item-time').val(),
            title: $('#item-title').val(),
            post_id: $("#post-id").val(),
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
            },
            url: location.origin + "/addOneItem",
            method: 'post',
            data: data_to_submit,
        }).done((res)=>{
            if(res.errors)
            {
                $('.alert-danger').html('');
                $.each(res.errors, (key, value)=>{
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>'+value+'</li>');
                });
            }
            else
            {
                $('.alert-danger').hide();
                $('#elementsContainer').append(`<li class="list-group-item" id="item-li-`+res.id+`">
                    <input type="hidden" class="info" id="item-info-`+res.id+`" value='`+JSON.stringify(res)+`'>
                    <h4>
                        `+res.order+`. `+res.title+`
                        <button class="btn btn-primary post-items" style="float: right" item-id="`+res.id+`">Modificar</button>
                    </h4>
                </li>`);
                $('#element-form').modal('hide');
            }
        }).fail((err)=>{
            $('.alert-danger').hide();
            $('#element-form').modal('hide');
        });
    });
    $(document).on("click", '.post-items', (e)=>{
        const item_id = $(e.target).attr("item-id");
        let item_json = JSON.parse($("#item-info-"+item_id).val());

        $("#submit-item").css("display", "none");
        $("#footer-modify").css("display", "block");
        $('#item-modal-title').text(item_json.title);
        $('#item-body').summernote('code', item_json.body);
        $('#item-time').val(item_json.time);
        $('#item-title').val(item_json.title);
        $("#item-modify-id").val(item_json.id);

        $('#element-form').modal('toggle');

    });
    $('#delete-item').click((e)=>{
        const data_to_submit = {
            id: $("#item-modify-id").val(),
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
            },
            url: location.origin + "/deleteItem",
            method: 'post',
            data: data_to_submit,
        }).done((res)=>{
            if(res==1){
                
                $('#item-li-'+data_to_submit.id).remove();
                $('#element-form').modal('hide');
            }
        }).fail((res)=>{
            alert("There was an error and the item couldn't be deleted");
            $('#element-form').modal('hide');
        });
    })
    $('#update-item').click((e)=>{
        const data_to_submit = {
            body: $('#item-body').summernote('code'),
            time: $('#item-time').val(),
            title: $('#item-title').val(),
            id: $("#item-modify-id").val(),
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
            },
            url: location.origin + "/modifyItem",
            method: 'post',
            data: data_to_submit,
        }).done((res)=>{
            
            if(res.errors) {
                $('.alert-danger').html('');
                $.each(res.errors, (key, value)=>{
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>'+value+'</li>');
                });
            }
            else {
                $('#item-li-'+res.id).empty().append(`<input type="hidden" class="info" id="item-info-`+res.id+`" value='`+JSON.stringify(res)+`'>
                    <h4>
                        `+res.order+`. `+res.title+`
                        <button class="btn btn-primary post-items" style="float: right" item-id="`+res.id+`">Modificar</button>
                    </h4>`)
                $('#element-form').modal('hide');
            }
        }).fail((res)=>{
            alert("There was an error and the item couldn't be modified");
            $('#element-form').modal('hide');
        });
    })
});