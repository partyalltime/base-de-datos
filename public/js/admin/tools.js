$( document ).ready(function() {
    $("#filtroCategorias").on("change",(e) => {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
            },
            url: location.origin + "/postByCategory/"+$("#filtroCategorias option:selected" ).val()
        })
        .done((response) => {
            $("#postsContainer").empty();
            let html = "";
            let i = 0;
            response.data.forEach((element, i) => {
                html += `<div class="col-lg-4 col-sm-12 col-md-6 d-flex">
                    <div class="tile p-0 cursos flex-fill">
                         <div class="tile-title-w-btn justify-content-end bg-primary">
     
                          <div class="btn-group">
                            <a class="btn btn-primary" href="./itemTools/` +element.id +`">
                            <i class="fa fa-lg fa-plus"></i></a>
                            <a class="btn btn-primary" href="./editItems/` +element.id +`">
                                <i class="fa fa-lg fa-edit"></i></a>
                            <a class="btn btn-primary" href="./deletePost/` +element.id +`"><i class="fa fa-lg fa-trash"></i></a>
                        </div>
                        </div>

                        <div class="card-header bg-default flex-fill">
                            <div class="card-category mb-2">`+$("select option:selected" ).text()+`</div>
                            <h3 class="title">`+element.name+`</h3>
                        </div>
                        <div class="tile-body p-4" style="overflow: hidden; text-overflow: ellipsis; max-height: 150px">
                        `+element.body+`
                        </div>
                            <div class="tile-footer p-4">
                                <div>
                                   <div class=" d-flex justify-content-between">
                                        <a class="btn btn-secondary" href="./tutorialView/` +element.id +`">Comenzar</a>
                                        <p class="text-muted text-right mt-2">Duración: ` +element.total_time +` minutos</p>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>`;
            });
            $("#postsContainer").append(html);
            $("#addPostContainer").empty().append(`<button type="button" class="btn btn-primary" style="float: right">Add Category Post</button>`);   
        })
    })
    $('#add-item-button').click((e)=>{
        $("#submit-post").css("display", "block");
        $("#footer-modify").css("display", "none");
        $('#item-modal-title').text("Add Item");
        $('#post-body').val("");
        $('#post-category').val("");
        $('#post-name').val("");
    });
    $('#submit-post').click((e)=>{
        const data_to_submit = {
            body: $('#post-body').val(),
            category_id: $('#post-category').val(),
            name: $('#post-name').val(),
        }
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
            },
            url: location.origin + "/addPost",
            method: 'post',
            data: data_to_submit,
        }).done((res)=>{
            if(res.errors)
            {
                $('.alert-danger').html('');
                $.each(res.errors, (key, value)=>{
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>'+value+'</li>');
                });
            }
            else
            {
                $('#post-form').modal('hide');
                window.location.href=window.location.href;
            }
        }).fail((err)=>{
            $('.alert-danger').hide();
            $('#post-form').modal('hide');
        });
    });
});