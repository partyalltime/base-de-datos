<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestRecord extends Model
{
    protected $table = 'test_record';
    protected $fillable = ['id_user', 'id_test','grade'];
    public $timestamps = false;
}
