<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'interview';
    protected $fillable = [
        'start_date','end_date', 'user_id'
    ];
}
