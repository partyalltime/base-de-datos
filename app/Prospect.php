<?php

namespace App;

use Auth;
use DB;
use App\User;
use App\Documents;
use Illuminate\Database\Eloquent\Model;

class Prospect extends Model
{
    protected $table = 'prospect_data';
    public $timestamps = false;

    protected $fillable = [
    	'programing_experience',
    	'informatics_experience',
    	'remarkable_experience',
    	'programing_knowledge',
    	'informatics_knowledge',
			'user_id',
			'has_been_approved',
			'rejected',
			'colaboration_scholarship',
			'residency_scholarship',
			'studies',
			'process_state',
			'other_studies',
			'studies_center',
			'average_grade',
			'other_degrees',
			'requested_scholarship',
    ];
    public function user(){
    	return $this->belongsTo('App\User');
		}
		public function newIntern(){
    	return $this->belongsTo('App\NewIntern');
    }
    public static function getActualState($id){
    	$data["prospect"] =  self::getSpecificData($id);

      $data["documents"] = self::getDocumentsFromSpecificProspect($id);
      
      return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    public static function getSpecificData($id){
      $data = DB::Table('prospect_data')
      ->select(DB::raw("users.name as 'name',
                users.identification_number as 'identification_number',
                users.address as 'address',
                users.city as 'city',
                users.postal_code as 'postal_code',
                users.phone as 'phone',
                prospect_data.studies as 'studies', 
                prospect_data.average_grade as 'average_grade', 
                prospect_data.other_degrees as 'other_degrees',
                users.birthday as 'birthday',users.country as 'country',
                users.email as 'email',
                prospect_data.process_state as 'process_state',
                 prospect_data.requested_interview as 'requested_interview', prospect_data.user_id as 'user_id', 
                 prospect_data.interview_room as 'interview_room', prospect_data.interviewed as 'interviewed', 
                  interview.start_date as 'start_date', interview.end_date as 'end_date'
                 "))
      ->leftJoin("users", "users.id", "=", "prospect_data.user_id")
      ->leftJoin("interview",'prospect_data.user_id','=','interview.user_id')
      ->leftJoin("documents", "users.id", "=", "documents.user_id")
      ->leftJoin("countries", "users.residency_id", "=", "countries.num_code")
      ->where('prospect_data.user_id','=',$id)
      ->first();
      return $data;
    }
    public static function getDocumentsFromSpecificProspect($id){
      $data = DB::Table("documents")
      ->select(DB::raw("(select name from documents where user_id = ".$id." and type like '%cv%' order by id desc limit 1) as CV,
          (select name from documents where user_id = ".$id." and type like '%grades%' order by id desc limit 1) as grades,
          (select name from documents where user_id = ".$id." and type like '%passport%' order by id desc limit 1) as passport,
          (select name from documents where user_id = ".$id." and type like '%identification_document%' order by id desc limit 1) as identification
           "))
      ->where("user_id", "=",$id)->get()->first();;
      return $data;
    }
  
}
