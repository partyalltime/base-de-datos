<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
  protected $table = 'question';
  protected $primaryKey = 'id';
  protected $fillable = ['content', 'id_test'];
  public $timestamps = false;

  public function test() {
    return $this->belongsTo('App\Test', 'id_test');
  }

  public function answers() {
    return $this->hasMany('App\Answer', 'id_question'); 
  }

  public static function boot() {
    parent::boot();

    static::deleting(function($question) {
      $question->answers()->delete();
    });
  }
}
