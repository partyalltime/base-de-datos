<?php

namespace App;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'gender', 
        'role_id', 
        'has_filled_att', 
        'postal_code', 
        'identification_id', 
        'identification_number', 
        'address', 
        'city',
        'responsible',
				'headquarters_id',
				'country'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Se pone en plural porque son muchas categorias
    public function posts() {
        return $this->hasMany(Post::class);
    }
    // Se pone en plural porque son muchas categorias
    public function roles() {
        return $this->hasMany(Roles::class);
    }

    public function isAdmin(){
        return $this->role_id == 1;
    }
    public function isManager(){
        return $this->role_id == 2;
    }
    public function isEditor(){
        return $this->role_id == 3;
    }
    public function isProspect(){
        return $this->role_id == 4;
    }
    public function isNewIntern(){
        return $this->role_id == 5;
    }
    public function isIntern(){
        return $this->role_id == 6;
    }
    public function hasBeenRequested(){
    	$approved =  false;
    	$counter = DB::table('users')
	  	->join('prospect_data','users.id', '=','prospect_data.user_id')
	  	->where('prospect_data.has_been_approved', '=', '1')
	  	->where('prospect_data.rejected', '=' ,'0')
	  	->where('prospect_data.user_id', '=', $this->id)
	  	->get();
	  	if($counter->count() > 0){
	  		$approved = true;
	  	}
	  	return $approved;
    }
}
