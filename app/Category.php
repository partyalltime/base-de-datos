<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
 
class Category extends Model
{
    use HasSlug; //Aqui es donde aplicamos el trait
    protected $fillable = [
        'name', 'slug', 'body',
   
      ];
      
       // Se pone en plural porque son muchas categorias
       public function  posts(){
          return $this->hasMany(Post::class);
      }
      
      public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
