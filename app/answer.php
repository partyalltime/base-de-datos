<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
  protected $table = 'answer';
  protected $fillable = ['content', 'goal', 'id_question'];
  public $timestamps = false;

  public function question() {
    return $this->belongsTo('App\Question');
  }
}
