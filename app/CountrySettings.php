<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountrySettings extends Model
{
    protected $table = 'country_settings';
    protected $fillable = ['country', 'academic_level_restrictions', 'academic_cycle_restrictions', 'interviewer'];
    public $timestamps = false;
    protected $casts = [
        'academic_level_restrictions' => 'array',
        'academic_cycle_restrictions' => 'array'
    ];
}
