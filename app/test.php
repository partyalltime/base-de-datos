<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Test extends Model
{
    protected $table = 'test';
    protected $fillable = ['name', 'id_post'];
    public $timestamps = false;

    public function questions() {
      return $this->hasMany('App\Question', 'id_test'); 
    }

    public static function getAllTest(){
    	$data =  DB::table('test')->get();
    	return $data;
    }
    
    public static function boot() {
      parent::boot();

      static::deleting(function($test) {
        $test->questions()->delete();
      });
    }
		
		public static function getQuizWithAnswer($testId) {
			$data =  DB::table('answer')
    	->select('answer.content as content', 
        'answer.id_question as question',
    		'answer.goal as goal', 
        'question.content as question_content',
        'answer.id as id_answer',
        'test.name as test',
        'test.id as testId')
    	->leftJoin('question','answer.id_question','=','question.id')
      ->leftJoin('test','question.id_test','=','test.id')
      ->leftJoin('posts','posts.id', '=','test.id_post')
    	->orderBy('id_question', 'asc')
    	->inRandomOrder()
      ->where('test.id','=',$testId)
    	->get();
    	$tempData = [];

    	foreach($data as $key => $value){
		    if(!isset($tempData[$value->question])){
		        $tempData[$value->question] = array(
	            'question'=>$value->question,
              'test' => $value->test,
							'question_content'=> $value->question_content,
							'test_id' => $value->testId,
	             'answers'=>array(
                  array('id_answer'=>$value->id_answer,
                  'content' => $value->content,
                  'goal' => $value->goal)
                )
		        );
		    }
		    else{
	        array_push($tempData[$value->question]['answers'],array(
						'id_answer'=>$value->id_answer,
						'goal' => $value->goal,
            'content' => $value->content
          ));    
		    }
			}
    	$data=$tempData;
    	return json_encode($data,JSON_UNESCAPED_UNICODE);
		}

    public static function getQuiz($testId){
    	$data =  DB::table('answer')
    	->select('answer.content as content', 
        'answer.id_question as question',
    		'answer.goal as goal', 
        'question.content as question_content',
        'answer.id as id_answer')
    	->leftJoin('question','answer.id_question','=','question.id')
      ->leftJoin('test', 'question.id_test','=','test.id')
    	->orderBy('id_question', 'asc')
    	->inRandomOrder()
    	->get();
    	$tempData;

    	foreach($data as $key => $value){
		    if(!isset($tempData[$value->question])){
		        $tempData[$value->question] = array(
	            'question'=>$value->question,
							'question_content'=> $value->question_content,
	             'answers' => array(
                  array(
										'id_answer'=>$value->id_answer,
										'content' => $value->content,
										'goal' => $value->goal,
									)
                )
		        );
		    }
		    else{
	        array_push($tempData[$value->question]['answers'],array(
						'id_answer'=>$value->id_answer,
						'content' => $value->content,
						'goal' => $value->goal,
          ));    
		    }
			}
    	$data=$tempData;
    	return json_encode($data,JSON_UNESCAPED_UNICODE);
    }
    public static function getAllQuiz($postId){
      $data = DB::table('test')
      ->select('name','id')
      ->where('id_post','=',$postId)
      ->get();
      return json_encode($data,JSON_UNESCAPED_UNICODE);
    }
}
