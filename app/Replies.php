<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
 
class Replies extends Model
{
    protected $fillable = [
        'user_id', 'comment_id', 'body'
    ];
   
    // Se pone en singular porque es un usuario
    public function comments()
    {
        return $this->belongsTo(Comments::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
   
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
