<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
 
class Post extends Model
{
    use HasSlug; //Aqui es donde aplicamos el trait
   protected $fillable = [
     'user_id','category_id', 'name', 'slug','excerpt','body','status','file',

   ];
   
    // Se pone en singular porque es un usuario
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    // Se pone en plural porque son muchas etiquetas
    public function  tags(){
       return $this->belongsToMany(Tag::class);
   }
   
   public function getSlugOptions() : SlugOptions
   {
       return SlugOptions::create()
           ->generateSlugsFrom('name')
           ->saveSlugsTo('slug');
   }
}
