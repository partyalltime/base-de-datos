<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasActiveProcess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user())  abort(403, 'Unauthorized action.');
        if (Auth::user()->hasBeenRequested()) {
            return $next($request);
        }
        else{
            abort(403, 'Unauthorized action.');
        }

    }
}
