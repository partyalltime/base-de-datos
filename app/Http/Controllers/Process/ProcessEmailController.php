<?php

namespace App\Http\Controllers\Process;

use Auth;
use App\Http\Controllers\Controller;
use App\ProcessDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Events\UserFinishedProcess;
use App\Prospect;
use App\Mail\AcceptScholarshipProcess;
use App\Mail\ProcessStatus;

class ProcessEmailController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendScholarshipRequestEmail()
    {
        $user = User::select("users.name as name", 
        	"responsibles.email as responsible_email", 
        	"responsibles.name as responsible_name",
        	"users.gender as gender", 
        	"users.birthday as birthday",
        	"users.academic_level as academic_level",
        	"users.academic_cycle as academic_cycle",
        	"users.email as email",
        	"users.phone as phone", 
        	"countries.en_short_name as residency")
        	->join('users as responsibles', 'responsibles.id', '=', 'users.responsible')
        	->leftJoin('countries','users.residency_id', '=', 'countries.num_code')
        	->where('users.id','=',\Auth::user()->id)
        ->first();
        Mail::to($user->responsible_email)->send(new AcceptScholarshipProcess($user));
        return response()->json($user);
    }

    public static function sendRejectionOrApproval($approved, $id){

        $decision = "";
        if($approved){
            $decision = "aprobado";
        }else{
            $decision = "denegado";
        }

        $user = User::findOrFail($id);
        $to_name = $user->name;
        $to_email = $user->email;
        $body = "Se le informa que su solicitud de inicio de proceso ha sido ".$decision.". Un saludo";

        $data = array('user' => $user, "status" => $decision);
        Mail::to($to_email)->send(new ProcessStatus($data));
        /*Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Notificación del proceso');
            $message->from('ulabs@uneatlantico.es', 'UNEACADEMY');
        });*/

    }

}
