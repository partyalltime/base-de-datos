<?php

namespace App\Http\Controllers\Process;

use Auth;
use App\Http\Controllers\Controller;
use App\ProcessDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Events\UserFinishedProcess;
use App\Prospect;
use App\Schedule;
use App\Interview;
use App\Mail\ProcessDateUpdate;
use App\Mail\ProcessDateUpdateConfirmation;


class ProcessDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if(Auth::user()->isAdmin()){
				$data = Schedule::select("interview.user_id as id", "users.name as name")
				                ->leftJoin('users','interview.user_id', '=', 'users.id')
				                ->get()->pluck('name','id');
    	}
    	else{
    		$data = Schedule::select("interview.user_id as id", "users.name as name")
                ->leftJoin('users','interview.user_id', '=', 'users.id')
                ->where('users.headquarters_id','=', Auth::user()->headquarters_id)
                ->get()->pluck('name','id');
       }
        return (Auth::user()) ? view('Process.Date')->with('prospects',$data) : abort('403');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProcessData  $processData
     * @return \Illuminate\Http\Response
     */
    public function show(ProcessData $processData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProcessData  $processData
     * @return \Illuminate\Http\Response
     */
    public function edit(ProcessData $processData)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProcessData  $processData
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
    	//I don't really know why I repeat some variables, I mean, $user and $mail['user'] It's the same.
    	//Kiss it's that you?
      $share = ProcessDate::find($id);
      $userTo = User::find($request->get('user_id'));
      $user = Auth::user();
      $share->start_date = $request->get('start_date');
      $share->end_date = $request->get('end_date');
      $share->user_id = $request->get('user_id');
      $message = "Felicidades";
      if($share->save()){
		    //create a new invite record
		    $mail['userTo'] = ProcessDate::select("interview.start_date as start_date", "users.name as name")
            ->leftjoin("users", "interview.user_id", "=", "users.id")
            ->where("interview.id",'=',$id)
            ->first();
		    $mail['user'] = $user;

		    // send the email
		    if(Mail::to($userTo->email)->bcc($user->email)->send(new ProcessDateUpdate($mail['userTo']))){
      		
      		$message = "Felicidades";
		    }
		    
      }else{
      	$message = 'No fue posible';
      }
      return $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProcessData  $processData
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProcessData $processData)
    {
        //
    }

    public function triggerEmail(Request $request)
    {
        $user = User::find(\Auth::user()->id);

        event(new UserFinishedProcess($user));

        return redirect("/process");
    }

    public function setInterviewInterval(Request $request){
        //This will be the fields to insert or update
        $initialDate = $request->get('initialDate');
        $finishingDate = $request->get('finishingDate');
        $user = \Auth::user()->id;

        //Check if the specified user has already set an interval for interview
        $interview = Interview::where('user_id', $user)->first();

        //If query returns null, it means that the user has not set an interval, thus create a new record
        if($interview == null){
            $interview = new Interview;
        }

        //Setting the fields for the new or updated record
        $interview->start_date = $initialDate;
        $interview->end_date = $finishingDate;
        $interview->user_id = $user;


        if($interview->save()){
        	$message = 'Done';
        }
        else{
        	$message = 'Fail';
        }
        return $message;
    }
}
