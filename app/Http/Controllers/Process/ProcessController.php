<?php

namespace App\Http\Controllers\Process;

use Auth;
use App\Http\Controllers\Controller;
use App\ProcessDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Events\UserFinishedProcess;
use App\Prospect;
use App\Schedule;
use App\Interview;
use PDF;
Use DB;


class ProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prospect = Prospect::where('prospect_data.user_id', \Auth::user()->id)
        ->select(DB::raw("users.name as 'name',
                users.identification_number as 'identification_number',
                users.address as 'address',
                users.city as 'city',
                users.postal_code as 'postal_code',
                users.phone as 'phone',
                prospect_data.studies as 'studies', 
                prospect_data.average_grade as 'average_grade', 
                prospect_data.other_degrees as 'other_degrees',
                users.birthday as 'birthday',users.country as 'country',
                users.email as 'email',
                (case when 
                  (select count(*) from documents where user_id  = ".\Auth::user()->id." ) > 0 then 
                    (select ( case when name is not null then 
                      name 
                    else 'Ingrese un documento' end) from documents where documents.type like '%cv%' order by id desc limit 1 ) 
                  else 'Ingrese un documento' end) as 'CV',
                (case when 
                  (select count(*) from documents where user_id  = ".\Auth::user()->id." ) > 0 then 
                    (select ( case when name is not null then 
                      name 
                    else 'Ingrese un documento' end) from documents where documents.type like '%grades%' limit 1 ) 
                  else 'ingrese un documento' end) as 'grades',
                (case when 
                  (select count(*) from documents where user_id  = ".\Auth::user()->id." ) > 0 then 
                    (select ( case when name is not null then 
                      name 
                    else 'Ingrese un documento' end) from documents where documents.type like '%identification_document%' limit 1) 
                  else 'Ingrese un documento' end) as 'identification_document',
                (case when 
                  (select count(*) from documents where user_id  = ".\Auth::user()->id." ) > 0 then 
                    (select ( case when name is not null then 
                      name 
                    else 'Ingrese un documento' end) from documents where documents.type like '%passport%' ORDER by 'desc' limit 1) 
                  else 'Ingrese un documento' end) as 'passport',
                prospect_data.process_state as 'process_state',
                 prospect_data.requested_interview as 'requested_interview', prospect_data.user_id as 'user_id', 
                 prospect_data.interview_room as 'interview_room'"))
        ->leftJoin("users", "users.id", "=", "prospect_data.user_id")
        ->leftJoin("documents", "users.id", "=", "documents.user_id")
        ->leftJoin("countries", "users.residency_id", "=", "countries.num_code")
        ->first();
        $_i = 0;
        $type = null;
        $data = json_decode(Prospect::getActualState(\Auth::user()->id));
        return view('Process.Process', compact('prospect', '_i','type','data'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Process  $Process
     * @return \Illuminate\Http\Response
     */
    public function show(Process $Process)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Process  $Process
     * @return \Illuminate\Http\Response
     */
    public function edit(Process $Process)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Process  $Process
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Process  $Process
     * @return \Illuminate\Http\Response
     */
    public function destroy(Process $Process)
    {
        //
    }

    public function triggerEmail(Request $request)
    {
        $user = User::find(\Auth::user()->id);

        event(new UserFinishedProcess($user));

        return redirect("/process");
    }

    public function requestInterview(Request $request) {
        $user_id = $request->get('user_id');
        Prospect::where('user_id', $user_id)->update(['requested_interview' => 1]);

        return redirect()->back();
    }

    public function nextProcessStage(Request $request)
    {
        $prospect = Prospect::where('user_id', \Auth::user()->id)->first();
        if($prospect->process_state == $request->current_step){
            if($request->direction == "forward")
                $prospect->process_state = $prospect->process_state + 1;
            else if($request->direction == "back")
                $prospect->process_state = $prospect->process_state - 1;
            else
                return "unknown direction";
        }
        else
            return "El proceso solicitado no coincide con el proceso en la base de datos";
            
        $prospect->save();
        return response()->json($prospect);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function printSignUpForm(){
    	$data = ['title' => 'Welcome to HDTuto.com'];
      $pdf = PDF::loadView('Process.print.PrintSignUpForm', $data);
      return $pdf->download('FormularioDeIngreso.pdf');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function printResidencyForm($id) {
    	$prospect = Prospect::where('user_id', $id)
        ->join("users", "users.id", "=", "prospect_data.user_id")
        ->join("countries", "users.residency_id","=","countries.num_code")->first();
        return response()->json($prospect);
    }

    public function startProcess() {
        $user_id = Auth::user()->id;
        $prospect = Prospect::where('user_id', "$user_id")->first();

        if ($prospect->requested_scholarship === 0) {
            $prospect->requested_scholarship = 1;
            $prospect->save();
        }

        return redirect('/individualInfo/'.Auth::user()->id);
    }
}
