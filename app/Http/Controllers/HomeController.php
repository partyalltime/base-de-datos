<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Post;
use App\Items;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        // $posts= Post::leftJoin('users_posts', 'posts.id', '=', 'users_posts.post_id')->where('users_posts.user_id', Auth::getUser()->id)->orderBy('posts.created_at','ASC')->get();
        $posts= Post::select("posts.*", "users_posts.user_id", "users_posts.completed")
        ->leftJoin('users_posts', function($join){
            $join->on('posts.id', '=', 'users_posts.post_id')
            ->on('users_posts.user_id', '=', DB::raw(Auth::getUser()->id));
        })
        ->orderBy('posts.created_at','ASC')
        ->get();
        foreach ($posts as $key=>$elem) {
            $items_time = Items::where("post_id", $elem->id)->sum('time');
            $posts[$key]["total_time"] = $items_time;
        }
        return view('home',compact('categories', 'posts'));
    }
    public function addPostCourses($id)
    {   
        $users_posts= array(
            'post_id' => $id,
            'user_id' => Auth::getUser()->id,
            'relationship' => 'subscriber'
        );
        $users_posts_relation = DB::table('users_posts')->insert($users_posts);
        return redirect('/home');
    }
    public function minusPostCourses($id)
    {
        $users_posts = DB::table('users_posts')->where(['post_id' => $id, 'user_id' => Auth::getUser()->id])->delete();
        return redirect('/home');
    }
}
