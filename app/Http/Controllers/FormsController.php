<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prospect;
use app\User;

class FormsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function fillData(Request $request){
        $current_user = \Auth::user()->id;
        $user = User::find($current_user);
        $prospect_data = Prospect::where('user_id',$current_user)->first();

        if(isset($request->name)){
            $user->name = $request->name;
        }

        if(isset($request->birthdayDate)){
            //$user->birthday = $request->birthdayDate;
        }

        if(isset($request->email)){
            $user->email = $request->email;
        }

        if(isset($request->phone)){
            $user->phone = $request->phone;
        }

        $user->identification_id = $request->identification_type;
        $user->identification_number = $request->identification_number;
        $user->address = $request->address;
        $user->city = $request->place;
        $user->postal_code = $request->cp;
        $user->phone = $request->phone;
        $user->country = $request->country;
        $user->save();

        $prospect_data->studies = $request->superior;
        $prospect_data->other_studies = $request->other;
        $prospect_data->studies_center = $request->places_studies;

        if($request->collaboration_scholarship){
            $prospect_data->collaboration_scholarship = true;
        } else if($request->residency_scholarship){
            $prospect_data->residency_scholarship = true;
        }

        $prospect_data->average_grade = $request->obtained_grade;
        $prospect_data->other_degrees = $request->other_studies;

        $prospect_data->save();

        return redirect('process');


        
    }

}
