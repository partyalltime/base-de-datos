<?php

namespace App\Http\Controllers\Test;

use App\TestRecord;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = "";
        $data = $request->all();
        $userId = $data['id_user'];
        $testId = $data['id_test'];
        $grade = $data['grade'];
        $result = TestRecord::create(['id_user' => $userId,
          'id_test' =>$testId,
          'grade' => $grade
        ]);
        if($result){
          $message = "Guardado con exito";
        }
        else{
          $message = "Error";
        }
        return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TestRecord  $testRecord
     * @return \Illuminate\Http\Response
     */
    public function show(TestRecord $testRecord)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TestRecord  $testRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(TestRecord $testRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TestRecord  $testRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TestRecord $testRecord)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TestRecord  $testRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(TestRecord $testRecord)
    {
        //
    }
}
