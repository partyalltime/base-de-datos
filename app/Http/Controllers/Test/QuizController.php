<?php

namespace App\Http\Controllers\Test;

use App\Test;
use App\Question;
use App\Answer;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Quiz/Quiz');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if (Auth::user()->isAdmin()) {
        $data = $request->all();
        $postId = $data['post_id'];
        $testName = $data['test_name'];
        $test = $data['test'];

        $newTest = Test::create([
          'name' => $testName,
          'id_post' => $postId,
        ]);

        for ($x = 0; $x < count($test); $x++) {
          $newQuestion = Question::create([
            'id_test' => $newTest->id,
            'content' => $test[$x]['content'],
          ]);

          for ($y = 0; $y < count($test[$x]['answers']); $y++) {
            Answer::create([
              'content' => $test[$x]['answers'][$y]['content'],
              'goal' => $test[$x]['answers'][$y]['goal'],
              'id_question' => $newQuestion->id,
            ]);
          }
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show($idtest)
    {
      return view('Quiz/Quiz',["test"=>$idtest]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function showAnswers($idtest)
    {
        return Test::getQuizWithAnswer($idtest);
        //return "hey";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $test)
    {
      Test::where('id', $test)->update([
        'name' => $request->name,
      ]);

      $exam = $request->test;

      for ($x = 0; $x < count($exam); $x++) {
        $question;
        $question_updated = false;

        if (is_string($exam[$x]['id'])) {
          $question = Question::create([
            'id_test' => $test,
            'content' => $exam[$x]['content'],
          ]);
        } else {
          $question = Question::where('id', $exam[$x]['id'])->first()->update([
            'content' => $exam[$x]['content'],
          ]);

          $question_updated = true;
        }

        $question_id;

        if ($question_updated) {
          $question_id = $exam[$x]['id'];

        } else {
          $question_id = $question->id;

        }

        for ($y = 0; $y < count($exam[$x]['answers']); $y++) {
          if (!array_key_exists('id', $exam[$x]['answers'][$y])) {
            Answer::create([
              'content' => $exam[$x]['answers'][$y]['content'],
              'goal' => $exam[$x]['answers'][$y]['goal'],
              'id_question' => $question_id,
            ]); 
          } else {
            Answer::where('id', $exam[$x]['answers'][$y]['id'])->first()->update([
              'content' => $exam[$x]['answers'][$y]['content'],
              'goal' => $exam[$x]['answers'][$y]['goal'],
            ]);
          }
        }
      }

      return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy($test)
    {
      $test = Test::where('id', $test)->first()->delete();

      return redirect()->back()->setStatusCode(200);
    }
    /**
     * Display the specified quiz .
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function showQuiz($id)
    {
        return Test::where('id', $id)->with(['questions', 'questions.answers'])->first();
    }

    public function getResultOfQuiz($id) {
        $test = Test::getQuizWithAnswer($id);
        return $test;
    }
    public function getAllQuiz($id){
      return view('Quiz/ChooseQuiz',["post"=>$id]);
    }
    public function getDataQuizes($idPost){
      return Test::getAllQuiz($idPost);
    }
}
