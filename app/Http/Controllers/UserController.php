<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\NotificationsController;
use Auth;
use App\User;
use App\Prospect;
use App\NewIntern;
use App\Companies;
use App\Countries;
use App\CountrySettings;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function create(Request $request) {
      if (Auth::user()->isAdmin()) {
        User::create([
          'name' => $request->name,
          'email' => $request->email,
          'headquarters_id' => $request->headquarter,
          'role_id' => $request->rol,
          'password' => Hash::make(str_random(8)),
        ]);

        return redirect('users/admin');
      }
		}
		
		public function adminUpdate(Request $request, $id) {
      if (Auth::user()->isAdmin()) {
        User::where('id', $id)->first()->update([
          'name' => $request->name,
          'email' => $request->email,
          'role_id' => $request->rol,
          'headquarters_id' => $request->headquarter,
        ]);

        return redirect('users/admin');
      }
    }
    
    public function update(Request $request){

        $current_user = User::find(\Auth::user()->id);
        //dd($request->pais);

        $country_id = Countries::select("countries.num_code")->where('alpha_3_code', $request->pais)->first();

        $current_user->birthday = date("Y-m-d", strtotime($request->birthdayDate));
        $current_user->phone = $request->phoneNumber;
		$current_user->residency_id = $country_id->num_code;
		$current_user->academic_level = $request->acdmc_lvl;
		$current_user->academic_cycle = $request->acdmc_cycle;
		if ((isset($request->Experiencia_de_programación))&&(isset($request->Experiencia_informática))&&(isset($request->Experiencias_reseñables))&&(isset($request->Conocimiento_de_programación))&&(isset($request->Conocimientos_informáticos))){
			$current_user->has_filled_att = 1;
		}
        $current_user->save();
		$current_prospect = Prospect::where('user_id', \Auth::user()->id)->first();
		
        if(isset($request->Experiencia_de_programación)){
        	$current_prospect->programing_experience = $request->Experiencia_de_programación;
        }
        if(isset($request->Experiencia_informática)){
        	$current_prospect->informatics_experience = $request->Experiencia_informática;
        }
        if(isset($request->Experiencias_reseñables)){
        	$current_prospect->remarkable_experience = $request->Experiencias_reseñables;
        }
        if(isset($request->Conocimiento_de_programación)){
        	$current_prospect->programing_knowledge = $request->Conocimiento_de_programación;
        }
        if(isset($request->Conocimientos_informáticos)){
        	$current_prospect->informatics_knowledge = $request->Conocimientos_informáticos;
		}
		
        if(isset($current_prospect)){
        	$current_prospect->save();
        }

        if(\Auth::user()->role_id == 5){
            $current_intern = NewIntern::where('prospect_id', 4)->first();
            $current_intern->arrival = $request->Fecha_de_llegada;
            $company = Companies::where('name', "Tsardom of Russia")->first();
            $current_intern->internship = $company['id'];
            
            $current_intern->save();
				}
				
				echo($request->acdmc_lvl);
        return redirect('individualInfo/'.User::find(\Auth::user()->id)->id)->with('shit', $request->acdmc_lvl, 'status', 'Profile updated!');
    }

    public function userData(Request $request, $id){
			$info["id"] = $id;
			$country_settings = [];
        if(Auth::user()){
	        $user = User::select("users.name as name","users.email as email",
	                "users.role_id as role_id", "users.phone as phone", "users.academic_level as academic_level",
	                "users.academic_cycle as academic_cycle","users.image_url as image_url", "users.birthday as birthday",
	                "users.gender as gender", "countries.num_code as country_id", "countries.en_short_name as residency", "countries.alpha_3_code as code")
									->join('roles', 'users.role_id', '=', 'roles.id')
									->leftJoin('countries','users.residency_id', '=', 'countries.num_code')
	                ->where('users.id','=',\Auth::user()->id)
	                ->get();

	        if($user[0]["role_id"] == 4){
	            $user_attributes = Prospect::where('user_id', \Auth::user()->id)->first();
	            $info['user_data'] = array(
	                "Experiencia de programación" => $user_attributes['programing_experience'],
	                "Experiencia informática" => $user_attributes['informatics_experience'],
	                "Experiencias reseñables" => $user_attributes['remarkable_experience'],
	                "Conocimiento de programación" => $user_attributes['programing_knowledge'],
	                "Conocimientos informáticos" => $user_attributes['informatics_knowledge'],
	                // "Teléfono" => $user[0]["phone"],
	                // "Cumpleaños" => $user[0]["birthday"]
	      
	            );
	        } else if($user[0]["role_id"] == 5){
	            $prospect_data = Prospect::where('user_id', \Auth::user()->id)-> first();
	            $user_attributes = NewIntern::where('prospect_id', $prospect_data['id'])->join('companies', 'companies.id', '=', 'new_intern_data.internship')->first();
	            $info['user_data'] = array(
	                "Experiencia de programación" => $prospect_data->programing_experience,
	                "Experiencia informática" => $prospect_data['informatics_experience'],
					        "Experiencias reseñables" => $prospect_data['remarkable_experience'],
	                "Conocimiento de programación" => $prospect_data['programing_knowledge'],
	                "Conocimientos informáticos" => $prospect_data['informatics_knowledge'],
	                "Fecha de llegada" => $user_attributes['arrival'],
	                "Pre-inscrito" => ($user_attributes['enrolled'] == 1) ? 'Si': 'No',
	                "Pre-registrado" => ($user_attributes['pre_registered'] == 1) ? 'Si': 'No',
	                "Nivel" => $user_attributes['level'],
	                "Otorgado" => $user_attributes['granted'],
	                "Notificado" => $user_attributes['notified'],
	                "Prácticas" => $user_attributes['name'],
	                // "Teléfono" => $user[0]["phone"],
	                // "Cumpleaños" => $user[0]["birthday"]
	            );
	        }

	        $info['user'] = $user;

	        $countriesDB = Countries::select("countries.alpha_3_code", "countries.en_short_name")->get();

					$countries = array();
				
					$country_settings = \App\CountrySettings::where('country', $info['user'][0]['country_id'])->first();

					$info['country_settings'] = $country_settings;

	        for($i = 0; $i < count($countriesDB); $i++){
	            $countries[$countriesDB[$i]["alpha_3_code"]] = $countriesDB[$i]["en_short_name"];
	        }

	        $info['countries'] = $countries;
	        //dd($countries[248]["alpha_3_code"]);
		   }

        return (Auth::user()) ? view('editProfile')->with('info', json_decode(json_encode($info,JSON_UNESCAPED_UNICODE),true)) : abort('403');
    }

    public function destroy($id) {
      if (Auth::user()->isAdmin()) {
        User::find($id)->delete();

        return redirect()->back();
      }
      
      return redirect()->back();
    }
}
