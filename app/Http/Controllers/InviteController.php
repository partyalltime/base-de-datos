<?php

namespace App\Http\Controllers;

use App\Invite;
use App\Mail\InviteCreated;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class InviteController extends Controller
{
  // show the user a form with an email field to invite a new user
  public function invite()
  {
     return view('auth.invite');
  }

  //CHANGE THIS FUNCTION TO ANOTHER CONTROLLER
  //IT DEFINITELY DOESN'T GOES HERE
  public function list()
  {
    $users = DB::table('users as usrs')
      ->select('usrs.name as name', 'usrs.email', 'countries.en_short_name as residency', 'posts.name as post', 'users_posts.completed', 'users_posts.relationship', 'responsibles.name as responsible')
      ->join('users as responsibles', 'usrs.responsible', '=', 'responsibles.id')
      ->join('users_posts', 'usrs.id', '=', 'users_posts.user_id')
      ->join('posts', 'users_posts.post_id', '=', 'posts.id')
      ->leftJoin('countries', 'usrs.residency_id', '=', 'countries.num_code')
      ->where('users_posts.relationship','=','subscriber')
      ->where('usrs.role_id','>',1)
      ->where('usrs.headquarters_id','=', Auth::user()->headquarters_id)
      ->orderBy('usrs.name','ASC')
      ->get();

    return view('admin.lista', compact('users'));
  }

  public function listByResponsible() {
    if (Auth::user()->isAdmin()) {
      $users = DB::table('users as usrs')
        ->select('usrs.name as name', 'usrs.email', 
                  'countries.en_short_name as residency', 
                  'responsibles.name as responsible',
                  'prospect_data.process_state as proccess',
                  'prospect_data.user_id as data'
                )
        ->join('users as responsibles', 'usrs.responsible', '=', 'responsibles.id')
        ->leftJoin('countries', 'usrs.residency_id', '=', 'countries.num_code')
        ->leftJoin('prospect_data', 'usrs.id' ,'=','prospect_data.user_id')
        ->where('usrs.role_id','>',1)
        ->orderBy('usrs.name','ASC')
        ->get();

      $headquarters = \App\Headquarter::get();

      return view('admin.ListByResponsible', compact(['users', 'headquarters']));
    } else {
      $users = DB::table('users as usrs')
        ->select('usrs.name as name', 'usrs.email', 
                  'countries.en_short_name as residency', 
                  'responsibles.name as responsible',
                  'prospect_data.process_state as proccess',
                  'prospect_data.user_id as data'
                )
        ->join('users as responsibles', 'usrs.responsible', '=', 'responsibles.id')
        ->leftJoin('countries', 'usrs.residency_id', '=', 'countries.num_code')
        ->leftJoin('prospect_data', 'usrs.id' ,'=','prospect_data.user_id')
        ->where('usrs.role_id','>',1)
        ->where('usrs.responsible','=', Auth::user()->id)
        ->where('usrs.headquarters_id', '=', Auth::user()->headquarters_id)
        ->orderBy('usrs.name','ASC')
        ->get();

      $headquarters = null;

      return view('admin.ListByResponsible', compact(['users', 'headquarters']));
    }
  }

  // process the form submission and send the invite by email
  public function process(Request $request)
  {
    // validate the incoming request data
    do {
      //generate a random string using Laravel's str_random helper
      $token = Str::random(20);
    } //check if the token already exists and if it does, try again
    while (Invite::where('token', $token)->first());

    //create a new invite record
    $invite = Invite::create([
      'email' => $request->get('email'),
      'token' => $token,
      'host' => \Auth::user()->id
    ]);

    // send the email
    Mail::to($request->get('email'))->send(new InviteCreated($invite));

    // redirect back where we came from
    return redirect()->back();
  }

  // here we'll look up the user by the token sent provided in the URL
  public function accept($token)
  {
    // Look up the invite
    if (!$invite = Invite::where('token', $token)->first()) {
      //if the invite doesn't exist do something more graceful than this
      abort(500);
    }

     $email = $invite->email;
    session(['token' => $token]);

    //Si existe mostramos error
    if ($user = User::where('email', $invite->email)->first()) {
      //if the invite doesn't exist do something more graceful than this
      abort(502, "User already exists");
    }
    
    $host = User::where('id', $invite->host)->first();
    $host_id = $host->id;

    // Maybe create a Headquarter model.
    $headquarters = DB::table('headquarters')->where('id', $host->headquarters_id)->first();
    $headquarter = $headquarters->id;

    // create the user with the details from the invite
    //User::create(['email' => $invite->email]);

    // delete the invite so it can't be used again
    //$invite->delete();

    // here you would probably log the user in and show them the dashboard, but we'll just prove it worked

    return view("auth.register", compact(['email', 'host_id', 'headquarter']));
  }
}
