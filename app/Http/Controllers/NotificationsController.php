<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications;

class NotificationsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function getNotifNumber(Request $request){

    }

    public static function getNotifications(){

        $user_notifications = Notifications::where('receiver', \Auth::user()->id)
        ->where('read', false)
        ->skip(0)
        ->take(2)
        ->get();

        return json_encode($user_notifications, JSON_UNESCAPED_UNICODE);

    }

    public function readNotification(Request $request){

        $notification = Notifications::find($request->notId)->update(['read' => true]);
        return response()->json($notification);
    }

    public function readAllNotif(){
        $current_user = \Auth::user()->id;
        Notifications::where('receiver', $current_user)->where('read', false)->update(['read' => true]);
        return NotificationsController::getNotifications();
    }

}
