<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Documents;

class FileUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function upload_file(Request $request){

        $ext = $request['extension'];
        $pdf = false;
        $type =$request['type'];
        if($ext == "pdf"){
            $file = $request[$request->type];
            $pdf=true;
        } else if($ext=="webm"){
            $file = $request['interview'];
        } else{
            //File not supported
        }

        $file_data = array();

        foreach($file as $key => $value){

            $file_data[$key] = $value;
        }

        $filename = $file->getClientOriginalName();

        $url = "";

        if($pdf){
            $url = storage_path('app/'.$request->file_type.'/');
        } else{
            $url = storage_path('app/interviews/');
        }
        
        $file->move($url, $filename);
        
        $document = new Documents;

        $document->name = $filename;
        $document->url = $url.$filename;
        $document->user_id = \Auth::user()->id;
        $document->extension = $ext;
        $document->type = $type;

        $document->save();

        $type = 1;
        return redirect("/process")->with(['type' => $type]);
    

        // return response()->json(['name' => $filename, 'state' => 'CA']);

    }
}
