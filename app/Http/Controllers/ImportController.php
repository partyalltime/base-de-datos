<?php

namespace App\Http\Controllers;
use App\Imports\ExcelImport;
use Illuminate\Http\Request;


use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Muestra formulario para subir excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.import');
    }

    /**
     * Muestra formulario para subir excel.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $array = Excel::toArray(new ExcelImport, $request->excel);

        return $array;
    }
}
