<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Post;
use App\Items;
use App\Comments;
use App\Replies;
use Auth;


class CoursesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function tutorialView($id){
        $post = Post::find($id);
        $items = Items::orderBy('order','asc')->where('post_id',$id)->get();
        $items_time = Items::where('post_id',$id)->sum('time');
        $post["total_time"] = $items_time;
        return view('web.tutorialView',compact('items', 'post'));
    }
    public function myCoursesView(){
        $categories = Category::all();
        $posts= Post::select("posts.*", "users_posts.user_id")
        ->join('users_posts', 'posts.id', '=', 'users_posts.post_id')
        ->where('users_posts.user_id', Auth::getUser()->id)
        ->orderBy('posts.created_at','ASC')
        ->get();
        foreach ($posts as $key=>$elem) {
            $items_time = Items::where("post_id", $elem->id)->sum('time');
            $posts[$key]["total_time"] = $items_time;
        }
        return view('home',compact('categories', 'posts'));
    }

    public function commentsView($id){
        $post_id = $id;
        $comments = Comments::select("comments.*", "users.name as username", DB::raw('COUNT(replies.id) AS total_replies'))
        ->where('post_id', $post_id)
        ->join('users', 'users.id', '=', 'comments.user_id')
        ->leftJoin('replies', 'comments.id', '=', 'replies.comment_id')
        ->groupBy('comments.id')
        ->orderBy('comments.created_at','ASC')
        ->get();

        return view('web.comments',compact('comments', 'post_id'));
    }

    public function addComment(Request $request)
    {
        $user_id = Auth::getUser()->id;
        $post_id = $request->input('post-id');
        $comment_subject = $request->input('comment-subject');
        $comment_body = $request->input('comment-body');
        Comments::create([
            'user_id' => $user_id,
            'subject' => $comment_subject,
            'post_id' => $post_id,
            'body' => $comment_body,
        ]);

        return redirect()->back();
    }
    public function repliesView($id)
    {
        $comment = Comments::select("comments.*", "users.name as username")
        ->where('comments.id', $id)
        ->join('users', 'users.id', '=', 'comments.user_id')
        ->orderBy('comments.created_at','ASC')
        ->limit(1)
        ->get();
        $replies = Replies::select("replies.*", "users.name as username")
        ->where('replies.comment_id', $id)
        ->join('users', 'users.id', '=', 'replies.user_id')
        ->orderBy('replies.created_at','ASC')
        ->get();
        return view('web.replies',compact('comment', 'replies'));

    }

    public function interview()
    {
        return view("web.questions");
    }

    public function addReply(Request $request)
    {
        $user_id = $user_id = Auth::getUser()->id;
        $comment_id = $request->input('comment-id');
        $reply_body = $request->input('reply-body');

        Replies::create([
            'user_id' => $user_id,
            'comment_id' => $comment_id,
            'body' => $reply_body,
        ]);

        return redirect()->back();
    }

    public function completePostUserStatus($post_id)
    {
        $new_users_posts= DB::table("users_posts")->where('user_id', Auth::getUser()->id)->where('post_id', $post_id)->update(['completed' => 1]);
        return response()->json($new_users_posts);
    }
}
