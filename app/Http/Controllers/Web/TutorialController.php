<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Items;

class TutorialController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function tutorialView($id){
        $post = Post::find($id);
        $items = Items::orderBy('order','asc')->where('post_id',$id)->get();
        $items_time = Items::where('post_id',$id)->sum('time');
        $post["total_time"] = $items_time;
        return view('web.tutorialView',compact('items', 'post'));
    }
    public function completePostUserStatus($post_id)
    {
        $new_users_posts= DB::table("users_posts")->where('user_id', Auth::getUser()->id)->where('post_id', $post_id)->update(['completed' => 1]);
        return response()->json($new_users_posts);
    }
}
