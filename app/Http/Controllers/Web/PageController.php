<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function blog(){
        $posts=Post::orderBy('id','DESC')->where('status','PUBLISHED')->paginate(3);
        return view('web.posters',compact('posts'));
    }
    public function category($slug){
        $category =Category::where('slug',$slug)->pluck('id')->first();//devuelve solo el id con pluck y el first, el primero
        $posts=Post::where('category_id',$category)
                    ->orderBy('id','DESC')->where('status','PUBLISHED')->paginate(3);
        return view('web.posts',compact('posts'));
    }
    public function tag($slug){
        
        $posts=Post::whereHas('tags',function($query) use($slug){
             $query->where('slug',$slug);
        })->orderBy('id','DESC')->where('status','PUBLISHED')->paginate(3);
        return view('web.posts',compact('posts'));
    }
    public function post($slug){
        
        $post =Post::where('slug',$slug)->first();
        
        return view('web.postersBlog',compact('post'));
    }
}
