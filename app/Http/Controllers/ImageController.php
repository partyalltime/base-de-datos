<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ImageController extends Controller
{
   public function store(Request $request)
    {

        $path = $request->file('fileToUpload')->storeAs(
        	'images/uploads', 
        	$request->user()->id.time().$request->file('fileToUpload')->getClientOriginalName(),
        	'public'
		);




        return response()->json([
		    'success' => true,
		    'url' => $path
		]);
    }
}
