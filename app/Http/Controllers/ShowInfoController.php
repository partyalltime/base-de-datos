<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShowInfoController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    public function info(Request $request)
    {

        $myValue = $request->input('envio');

        return response()->json([
            'name' => 'Info',
            'state' => 'Intern',
            'requestInfo' => $myValue
        ]);
    }

}
