<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Category;
use App\Tag;
use App\Items;
use Auth;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
    
        $this->middleware('auth');

   }

    public function index()
    {
        $posts = Post::orderBy('id','DESC')->where('user_id',auth()->user()->id)->paginate();

        return view('admin.posts.index',compact('posts'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name','ASC')->pluck('name','id');//porque lo voy a pasar a un select y solo necesito el nombre y el id
        $tags= Tag::orderBy('name','ASC')->get(); //Voy a utilizar un checkbox y es conveniente utilizar get
        return view('admin.posts.create',compact('categories','tags'));//muestra el formulario   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        $post = Post::create($request->all()); //solo es aceptable si esta el fillable
        //Image
        if($request->file('file')){

            $path = Storage::disk('public')->put('images',$request->file('file'));
            $post->fill(['file' => asset($path)])->save();
             }
       //Tags
       $post->tags()->attach($request->get('tags'));
        return redirect()->route('posts.edit',$post->id)
        ->with('info','El Post ha sido creado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $this->authorize('pass',$post);
        return view('admin.posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        
        $this->authorize('pass',$post);

        $categories = Category::orderBy('name','ASC')->pluck('name','id');
        $tags= Tag::orderBy('name','ASC')->get();
        
        return view('admin.posts.edit',compact('post','categories','tags'));//muestra el formulario  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $id)
    {
       
        $post = Post::find($id); 
        $post->fill($request->all())->save();
        $this->authorize('pass',$post);
        //Image
        if($request->file('file')) {

        $path = Storage::disk('public')->put('images',$request->file('file'));
        $post->fill(['file' => asset($path)])->save();
        }
        //Tags
        $post->tags()->sync($request->get('tags'));
                
        return redirect()->route('posts.edit',$post->id)
                ->with('info','Post actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post= Post::find($id);
        $this->authorize('pass',$post);
        $post->delete();
        return back()->with('info','Eliminando el registro');
    }

    public function postByCategory($categoryId)
    {
        $posts = [];
        if ($categoryId == 0){
            $posts= Post::select("posts.*", "users_posts.user_id")
            ->leftJoin('users_posts', function($join){
                $join->on('posts.id', '=', 'users_posts.post_id')
                ->on('users_posts.user_id', '=', DB::raw(Auth::getUser()->id));
            })
            ->orderBy('created_at','ASC')
            ->limit(1000)
            ->paginate(1000);
        }else{
            $posts= Post::select("posts.*", "users_posts.user_id")
            ->leftJoin('users_posts', function($join){
                $join->on('posts.id', '=', 'users_posts.post_id')
                ->on('users_posts.user_id', '=', DB::raw(Auth::getUser()->id));
            })
            ->orderBy('created_at','ASC')
            ->where('category_id',$categoryId)
            ->limit(1000)
            ->paginate(1000);
        }
        foreach ($posts as $key=>$elem) {
            $items_time = Items::where("post_id", $elem->id)->sum('time');
            $posts[$key]["total_time"] = $items_time;
        }
        return response()->json($posts);
    }
    public function postByCategoryMyCourses($categoryId)
    {
        $posts = [];
        if ($categoryId == 0){
            $posts= Post::select("posts.*", "users_posts.user_id")
            ->join('users_posts', 'posts.id', '=', 'users_posts.post_id')
            ->where('users_posts.user_id', Auth::getUser()->id)
            ->orderBy('created_at','ASC')
            ->limit(1000)
            ->paginate(1000);
        }else{
            $posts= Post::select("posts.*", "users_posts.user_id")
            ->join('users_posts', 'posts.id', '=', 'users_posts.post_id')
            ->where('users_posts.user_id', Auth::getUser()->id)
            ->orderBy('created_at','ASC')
            ->where('category_id',$categoryId)
            ->limit(1000)
            ->paginate(1000);
        }
        foreach ($posts as $key=>$elem) {
            $items_time = Items::where("post_id", $elem->id)->sum('time');
            $posts[$key]["total_time"] = $items_time;
        }
        return response()->json($posts);
    }
    public function postTools()
    {
        $categories = Category::all();
        $posts= Post::orderBy('created_at','ASC')->limit(1000)->get();
        foreach ($posts as $key=>$elem) {
            $items_time = Items::where("post_id", $elem->id)->sum('time');
            $posts[$key]["total_time"] = $items_time;
        }
        return view('admin.posts.tools',compact('categories', 'posts'));
    }
    public function addPost(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'body' => 'required',
            'category_id' => 'required',
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $post= array(
            'body' => $request->get('body'),
            'slug' => str_replace(' ', '-', strtolower($request->get('name'))),
            'category_id' => $request->get('category_id'),
            'name' => $request->get('name'),
        );
        $newPost= Post::insertGetId($post);
        $users_posts= array(
            'user_id' => Auth::getUser()->id,
            'post_id' => $newPost,
            'relationship' => 'creator',
        );
        $new_users_posts= DB::table("users_posts")->insertGetId($users_posts);

        return response()->json($newPost);
    }
    public function deletePost($id)
    {
        $post = Post::find($id);
        $delete_post= $post->delete();
        return redirect('postTools');
    }
}
