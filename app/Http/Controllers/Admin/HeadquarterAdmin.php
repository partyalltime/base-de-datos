<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Headquarter;

class HeadquarterAdmin extends Controller
{
    public function index() {
        if (\Auth::user()->isAdmin()) {
            $heaquarter_id = \App\User::where('id', \Auth::user()->id)
                ->pluck('headquarters_id')
                ->first();

            $headquarter_country = \App\Headquarter::where('id', $heaquarter_id)
                ->pluck('country')
                ->first();

            $country_settings = \App\CountrySettings::where('country', $headquarter_country)
                ->select('academic_level_restrictions', 'academic_cycle_restrictions', 'country')
                ->first();

            $interviewers = \App\User::where([
                ['role_id', 13],
                ['headquarters_id', $heaquarter_id]
            ])->get();

            return view('admin.HeadquarterAdmin', compact(['country_settings', 'headquarter_country', 'interviewers']));
        }
    }

    public function updateHeadquarterPreferences(Request $request) {
        if (\Auth::user()->isAdmin()) {
            $academic_level_restrictions = explode(',', $request->get('tags-input-1'));
            $academic_cycle_restrictions = explode(',', $request->get('tags-input-2'));

            $country_settings = \App\CountrySettings::where('country', $request->get('headquarter_country'))->first();

            if ($country_settings !== null) {
                $country_settings =  \App\CountrySettings::where('country', $request->get('headquarter_country'))->update([
                    'academic_level_restrictions' => json_encode($academic_level_restrictions),
                    'academic_cycle_restrictions' => json_encode($academic_cycle_restrictions),
                    'interviewer' => $request->get('interviewer')
               ]);
            } else {
                $country_settings = \App\CountrySettings::create([
                    'country' => $request->get('headquarter_country'),
                    'academic_level_restrictions' => $academic_level_restrictions,
                    'academic_cycle_restrictions' => $academic_cycle_restrictions,
                    'interviewer' => $request->get('interviewer')
                ]);
            }

            return redirect()->to('headquarter/admin');
        } 
    }
    public function getAllHeadquarter(){
      $data = Headquarter::all();
      return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
}
