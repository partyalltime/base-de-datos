<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\ProspectStoreRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Process\ProcessEmailController;
use Illuminate\Support\Facades\Storage;
use App\Prospect;

class ProspectController extends Controller
{
		/**
     * This method references get HTTP request 
     *
     * @return JSON
     */
		public function index()
		{
			if(Auth::user()->isAdmin()){
				$data = Prospect::select('prospect_data.id', 'prospect_data.programing_experience',
          'prospect_data.informatics_experience','users.name',
          'prospect_data.remarkable_experience','prospect_data.programing_knowledge','new_intern_data.prospect_id as new_intern',
        	'informatics_knowledge','user_id')
          ->join('users','users.id','=','prospect_data.user_id')
          ->leftJoin('new_intern_data','prospect_data.id' ,'=', 'new_intern_data.prospect_id')
          ->get();
			}
			else{
				$data = Prospect::select('prospect_data.id', 'prospect_data.programing_experience',
          'prospect_data.informatics_experience','users.name',
          'prospect_data.remarkable_experience','prospect_data.programing_knowledge','new_intern_data.prospect_id as new_intern',
        	'informatics_knowledge','user_id')
          ->join('users','users.id','=','prospect_data.user_id')
          ->leftJoin('new_intern_data','prospect_data.id' ,'=', 'new_intern_data.prospect_id')
          ->where('users.headquarters_id','=', Auth::user()->headquarters_id)
          ->get();
			}
			
      return json_encode($data, JSON_UNESCAPED_UNICODE);
      //return Auth::user();
		}
		/**
     * Show the view for a new prospect.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('web.ProspectForm');
    }
		/**
     * Show the application view.
     *
     * @return \Illuminate\Http\Response
     */
    public function prospect()
    {
    	if(Auth::user()->isAdmin()){
				$prospects = Prospect::select('prospect_data.id', 'prospect_data.programing_experience',
          'prospect_data.informatics_experience','users.name',
          'prospect_data.remarkable_experience','prospect_data.programing_knowledge','new_intern_data.prospect_id as new_intern',
        	'informatics_knowledge','user_id')
          ->join('users','users.id','=','prospect_data.user_id')
          ->leftJoin('new_intern_data','prospect_data.id' ,'=', 'new_intern_data.prospect_id')
          ->get();
			}
			else{
				$prospects = Prospect::select('prospect_data.id', 'prospect_data.programing_experience',
          'prospect_data.informatics_experience','users.name',
          'prospect_data.remarkable_experience','prospect_data.programing_knowledge','new_intern_data.prospect_id as new_intern',
        	'informatics_knowledge','user_id')
          ->join('users','users.id','=','prospect_data.user_id')
          ->leftJoin('new_intern_data','prospect_data.id' ,'=', 'new_intern_data.prospect_id')
          ->where('users.headquarters_id','=', Auth::user()->headquarters_id)
          ->get();
			}
        return view('web.ProspectTable')->with('prospects',$prospects);
        // return view('home');
    }
    /**
    *	This function stores on db a new Prospect
    *	
    *	@param $request
    *	@return response
    */
    public function store(ProspectStoreRequest $request)
    {
    	$response = Prospect::create($request->all());
    	$data = [
    		'success'=> false,
    		'message'=> 'No se puede procesar el pedido por el momento'
    	];
    	if($response->save()){
    		$data["success"] = true;
    		$data["message"] = 'La peticion se ha procesado correctamente';
    	}
    	
    	return response()->json($request);
    }
    /**
    *	This function allow us to get the form to edit a specific prospect
    *
    *	@param $id Prospect Id
    * @return \Illuminate\Http\Response
    */
    public function show($id){
    	$prospect = Prospect::find($id);
    	return view('web.ProspectForm')->with('prospect',$prospect);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prospect = Prospect::findOrFail($id);
        $prospect->update($request->all());
        $prospect->process_state = 1;
        $prospect->save();
        ProcessEmailController::sendRejectionOrApproval($request->has_been_approved, $prospect->user_id);
        return "Done";
    }
    /**
		* This function allow us to get a specific prospect
		* 
		*	@param $id Prospect Id
    * @return \Illuminate\Http\Response
    */
    public function getProspect($id){
    	
    	return Prospect::getActualState($id);
    }

    public function downloadProspectData($type, $user_id) {
      $document = \App\Documents::where([
        ['user_id', '=', $user_id],
        ['type', '=', $type]
        ])->first();

      return Storage::download($document->name);
    }
}
