<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Category;
use App\Tag;
use App\Items;
use App\Test;
class ItemController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function __construct(){
    
        $this->middleware('auth');

    }

    public function itemTools($id)
    {
        $post = Post::find($id);
        $items = Items::orderBy('order','asc')->where('post_id',$id)->get();
        $exams = Test::where('id_post', $id)->get(); 
        return view('admin.posts.itemTools',compact('items', 'post', 'exams'));
    }
    public function getItems(Request $request)
    {
        
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $items = Items::orderBy('order','asc')->where('post_id',$request->get("id"))->get();
        return response()->json($items);
    }
    public function addOneItem(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'body' => 'required',
            'time' => 'required',
            'post_id' => 'required',
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $lastItemOfPost = DB::table('items')->select('order')->orderBy('order','desc')->where('post_id',$request->get('post_id'))->limit(1)->get();
        $itemOrder = 0;
        if(!$lastItemOfPost->isEmpty()){
            $itemOrder = $lastItemOfPost[0]->order + 1;
        }
        $item= array(
            'body' => $request->get('body'),
            'time' => $request->get('time'),
            'title' => $request->get('title'),
            'post_id' => $request->get('post_id'),
            'order' => $itemOrder,
        );
        $newItem= DB::table('items')->insertGetId($item);
        $item['id'] = $newItem;
        return response()->json($item);
    }
    public function modifyItem(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'body' => 'required',
            'time' => 'required',
            'id' => 'required',
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $item= array(
            'body' => $request->get('body'),
            'time' => $request->get('time'),
            'title' => $request->get('title'),
        );
        $item_update = Items::where('id', $request->get('id'))->update($item);
        $updated_item= Items::find($request->get('id'));
        return response()->json($updated_item);
    }
    public function deleteItem(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $items = Items::where('id', $request->get('id'))->delete();
        return response()->json($items);
    }
}
