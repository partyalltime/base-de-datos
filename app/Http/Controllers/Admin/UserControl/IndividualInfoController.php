<?php

namespace App\Http\Controllers\Admin\UserControl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Prospect;
use App\NewIntern;
use Auth;

class IndividualInfoController extends Controller
{
    public function create(Request $request)
    {
        return User::create($request->all());
    }
    public function get_all()
    {
        return User::all();
    }
    public function get($id)
    {
        $info = [];
        if(Auth::user()){
	        if((Auth::user()->id == $id) || Auth::user()->isAdmin() || Auth::user()->isManager()){
	            $user = User::select("users.name as name","users.email as email",
	                    "users.role_id as role_id", "users.phone as phone", "users.academic_level as academic_level",
											"users.academic_cycle as academic_cycle",
	                    "users.image_url as image_url", "users.birthday as birthday",
	                    "users.gender as gender", "countries.en_short_name as residency", "countries.alpha_3_code as code",
	                    "prospect_data.has_been_approved as approved", "prospect_data.requested_scholarship as requested_scholarship")
	                    ->join('roles', 'users.role_id', '=', 'roles.id')
	                    ->leftJoin('countries','users.residency_id', '=', 'countries.num_code')
	                    ->join('prospect_data','users.id','=','prospect_data.user_id')
	                    // ->leftJoin('countries', 'users.residency_id', '=', 'countries.num')
	                    ->where('users.id','=',$id)
	                    ->get();


	            
	            if($user[0]["role_id"] == 4){
	                $user_attributes = Prospect::where('user_id', $id)->first();
	                $info['user_data'] = array(
	                    "Experiencia de programación" => $user_attributes['programing_experience'],
	                    "Experiencia informática" => $user_attributes['informatics_experience'],
	                    "Experiencias reseñables" => $user_attributes['remarkable_experience'],
	                    "Conocimiento de programación" => $user_attributes['programing_knowledge'],
	                    "Conocimientos informáticos" => $user_attributes['informatics_knowledge'],

	                );
	            } else if($user[0]["role_id"] == 5){
	                $prospect_data = Prospect::where('user_id', $id)-> first();
	                $user_attributes = NewIntern::where('prospect_id', $prospect_data['id'])->join('companies', 'companies.id', '=', 'new_intern_data.internship')->first();
	                $info['user_data'] = array(
	                    "Experiencia de programación" => $prospect_data->programing_experience,
	                    "Experiencia informática" => $prospect_data['informatics_experience'],
	                    "Experiencias reseñables" => $prospect_data['remarkable_experience'],
	                    "Conocimiento de programación" => $prospect_data['programing_knowledge'],
	                    "Conocimientos informáticos" => $prospect_data['informatics_knowledge'],
	                    "Fecha de llegada" => date("M d Y",strtotime($user_attributes['arrival'])),
	                    "Pre-inscrito" => ($user_attributes['enrolled'] == 1) ? 'Si': 'No',
	                    "Pre-registrado" => ($user_attributes['pre_registered'] == 1) ? 'Si': 'No',
	                    "Nivel" => $user_attributes['level'],
	                    "Otorgado" => $user_attributes['granted'],
	                    "Notificado" => $user_attributes['notified'],
											"Prácticas" => $user_attributes['name'],
	                );
	            }
	            $info['user'] = $user;

	        }
	        else{
	            return abort('403');
	        }
       
        return view('admin.users.user')->with('info',json_decode(json_encode($info,JSON_UNESCAPED_UNICODE),true));
      }
      else{
      	return abort('403');
      }
    }
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return $user;
    }
    public function delete($id)
    {
        $user = Post::findOrFail($id);
        $user->delete();
        return view('admin.posts.itemTools', compact('items', 'post'));
    }
}
