<?php

namespace App\Http\Controllers\Admin\UserControl;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\NewIntern;
use App\Prospect;
use App\Companies;
use App\User;
use App\Mail\ScholarshipGranted;
use App\Mail\ScholarshipGrantedInterviwed;
use App\Mail\InterviewReady;
use Illuminate\Support\Facades\Mail;

class NewInternController extends Controller
{
    public function interviews() {
        if (Auth::user()->isAdmin()) {
            $interviews = Prospect::where([
                ['requested_interview', '=', 1],
                ['interviewed', '!=', 1],
                ])
                ->leftJoin('users', 'users.id', '=', 'prospect_data.user_id')
                ->get();

            $headquarters = \App\Headquarter::get();

            return view('web.Interviews', compact(['interviews', 'headquarters']));
        } else {
            $interviewer_hq = \App\User::select('headquarters_id')->where('id', Auth::user()->id)->first();
            $interviews = Prospect::where([
                ['requested_interview', '=', 1],
                ['interviewed', '!=', 1],
                ])
                ->leftJoin('users', function($join, $interviewer_hq) {
                    $join->on('users.id', '=', 'prospect_data.user_id');
                    $join->on('users.headquarters_id', '=', $interviewer_hq);
                })
                ->leftJoin()
                ->get();

            $headquarters = null;

            return view('web.Interviews', compact(['interviews', 'headquarters']));
        }
    }

    public function accept_interview(Request $request) {
        if (Auth::user()->isAdmin()) {
            $email = $request->email;
            $user_to_email = User::where('email', $email)->first();
            $room_id = str_random(60);

            Prospect::where('user_id', $user_to_email->id)->update([
                'interview_room' => $room_id,
            ]);

            Mail::to($user_to_email->email)->bcc($user_to_email->email)->send(new InterviewReady($user_to_email));

            return redirect()->route('interview/conference/admin', $user_to_email->id);
        }
    }

    public function interview_room_admin($id) {
        $user = User::where('id', $id)->first();
        $user_id = $user->id;
        $user_name = $user->name;
        $prospect = Prospect::where('user_id', $id)->first();
        $room_id = $prospect->interview_room;

        return view('web.InterviewRoomAdmin', compact(['user_name', 'room_id', 'user_id']));
    }

    public function interview_done(Request $request) {
        $user_id = $request->get('user_id');
        $prospect = Prospect::where('user_id', $user_id)->update([
            'interview_room' => NULL,
            'interviewed' => 1,
        ]);

        return redirect()->route('interviews');
    }

    public function newIntern()
    {
    	if(Auth::user()->isAdmin()){
    		$new_intern = NewIntern::select('new_intern_data.id','users.name','prospect_data.user_id', 'new_intern_data.arrival','new_intern_data.enrolled',
        'new_intern_data.pre_registered', 'new_intern_data.level', 'new_intern_data.granted',
        'new_intern_data.arrival', 'new_intern_data.notified','companies.name as internship','new_intern_data.scholarship'
        )
        ->join('prospect_data', 'prospect_data.id','=','new_intern_data.prospect_id')
        ->join('users','users.id','=','prospect_data.user_id')
        ->leftJoin('companies','companies.id','=','new_intern_data.internship')
        ->get();
    	}else{
    		$new_intern = NewIntern::select('new_intern_data.id','users.name','prospect_data.user_id', 'new_intern_data.arrival','new_intern_data.enrolled',
        'new_intern_data.pre_registered', 'new_intern_data.level', 'new_intern_data.granted',
        'new_intern_data.arrival', 'new_intern_data.notified','companies.name as internship','new_intern_data.scholarship'
        )
        ->join('prospect_data', 'prospect_data.id','=','new_intern_data.prospect_id')
        ->join('users','users.id','=','prospect_data.user_id')
        ->leftJoin('companies','companies.id','=','new_intern_data.internship')
        ->where('users.headquarters_id','=', Auth::user()->headquarters_id)
        ->get();
        }
        
        return view('web.NewInternTable', compact('new_intern'));
    }
    public function create(Request $request)
    {
    		$users = DB::table('prospect_data')
        ->select( DB::raw("users.id as 'user',prospect_data.id as 'Prospectid', users.name as 'Nombre', headquarters.name as 'Headquarter', 
									(select users.email from country_settings left join users on country_settings.interviewer = users.id where country_settings.country = headquarters.country) as 'Interviewer'"))
        ->leftJoin('users', 'prospect_data.user_id', '=', 'users.id')
        ->leftJoin('headquarters','headquarters.id','=', 'users.headquarters_id')
        ->where('prospect_data.id','=',$request->prospect_id)
        ->get()->first();
        $data = User::where('id', $users->user)->first();
        Mail::to($users->Interviewer)->send(new ScholarshipGrantedInterviwed($data));
        return NewIntern::create($request->all());
    }
    public function get_all()
    {
        return NewIntern::all();
    }

    public function get_pending(){

        $prospect = Prospect::select('prospect_data.id', 'users.name')->where([
            ['has_been_approved', '=', "0"],
            ['rejected', "=", "0"],
            ['requested_scholarship','=','1']
        ])
        ->join('users', 'users.id', '=', 'prospect_data.user_id')
        ->get();

        return view('web.PendingTable', compact('prospect'));
    }

    public function get($id)
    {
        return NewIntern::find($id);
    }
    public function update(Request $request, $id)
    {
        $new_intern = NewIntern::findOrFail($id);
        $new_intern->update($request->all());
        return $new_intern;
    }
    public function delete($id)
    {
        $new_intern = Post::findOrFail($id);
        $new_intern->delete();
        return $new_intern;
    }
    public function viewCreate($id)
    {
        $prospect = Prospect::find($id);
        $user = $prospect->user;
        $companies = Companies::all();
        $user_to_email = User::where('id', $user->id)->first();
        Mail::to($user_to_email->email)->bcc($user_to_email->email)->send(new ScholarshipGranted($user_to_email));
        return view('admin.users.createNewIntern', compact('user', 'companies', 'id'));
    }
}
