<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Roles;
use App\Headquarter;
use App\Http\Controllers\Controller;
use Auth;

class AdminController extends Controller
{
  public function index() {
    if (Auth::user()->isAdmin()) {
      $users = User::all();
      return view('admin/UserAdmin', compact(['users']));
    }
  }

  public function create() {
    if (Auth::user()->isAdmin()) {
      $headquarters = Headquarter::get();
      return view('admin/CreateUser', compact(['headquarters']));
    }
  }

  public function update($id) {
    if (Auth::user()->isAdmin()) {
      $user = User::where('id', $id)->first(['id', 'name', 'role_id', 'headquarters_id', 'email']);
      $headquarters = Headquarter::get(['id', 'name']);

      return view('admin/UpdateUser', compact(['user', 'headquarters']));
    }
  }
}
