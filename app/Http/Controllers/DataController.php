<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\ArrayToXml\ArrayToXml;


class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.data');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function review()
    {
        return view('admin.review');
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function export()
    {

        $array = [
            'Good guys' => [
                'Guy' => [
                    ['name' => 'Luke Skywalker', 'weapon' => 'Lightsaber'],
                    ['name' => 'Captain America', 'weapon' => 'Shield'],
                ],
            ],
            'Bad guys' => [
                'Guy' => [
                    ['name' => 'Sauron', 'weapon' => 'Evil Eye'],
                    ['name' => 'Darth Vader', 'weapon' => 'Lightsaber'],
                ],
            ],
        ];

        $result = ArrayToXml::convert($array, [
            'rootElementName' => 'EnvioEntrada',
            '_attributes' => [
                'VERSION'=>'3.4', 
                'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                
            ],
        ], true, 'UTF-8');

        return response($result, 200)
        ->header('Cache-Control', 'public')
        ->header('Content-Description', 'File Transfer')
        ->header('Content-Disposition', 'attachment; filename=test.xml')
        ->header('Content-Transfer-Encoding', 'binary')
        ->header('Content-Type', 'text/xml');
    }
}
