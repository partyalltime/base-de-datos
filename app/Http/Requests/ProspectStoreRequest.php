<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProspectStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'user_id'=>'required|integer',
            'programing_experience'=>'required',
            'informatics_experience'=>'required',
            'remarkable_experience'=>'required',
            'programing_knowledge'=>'required',
            'informatics_knowledge'=>'required'
        ];
        return $rules;
    }
}
