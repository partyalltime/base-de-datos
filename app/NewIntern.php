<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewIntern extends Model
{
    protected $table = 'new_intern_data';
    public $timestamps = false;
    protected $fillable = [
        'arrival',
        'enrolled', 
        'pre_registered', 
        'level',
        'granted',
        'notified',
        'internship',
        'prospect_id',
        'scholarship'
    ];

    public function prospect(){
        return $this->belongs(Prospect::class);
    }

    public function companies(){
        return $this->hasOne(Companies::class);
    }
}
