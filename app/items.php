<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
 
class Items extends Model
{
    use HasSlug; //Aqui es donde aplicamos el trait
    protected $fillable = [
        'post_id','time', 'title', 'body','before_id',
    ];
   
    // Se pone en singular porque es un usuario
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
   
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
