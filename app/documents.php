<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    public $timestamps = false;
    protected $table = 'documents';
}