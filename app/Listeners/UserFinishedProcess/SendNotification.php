<?php

namespace App\Listeners\UserFinishedProcess;

use App\Events\UserFinishedProcess;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications;
use Carbon\Carbon;

class SendNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserFinishedProcess  $event
     * @return void
     */
    public function handle(UserFinishedProcess $event)
    {
        //
        $notification = new Notifications;

        $notification->type = "Usuario ha finalizado proceso";
        $notification->message = "El usuario ".$event->user->name." ha finalizado el proceso inicial.";
        $notification->read = false;
        $notification->receiver = 4;
        $notification->date = Carbon::now()->toDayDateTimeString();

        $notification->save();

    }
}
