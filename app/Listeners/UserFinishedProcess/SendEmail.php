<?php

namespace App\Listeners\UserFinishedProcess;

use App\Events\UserFinishedProcess;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserFinishedProcess  $event
     * @return void
     */
    public function handle(UserFinishedProcess $event)
    {
        
        $to_name = "Uneacademy";
        $to_email = "cts.desarrollo@uneatlantico.es";
        $body = "El usuario ".$event->user->name." ha completado el proceso inicial.";

        $data = array('name' => $to_name, "body" => $body);
        Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Un nuevo usuario ha finalizado su proceso');
            $message->from('cts.desarrollo@uneatlantico.es', 'UNEACADEMY');
        });
    }
}
