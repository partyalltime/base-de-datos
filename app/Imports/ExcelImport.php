<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\ToCollection;


class ExcelImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {

        $data = [];

        foreach ($collection as $row) 
        {
           Arr::prepend($data, $row);
        }

        return $data;
    }
}
