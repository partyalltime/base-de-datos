<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcessDate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'interview';
    protected $fillable = [
        'start_date','end_date', 'user_id'
    ];
     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
