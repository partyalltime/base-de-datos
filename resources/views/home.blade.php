@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center animated fadeIn slower">
    <div class="col-md-12">            
      <div class="row">
	      @if(request()->is('home'))
          <div class="col-md-6">
            <div class="tile">
              <div class="tile-title-w-btn">
                <h3 class="title">¡Tú eliges que aprender!</h3>
              </div>
              <div class="tile-body text-muted my-4">
                El material que encontrarás aquí son guías paso a paso basadas en proyectos 
                para hacer cosas increíbles y marcar un camino más fácil para el proceso de aprendizaje. 
                Desde los fundamentos básicos hasta el despliegue de tu proyecto, 
                estamos aquí para ayudarte en tú camino.
              </div>
            </div>
          </div>
	      @endif
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <form>
                <div class="form-group">
                  <label for="filtroCategorias">Filtrar por Categorías: </label>
                  <select style="background-color: transparent;" class="form-control" id="filtroCategorias">
                  <option  selected value="0">Todas las categorías</option>
                    @foreach ($categories as $item)
                      <option value="{{$item->id}}">{{ $item->name }}</option>
                    @endforeach
                  </select>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row mt-4 animated fadeIn slower" id="postsContainer">
            @foreach ($posts as $item)
	            <div class="col-lg-4 col-sm-12 col-md-6 d-flex">
                <div class="tile p-0 cursos flex-fill">
                  <div class="tile-title-w-btn justify-content-end bg-primary">
                    <div class="btn-group">
                      @if (is_null($item->user_id))
                        <a class="btn btn-primary" href="/addPostCourses/{{$item->id}}">
                        	<i class="fa fa-lg fa-user-plus"></i>
                        </a>
                      @else
                        <a class="btn btn-primary" href="./minusPostCourses/{{$item->id}}">
                          <i class="fa fa-lg fa-user-times"></i>
                        </a>
                        <a class="btn btn-primary" href="./commentsView/{{$item->id}}">
                          <i class="fa fa-lg fa-comment"></i>
                        </a>
                      @endif
                    </div>
                  </div>
                  <div class="card-header bg-default flex-fill">
                    @foreach ($categories as $itemC)
                      @if ($itemC->id === $item->category_id)
                        <div class="card-category mb-2">{{ $itemC->name }}</div>
                      @endif
                    @endforeach
                      <h3 class="title flex-grow-1">{{ $item->name }}</h3>                          
                  </div>
                  <div class="tile-body p-4" style="overflow: hidden; text-overflow: ellipsis; max-height: 150px">
                  	{{ $item->body }}
                  </div>
                  <div class="tile-footer p-4  ">
                    <div class=" d-flex justify-content-between">
                      @if (!is_null($item->user_id))
                        @if ($item->completed)
                          <a class="btn btn-outline-danger btn-sm" href="{{ './tutorialView/' . $item->id }}">Completado</a>
                          <a style="display: none;" class="btn btn-outline-info btn-sm" href="{{ './quizes/' . $item->id }}">Hacer Examen</a>
                        @else
                          <a class="btn btn-secondary" href="{{ './tutorialView/' . $item->id }}">Comenzar</a>
                        @endif
                      @endif
                      	<p class="text-muted text-right mt-2">Duración: {{$item->total_time}} minutos</p>
                    </div>
                  </div>
                </div>
	            </div>
            @endforeach
          </div>                                   
        </div>
    </div>
    </div>
  </div>
  <script>
    $("#filtroCategorias")
    .on("change",(e) => {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
            },
            url: location.origin + "/postByCategory{{ (request()->is('myCoursesView'))  ? 'MyCourses' : '' }}/"+$("#filtroCategorias option:selected" ).val()
        })
        .done((response) => {
            $("#postsContainer").empty();
            let html = "";
            response.data.forEach((element, i) => {
                let buttonAction = ``;
                if(element.user_id==null)
                    buttonAction = `<a class="btn btn-primary" href="/addPostCourses/`+element.id+`">
                        <i class="fa fa-lg fa-user-plus"></i>
                    </a>`
                else
                    buttonAction = `<a class="btn btn-primary" href="./minusPostCourses/`+element.id+`">
                        <i class="fa fa-lg fa-user-times"></i>
                    </a>`
                html += `<div class="col-lg-4 col-sm-12 col-md-6 d-flex animated fadeIn slower">
                    <div class="tile p-0 cursos flex-fill">
                        <div class="tile-title-w-btn justify-content-end bg-primary">
                            <div class="btn-group">`
                            +buttonAction+
                            `</div>
                        </div>
                        <div class="card-header bg-default flex-fill">
                            <div class="card-category mb-2">`+$("select option:selected" ).text()+`</div>
                            <h3 class="title">`+element.name+`</h3>
                        </div>
                        <div class="tile-body p-4" style="overflow: hidden; text-overflow: ellipsis; max-height: 150px">
                        `+element.body+`
                        </div>
                            <div class="tile-footer p-4">
                                <div>
                                    <div class=" d-flex justify-content-between">
                                         <a class="btn btn-secondary" href="./tutorialView/` +element.id +`">Comenzar</a>
                                        <p class="text-muted text-right mt-2">Duración: ` +element.total_time +` minutos</p>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>`;
            });
            $("#postsContainer").append(html);
        })
    })
  </script>
    </div>
</div>
@endsection
