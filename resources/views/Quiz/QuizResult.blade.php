<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<style type="text/css">
		
.question-container {
  padding: 30px 20px;
  width: 50%;
  min-height: 12em;
  background: #fafafa;
  border-top: 1px solid #eeeeee;
  border-left: 1px solid #eeeeee;
}
.question-container:nth-child(even) {
  border-right: 1px solid #eeeeee;
}
.question-container .item-incorrect {
  background: #f6f6f6;
}
.question-container .item-incorrect:before {
  position: absolute;
  z-index: 399;
  bottom: 0;
  right: 0;
  content: "";
  height: 0;
  width: 0;
  border: 35px solid;
  border-color: transparent #ff5c61 #ff5c61 transparent;
}
.question-container .item-incorrect:after {
  line-height: 1.4;
  position: absolute;
  z-index: 499;
  font-family: "fontawesome";
  content: ":(";
  bottom: 0;
  right: 7px;
  font-size: 1.9em;
  color: #BF383E;
}
.question-container .item-correct:before {
  position: absolute;
  z-index: 399;
  bottom: 0;
  right: 0;
  content: "";
  height: 0;
  width: 0;
  border: 35px solid;
  border-color: transparent #48d7bd #48d7bd transparent;
}
.question-container .item-correct:after {
  line-height: 1.4;
  position: absolute;
  z-index: 499;
  font-family: "fontawesome";
  content: ":)";
  bottom: 0;
  right: 7px;
  font-size: 1.9em;
  color: #2D9FB1;
}
input[type=radio] {
  position: absolute;
  visibility: hidden;
}
input[type=radio] + label {
  background: #fff;
  display: inline-block;
  padding: 5px 15px;
  margin: 5px 10px 5px 0;
  border: 1px solid #eeeeee;
  -webkit-transition: all 0.1s linear;
  -moz-transition: all 0.1s linear;
  transition: all 0.1s linear;
}
input[type=radio] + label:before {
  content: "";
  font-family: "fontawesome";
  margin-right: 7px;
  color: #2dceb1;
}
input[type=radio] + label:hover {
  cursor: pointer;
}
input[type=radio]:checked + label {
  background: #50d9c0;
  color: #1f8f7b;
}
input[type=radio]:checked + label:before {
  content: "";
  color: #1f8f7b;
}
	</style>
</head>
<body>
	<div class="container" id="element">
	
  <div class="row justify-content-md-center">
    <div class="col-md-5 question-container" v-for="(value,key,index) in dataExample">
      <div :class="setAnswerState(value.id)" >
        <p v-html="dataExample[key].question_content" />
      </div>
      <div class="choices" :for="'question' + value.question">
        <div v-for="(value2,key2,index2) in value.answers">
          <input type="radio" :id="'answer' + value2.id_answer" :name="'question' + value.question">
          <label :for="'answer' + value2.id_answer">@{{value2.content}}</label>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript">

	const app = new Vue({
  el: '#element',

  data: function(){
  	return {
	    message:'hi',
	    questions: [{'id': 'hey'},{'id':'nel'},{'id': 'hey'},{'id':'nel'}],
	    dataExample: null
	  }	
	},
	beforeMount: function () {
		const el = this;
	  this.getData().then(
	  	function(response){
	  		el.dataExample = response.data;
	  	});
	},
  methods:{
    setAnswerState: function(text){
      let cl = 'question item-correct';
      if(text === 'hey'){
        cl = 'question item-incorrect';
      }
      return cl;
    },
    getData: function(){
      const data = axios.get('/test/1');  
      console.log('lol: ', data);
	    return data;
    }
  }
});
</script>
</body>
</html>