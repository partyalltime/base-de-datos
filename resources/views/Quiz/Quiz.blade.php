<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
  <style>
  @import "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css";
  body{
  	font-family: 'Roboto', sans-serif;
  }

  .question-container {
    padding: 30px 20px;
    width: 100%;
    min-height: 12em;
    background: #fafafa;
    border-top: 1px solid #eeeeee;
    border-left: 1px solid #eeeeee;
    margin-bottom: 2%;
  }

  .question-container:nth-child(even) {
    border-right: 1px solid #eeeeee;
  }

  .question-container .item-incorrect {
    background: #f6f6f6;
  }

  .question-container .item-incorrect:before {
    position: absolute;
    z-index: 399;
    bottom: 0;
    right: 0;
    content: "";
    height: 0;
    width: 0;
    border: 35px solid;
    border-color: transparent #ff5c61 #ff5c61 transparent;
  }

  .question-container .item-incorrect:after {
    line-height: 1.4;
    position: absolute;
    z-index: 499;
    font-family: "fontawesome";
    content: ":(";
    bottom: 0;
    right: 7px;
    font-size: 1.9em;
    color: #BF383E;
  }

  .question-container .item-correct:before {
    position: absolute;
    z-index: 399;
    bottom: 0;
    right: 0;
    content: "";
    height: 0;
    width: 0;
    border: 35px solid;
    border-color: transparent #48d7bd #48d7bd transparent;
  }

  .question-container .item-correct:after {
    line-height: 1.4;
    position: absolute;
    z-index: 499;
    font-family: "fontawesome";
    content: ":)";
    bottom: 0;
    right: 7px;
    font-size: 1.9em;
    color: #2D9FB1;
  }

  input[type=radio] {
    position: absolute;
    visibility: hidden;
  }

  input[type=radio] + label {
    background: #fff;
    display: inline-block;
    padding: 5px 15px;
    margin: 5px 10px 5px 0;
    border: 1px solid #eeeeee;
    -webkit-transition: all 0.1s linear;
    -moz-transition: all 0.1s linear;
    transition: all 0.1s linear;
  }

  input[type=radio] + label:before {
    content: "\21d2";
    font-family: "fontawesome";
    margin-right: 7px;
    color: #2dceb1;
  }

  input[type=radio] + label:hover {
    cursor: pointer;
  }

  input[type=radio]:checked + label {
    background: #50d9c0;
    color: #1f8f7b;
  }

  input[type=radio]:checked + label:before {
    content: "\21d2";
    color: #1f8f7b;
  }
  .shadow-box{
		-moz-box-shadow: 0 0 2px #888;
		-webkit-box-shadow: 0 0 2px #888;
		box-shadow: 0 0 2px #888;
  }
  .header {
	  height: 8rem;
	  background:  #287cc0;
	  z-index: 0;
	}
	.borderTop{
		border-top-width: 8px;
  	border-top-style: solid;
  	border-top-color: #31A3FF;
	}
	.float-return{
		position: absolute;
		right: 0;
		left: 0;
		top: 0;
	}
	/*
	*	Start my own modal, 'cause bootstrap modal use its own js
	*	and it's crashing with our vue module
	*
	*/
	/* The Modal (background) */
	.modal {
	  display: none; /* Hidden by default */
	  position: fixed; /* Stay in place */
	  z-index: 1; /* Sit on top */
	  padding-top: 100px; /* Location of the box */
	  left: 0;
	  top: 0;
	  width: 100%; /* Full width */
	  height: 100%; /* Full height */
	  overflow: auto; /* Enable scroll if needed */
	  background-color: rgb(0,0,0); /* Fallback color */
	  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}

	/* Modal Content */
	.modal-content {
	  background-color: #fefefe;
	  margin: auto;
	  padding: 20px;
	  border: 1px solid #888;
	  width: 30%;
	}

	/* The Close Button */
	.close {
	  color: red;
	  float: right;
	  font-size: 28px;
	  font-weight: bold;
	}

	.close:hover,
	.close:focus {
	  color: #000;
	  text-decoration: none;
	  cursor: pointer;
	}
</style>
</head>
<body>
	<div class="header"></div>
	<div class="container" id="element" style="heigth: 100%; margin: -4rem auto 0 auto;">
		{{-- Start extra elements --}}
		<div class="back-button float-return">
			<button class="btn btn-outline-light" @click="getBackHome()"><i class="fa fa-arrow-left"></i> Regresar</button>
		</div>
		<div id="myModal" class="modal" ref="myModal">
		  <!-- Modal content -->
		  <div class="modal-content">
		    <span class="close" @click="closeModal()">&times;</span>
		    <p>La puntuación del examen se guardará ¿Desea contiunar?</p>
		    <div class="row justify-content-center">
		    	<div class="col-md-5">
		    		<button class="btn btn-primary btn-md" @click="sendData()">Aceptar</button><button class="btn btn-danger btn-md" @click="closeModal()">Salir</button>
		    	</div>
		    </div>
		  </div>
		</div>		{{-- End extra elements --}}
		<div class="row justify-content-center">
			<div class="col-md-10">
				<div class="card shadow-box borderTop" >
				  <div class="card-body">
				    <h3 class="card-title" style="padding-top: 2%;">@{{title}} - <span>Puntuación: @{{countOfCorrectAnswers}}/@{{countOfQuestions}}</span></h3>
				    <h6 class="card-subtitle mb-2 text-muted" style=" padding-bottom: 4%;">Responde con cuidado solo tienes una oportunidad por pregunta</h6>
				    <div class="row justify-content-md-center">
				      <template v-for="(value, key, index) in questions">
				        <question :question_content="value.question_content" :answers="value.answers" :question="value.question" />
				      </template>
				    </div>
				    <div class="row justify-content-end">
				    	<div class="col-md-2 offset-md-1">
					    	<button class="btn btn-primary btn-md" @click="setTestGrade(countOfCorrectAnswers,countOfQuestions)">Enviar</button>
				    	</div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
  </div>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="module">
	
  @if (app()->environment('production'))
    import Vue from 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.esm.browser.min.js';
    import Vuex from 'https://cdnjs.cloudflare.com/ajax/libs/vuex/3.1.1/vuex.esm.browser.min.js'; 
  @else
    import Vue from 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.esm.browser.js';
    import Vuex from 'https://cdnjs.cloudflare.com/ajax/libs/vuex/3.1.1/vuex.esm.browser.js';
  @endif

  Vue.use(Vuex);

  const store = new Vuex.Store({
    state: {
      correctAnswers: 0,
      totalQuestions: 0,
    },
    mutations: {
      addScore(state) {
        state.correctAnswers++;
      },
      addTotalQuestions(state, value) {
        state.totalQuestions = value;
      }
    }
  });

  let question = Vue.component('question', {
    template: `
      <div class="col-md-5 question-container">
        <div :class="answer.goal === null ? '' : answer.goal === 1 ? 'question item-correct' : 'question item-incorrect'">
          <p v-html="question_content" />
        </div>
        <div class="choices" :for="'question' + question">
          <div v-for="(value2,key2,index2) in answers">
            <input type="radio" 
            @click="checkAnswer(value2)" 
            :id="'answer' + value2.id_answer" 
            :name="'question' + question" 
            :disabled="answer.goal !== null">
            <label :for="'answer' + value2.id_answer">@{{value2.content}}</label>
          </div>
        </div>
      </div>
    `,
    props: ['answers', 'question_content', 'question'],
    data() {
      return {
        answer: {
          goal: null,
        }
      };
    },
    methods: {
      checkAnswer(value) {
        this.answer = value;

        if (value.goal === 1) store.commit('addScore');
      },
    }
  });

	const app = new Vue({
    el: '#element',
    data: function(){
      return {
        questions: [],
        title: null,
        grade: 0,
        test_id: null
      };	
    },
    computed: {
      countOfCorrectAnswers() {
        return store.state.correctAnswers;
      },
      countOfQuestions() {
        return store.state.totalQuestions;
      }
    },
    components: {
      'question': question,
    },
    beforeMount: function () {
      const el = this;
      this.getData().then(
        function(response){
          el.questions = response.data;
          Object.keys(el.questions).forEach(key => {el.title = el.questions[key].test; el.test_id = el.questions[key].test_id});
          store.commit('addTotalQuestions', Object.keys(el.questions).length);
        });
    },
    methods:{
      getData: function(){
        const data = axios.get('/question/{{$test}}');  
        return data;
      },
      getBackHome: function(){
      	window.location.href ="/home";
      },
      setTestGrade: function(correctQuestion,TotalAnswers){
      	let base = 10/TotalAnswers;
      	let grade  = correctQuestion * base;
      	this.grade = grade;
      	console.log(this.grade);
      	console.log({{ Auth::id() }});
      	this.$refs.myModal.style.display = "block";
      },
      closeModal: function(){
      	this.$refs.myModal.style.display = "none";
      },
      sendData: function(){
      	let sendData = {
      		id_user: {{ Auth::id() }},
      		id_test: this.test_id,
      		grade: this.grade
      	};
      	const data = axios.post('/record',sendData)
      	.then((response)=>{
      		console.log(response);
      	}).catch(function (error) {
			    // handle error
			    console.log(error);
			  });
      }
    }
  });
</script>
</body>
</html>