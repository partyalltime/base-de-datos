@extends('layouts.app')

@section('content')
<div class="container">
	<h3>Seleccione un examen de los disponibles</h3>
	<div class="row" id="quizes">
	</div>
</div>
<script type="text/javascript">
	/*
	*	Promise to get test 
	*	@param id
	*
	*	return JSON Object
	*/
	async function getData(id){
		const data = await $.ajax({
			url: '/getQuizes/{{$post}}',
			type: 'get',
			dataType: 'json'
		});
		return data;
	}

	function setQuizesElements(object){
		const container = document.getElementById('quizes');
		let elements = null;
		let mainCard = null;
		let flexContainer = null;
		let headerContainer = null;
		let headerTextDiv = null;
		let headerTitle = null;
		let footerContainer = null;
		let footerContent = null;
		let btnFooter = null;
		let footerScore = null;

		for (let value of Object.values(object)) {
		  mainCard = document.createElement("div");
		  mainCard.className  = "col-lg-4 col-sm-12 col-md-4 d-flex";
		  flexContainer = document.createElement("div");
		  flexContainer.className = "tile p-0 cursos flex-fill";
		  headerContainer = document.createElement("div");
		  headerContainer.className = "card-header bg-default flex-fill";
		  headerTextDiv = document.createElement("div");
		  headerTextDiv.className  = "card-category mb-2";
		  headerTextDiv.appendChild(document.createTextNode("Nombre del Examen"));
		  headerContainer.appendChild(headerTextDiv);
		  headerTitle = document.createElement("h3");
		  headerTitle.className = "title flex-grow-1";
		  headerTitle.appendChild(document.createTextNode(value.name));
		  headerContainer.appendChild(headerTitle);
		  footerContainer =  document.createElement("div");
		  footerContainer.className = "tile-footer p-4";
		  footerContent = document.createElement("div");
		  footerContent.className = "d-flex justify-content-between";
		  btnFooter = document.createElement("a");
		  btnFooter.className = "btn btn-outline-info btn-sm";
		  btnFooter.setAttribute("href","/test/"+value.id);
		  btnFooter.appendChild(document.createTextNode("Hacer Examen"));
		  footerContent.appendChild(btnFooter);
		  footerScore = document.createElement("p");
		  footerScore.className = "text-muted text-right mt-2";
		  footerScore.appendChild(document.createTextNode("Nota promedio: "+0));
		  footerContent.appendChild(footerScore);
		  footerContainer.appendChild(footerContent);
		  flexContainer.appendChild(headerContainer);
		  flexContainer.appendChild(footerContainer);
		  mainCard.appendChild(flexContainer);
		  container.appendChild(mainCard);
		  console.log(container);
		}
	}

	$(document).ready(function(){
		getData(28)
		.then((response) => {
			setQuizesElements(response);
		});
	});
</script>
@endsection