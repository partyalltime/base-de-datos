@extends('layouts.app')

@section('content')
<style>
.container {
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 2%;
}

.author {
    color: #aaa;
}
.date{
    float: right;
    color: #aaa;
}
</style>
    
    <div class="container">
        <div class="col-md-8">
            <div class="row ustify-content-center">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-title">
                            <h3>{{$comment[0]->subject}}</h3>
                        </div>
                        <div class="tile-body text-muted">
                            <p>{{$comment[0]->body}}<p>
                        </div> 
                        <div class="tile-footer text-muted">
                            <a class="author">{{$comment[0]->username}}</a>
                            <a class="date">{{$comment[0]->created_at}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-end">
            @foreach ($replies as $item)

                <div class="col-md-10">
                    <div class="tile">
                        <div class="tile-body text-muted">
                            <p>{{$item->body}}<p>
                        </div> 
                        <div class="tile-footer text-muted">
                            <a class="author">{{$item->username}}</a>
                            <a class="date">{{$item->created_at}}</a>
                        </div>
                    </div>
                </div>         
            @endforeach
            </div>
            <div class="row justify-content-end">
            <div class="col-md-10">
                <div class="tile">
                    <form method="POST" action="{{url('addReply')}}">
                        {{ csrf_field() }}
                        <div class="tile-body text-muted my-4">
                             <div class="form-group col-md-12">
                            <textarea style="height: 5em;" class="form-control line-head" name="reply-body" id="reply-body" placeholder="Escribe tu comentario aquí" autoresize></textarea>
                            </div>
                        </div>
                        <div class="mt-4 d-flex justify-content-end" style="overflow: hidden">
                            <input type="hidden" value="{{$comment[0]->id}}" id="comment-id" name="comment-id">
                            <button type="submit" class="btn btn-success" id="submmit-reply">Comentar</button>
                            </form>
                        </div>
                    </div>
                    </form>
            </div>
        </div>
        </div>
    </div>
    <script src="{{ asset('js/web/replies.js') }}"></script>
@endsection