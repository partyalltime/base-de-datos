@extends('layouts.tutorial')
@section('content')
<input type="hidden" id="userId" name="userId" value="{{Auth::user()->id}}">
<div class="container slider animated fadeIn slower">
  <div class="row justify-content-center">
    <div class="slides">
    @foreach ($items as $elem)
        <div class="slide-container" id="slide-item-{{$elem->id}}">
          <div class="col-md-10 col-sm-12 col-lg-10">
            <div class="tile">
              <div class="tile-body">
                  <h3 class="tile-title">{{$loop->index + 1}}. {{$elem->title}}</h3>
                  <div class="tile-body text-muted">{!!html_entity_decode($elem->body)!!}</div>
              </div>
              <div class="tile-footer text-muted mt-4" style="overflow: hidden">
                  @if($loop->index > 0)
                    <a class="btn footer-btn previous-item-btn" href="#slide-item-{{$items[$loop->index-1]->id}}" onClick="previousItem({{$elem->id}})">Anterior</a>
                  @endif
                  @if($loop->index < count($items)-1)
                    <a class="btn footer-btn next-item-btn" href="#slide-item-{{$items[$loop->index+1]->id}}" onClick="nextItem({{$items[$loop->index+1]->id}})">Siguiente</a>
                  @elseif($loop->index < count($items))
                    <a class="btn footer-btn next-item-btn" href="#" onClick="completeTutorial()">Completar</a>
                  @endif
              </div>
            </div>
          </div>
        </div>
    @endforeach
    </div>
  </div>
</div>
@endsection