<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
     <!-- Scripts -->
     
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/init.css') }}" rel="stylesheet">

    <!-- ********************* JS JQUERY LINKS ********************* -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{ asset('js/plugins/pace.min.js') }}"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="{{ asset('js/plugins/chart.js') }}"></script>
</head>
<body class="bg">

<div class="container">


  <div class="row full-height justify-content-center align-items-center">
    <div class="col-8">

        <div class="tile bg-transparent" style="width:20rem">            
            <div class="text-center mb-2">
                <img class="pr-2" height="25px" src="{{ asset('images/logo.png') }}"/>
            </div>

                    
            <div class="tile-footer">  
            <h3 class="tile-title text-center">¡BIENVENIDO!</h3>
            <div class="mb-3 tile-body lead text-center">
                El código es para vosotros, programadores, lo que los versos al poeta.
                
            </div>                  

                <div class="init">
                        <a class="button" href="login">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
                            <g fill="none" fill-rule="evenodd">

                               <circle class="inner-circle" cx="60" cy="60" r="39" fill="#f8f9fa" fill-opacity="0.1"/>
                                <text class="menu-button" y="50%" x="50%" font-family="Lato" font-size="15" fill="#9B9B9B" fill-rule="nonzero" text-anchor="middle" alignment-baseline="central">Entrar</text>

                               <g class="outer-circle">
                                  <circle cx="60" cy="60" r="53" stroke="#9B9B9B" transform="rotate(90 60 60)"/>
                                  <circle cx="60" cy="7" r="2" fill="#9B9B9B"/>
                                  <circle cx="60" cy="113" r="2" fill="#9B9B9B"/>
                                  <circle cx="113" cy="60" r="2" fill="#9B9B9B"/>
                                  <circle cx="7" cy="60" r="2" fill="#9B9B9B"/>
                               </g>

                            </g>
                        </svg>
                    </a>
                </div>

                

            </div>
        </div>
    </div>
    <div class="col-4"></div>
  </div>
</div>
   
</body>