@extends('layouts.app')
@section('content')
<div class="container">
	<h1>Entrevista con {{$user_name}}</h1>
	<br />
	<div>
		<input type="hidden" id="room_id" value="{{ $room_id }}" />
		<div id="video-conference"></div>
		<form id="form-interview" method="POST" action="{{ route('interviewDone') }}">
			@csrf
			<input type="hidden" name="user_id" value="{{ $user_id }}" />
			<br />
			<button type="button" id="stop-recording-interview" onclick="stopInterview()" class="btn btn-info">Terminar entrevista</button>
		</form>
	</div>
</div>
@endsection
<script src='https://meet.jit.si/external_api.js'></script>
<script type="text/javascript" src="{{ URL::asset('js/recordInterview.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/RecordScreen.js')}}"></script>