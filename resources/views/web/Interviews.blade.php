@extends('layouts.app')
@section('content')
<div class="container" id="component">
	<h1>Pendientes de entrevista</h1>
	<br />
	<div class="tile">
		<div class="tile-body">
			@if (count($interviews) > 0)
				<filter-component />	
			@else
				<div style="display: flex; justify-content: center;">
					<h3>No hay entrevistas pendientes ¡Buen trabajo!</h3>
				</div>
			@endif
		</div>
	</div>
</div>
@endsection

<script type="module">
	@if (app()->environment('production'))
    import Vue from 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.esm.browser.min.js';
  @else
    import Vue from 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.esm.browser.js';
	@endif

	let filterComponent = Vue.component('filter-component', {
			template: `
				<div>
					<select v-show="headquarters !== null" class="form-control" v-model="currentFilter">
						<option :value="0">Todas las sedes</option>
						<template v-for="headquarter in headquarters">
							<option :value="headquarter.id">@{{headquarter.name}}</option>
						</template>
					</select>
					<br />
					<table class="table table-hover table-striped table-condensed">
						<tbody>
							<template v-for="interview in interviewsByHeadquarter">
								<tr :key="interview.id">
									<td>@{{interview.name}}</td>
									<td style="float: right;">
										<form method="POST" action="{{ route('acceptInterview') }}">
											@csrf
											<input type="hidden" name="email" :value="interview.email"></input>
											<button type="submit" class="btn btn-info" style="color: white;">Comenzar entrevista</button>
										</form>
									</td>	
								</tr>
							</template>
						</tbody>
					</table>
				</div>
			`,
			beforeMount() {
				this.pendingInterviews = @json($interviews);
				this.headquarters = @json($headquarters);
			},
			data() {
				return {
					headquarters: [],
					pendingInterviews: [],
					currentFilter: 0,
				};
			},
			computed: {
				interviewsByHeadquarter() {
					return this.pendingInterviews.filter(interview => {
						return interview.headquarters_id === this.currentFilter || this.currentFilter === 0;
					});
				}
			},
		}
	);
	
	const app = new Vue({
		el: '#component',
		components: {
			'filter-component': filterComponent,
		},
	});
</script>