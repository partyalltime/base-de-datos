@extends('layouts.app')

@section('content')
<style>
.container {
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 2%;
}
</style>
    <!-- Modal -->
    <div class="modal" tabindex="-1" role="dialog" id="comment-form">
        <div class="modal-dialog" role="document" style="max-width: 700px;">
            <div class="modal-content">
                <div class="alert alert-danger" style="display:none"></div>
                <div class="modal-header">
                    <h5 class="modal-title" id="item-modal-title">Agregar comentario</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{url('addComment')}}">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                            <label for="comment-subject">Titulo:</label>
                            <input type="text" class="form-control" name="comment-subject" id="comment-subject">
                            </div>
                        </div>
                        <div class="row">
                        <div class="form-group col-md-12">
                            <label for="comment-body">Cuerpo:</label>
                            <textarea class="form-control" name="comment-body" id="comment-body" style="height: 100px; width: 100%"></textarea>
                        </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" value="{{$post_id}}" id="post-id" name="post-id">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Descartar</button>
                        <button type="submit" class="btn btn-success" id="submit-comment">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal End-->
    <div class="container">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#comment-form">Add comment</button>
    </div>
    <div class="container">
    <table class="table table-striped">
        <tbody>
            <tr>
                <th>Subject</th>
                <th>Posted on</th>
                <th>Replies</th>
            </tr>
            @foreach($comments as $elem)
                <tr>    
                    <td>
                        <a href="/repliesView/{{$elem->id}}">{{$elem->subject}}</a>
                        <div>by {{$elem->username}}</div>
                    </td>    
                    <td>
                        {{$elem->created_at}}       
                    </td>
                    <td>{{$elem->total_replies}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <script src="{{ asset('js/web/comments.js') }}"></script>
@endsection