@extends('layouts.app')
@section('content')
<style type="text/css">
  td {
    text-align: center;
  }
</style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12 animated fadeIn slower">
      <div class="row justify-content-center">
        <!--titulo -->
        <div class="col-lg-12 col-md-12 col-sm-12">
        </div>
        <!--fin titulo-->
        <div class="col-lg-12 col-md-12 col-sm-12 ">
          <div class="row">
            <div class="col-md-12">
              <div class="tile">
                <div class="row">
                  <div class="col-md-9">
                    <h1><i class="fa fa-th-list"></i>Prospectos pendientes de aprobación</h1>
                  </div>
                </div>
                <div class="tile-body">
                  <table class="table  table-hover table-striped table-condensed" id="sampleTable">
                    <thead>
                      <tr>
                        <th>
                          <p align="center">Nombre<p>
                        </th>
                        <th>
                          <p align="center">Aprobar/Denegar</p>
                        </th>

                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($prospect as $datos)
                      <tr>
                        <td>{{$datos->name}}</td>
                        <td>
                          <button type="button" class="btn btn-success" id="yes{{$datos->id}}" onclick="updateButtons(this, {{$datos->id}})">✔️</button>
                          <button type="button" class="btn btn-danger" id="no{{$datos->id}}" onclick="updateButtons(this, {{$datos->id}})">✖️</button>
                        </td>
                        <td>
                          <button class="btn btn-primary update-btn" prospect-id="{{$datos->id}}" style="color: white" id="update{{$datos->id}}" disabled>Actualizar</button>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- fin row -->
    </div>
  </div>
</div>
</div>
<script>
  
</script>
<script src="{{ asset('js/web/updatePending.js') }}"></script>
@endsection