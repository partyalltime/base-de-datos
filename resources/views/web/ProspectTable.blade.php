@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row justify-content-center">
    <div class="col-md-12 animated fadeIn slower">
      <div class="row justify-content-center">
	      <!--titulo -->
	      <div class="col-lg-12 col-md-12 col-sm-12">
	        
	        
	      </div>
        <!--fin titulo-->
        <div class="col-lg-12 col-md-12 col-sm-12 ">
          
          <div class="row">

            <div class="col-md-12">
              <div class="tile">
                  <div class="row">
                    <div class="col-md-9">
                        <h1><i class="fa fa-th-list"></i>Prospectos</h1>
                    </div>
                    <div class="col-md-3">
                    <a id="change-view-btn" class="btn btn-primary icon-btn" href="/newIntern/view">
                            <i class="fa fa-th-list"></i>
                            Mostrar nuevos becarios
                        </a>
                    </div>
                </div>
                <div class="tile-body">
                  <table class="table  table-hover table-striped table-condensed" id="sampleTable">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Experiencia de programador</th>
                        <th>Experiencia informática</th>
                        <th>Experiencia remarcable</th>
                        <th>Conocimiento de programación</th>
                        <th>Conocimiento informático</th>
                        <th>Documentos</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($prospects as $datos)
                      <tr>
                        <td>{{$datos->name}}</td>
                        <td>{{$datos->programing_experience}}</td>
                        <td>{{$datos->informatics_experience}}</td>
                        <td>{{$datos->remarkable_experience}}</td>
                        <td>{{$datos->programing_knowledge}}</td>
                        <td>{{$datos->informatics_knowledge}}</td>
                        <td><button onClick="print({{$datos->user_id}})" class="btn btn-outline-info">Imprimir documentos</button></td>
                        <td>
                          @if(is_null($datos->new_intern))
                            <a class="btn btn-success scholarship" data-thisId="{{$datos->id}}" style="width: 100%">Nuevo becario</a>
                          @else
                            <a class="btn btn-warning"  style="color: white; width: 100%">Ya es becario</a>
                          @endif
                        </td>
                      </tr>
                      @endforeach  
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    	</div>
    <!-- fin row -->
    </div>
  </div>
</div>
 
  <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
  <script type="text/javascript">
  	$('.scholarship').each(function () {
		    var $this = $(this);
		    $this.on("click", function () {
		    	var these = $(this);
		    	doConfirm("¿Desea confirmar la beca de este prospecto?", function yes() {
		    		window.location.href =`/newIntern/create/${these.data('thisid')}`
		    		/*updateIntern(these.data('thisid')).then(response => {
		    			toastFunction("Cambio realizado con exito");
		    			console.log(response)
		    		}).catch(error => console.log(error));*/
	        }, function no() {
	            // do nothing
	        });
		        
		    });
		});
  	/*$(".scholarship").on('click',function(){
  		const id = this.dataset.thisId;
  		doConfirm("¿Desea confirmar la beca de este prospecto?", function yes() {
            alert($(".scholarship").data("thisId"));
            console.log($(".scholarship"));
        }, function no() {
            // do nothing
        });
  	});*/
  async function updateIntern(id){
  	const data = await $.ajax({
  		url: `/newIntern/create/${id}`,
  		type: 'get',
  		dataType: 'json',
  	});
  	return data;
  } 

	function getImgFromUrl(logo_url, callback) {
    const img = new Image();
    img.src = logo_url;
    img.onload = function () {
        callback(img);
    };
	}
	function getImgFromUrl2(logo_url, callback) {
    const img2 = new Image();
    img2.src = logo_url;
    img2.onload = function () {
        callback(img2);
    };
	}
	async function getSchedules(id){
	
		const called = await $.ajax({
			url: '/generate-pdf2/'+id,
			type: 'get',
			dataType: 'json',
			/*headers: {
	      'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
	  	},*/
			});

		return called;
	}

	function generatePDF(img,data){
		//console.log(data);
    const options = {format: 'a4'};
    const doc = new jsPDF(options);
		doc.setTextColor(1, 138, 200);
		doc.text('PREINSCRIPCIÓN Y RESERVA DE PLAZAS',200, 25,{align: 'right'});
		doc.setFontSize(10);
		/* text('Text', X, Y) */
		/*line(x1,y1,x2,y2)*/
		/* Beggining of text fields */
		doc.text('GRADOS OFICIALES',200, 30,{align: 'right'});
		doc.text("Nombre y Apellidos:",15,50);
		doc.line(47, 51, 200, 51, 'S')
		doc.text("DNI / NIE o Pasaporte:",15,58);
		doc.line(51, 59, 103, 59, 'S')
		doc.text("Fecha de Nacimiento (dd/mm/aaaa):",105,58);
		doc.line(163, 59, 200, 59, 'S')
		doc.text("Nacionalidad:",15,64);
		doc.line(37, 65, 200, 65, 'S')
		doc.text("Dirección:",15,70);
		doc.line(31, 71, 200, 71, 'S')
		doc.text("Población / Ciudad:",15,76);
		doc.line(46, 77, 93, 77, 'S')
		doc.text("País:",95,76);
		doc.line(103, 77, 143, 77, 'S')
		doc.text("Código Postal:",145,76);
		doc.line(168, 77, 200, 77, 'S')
		doc.text("E-mail:",15,82);
		doc.line(26, 83, 200, 83, 'S')
		doc.text("Teléfono de contacto: ",15,88);
		doc.line(50, 89, 200, 89, 'S')
		doc.text("GRADOS OFICIALES ",15,105);
		doc.setTextColor(0, 0, 0);
		doc.text("ingeniería Informática ",21,111);
		
		//Para agregar checkBox
		var checkBox = new jsPDF.API.AcroFormCheckBox();
        checkBox.fieldName = "CheckBox2";
        checkBox.x = 15;
        checkBox.y = 107;
        checkBox.width = 5;
        checkBox.height = 5;
        checkBox.maxFontSize = 20;
		//checkBox.readOnly = true;
        doc.addField(checkBox);


		//doc.text(data.other_studies,25,115);		
		/* Ending of text fields */
		doc.setTextColor(0, 0, 0);
		/* Here starts information */
		if(data.name !== null && typeof data.name !== undefined){
			doc.text(data.name.toString(),50,50);

		}
		if(data.address !== null && typeof data.address !== undefined){
			doc.text(data.address.toString(),40,70);

		}
		if(data.city !== null && typeof data.city !== undefined){
			doc.text(data.city.toString(),50,76);

		}
		if(data.postal_code !== null && typeof data.postal_code !== undefined){
			doc.text(data.postal_code.toString(),170,76);

		}
		if(data.identification_number !== null && typeof data.identification_number !== undefined){
			doc.text(data.identification_number.toString(),55,58);

		}
		if(data.birthday !== null && typeof data.birthday !== undefined){
			doc.text(data.birthday.toString(),165,58);

		}
		if(data.phone !== null && typeof data.phone !== undefined){
			doc.text(data.phone.toString(),55,88);

		}
		if(data.email !== null && typeof data.email !== undefined){
			doc.text(data.email.toString(),30,82);

		}if(data.en_short_namel !== null && typeof data.en_short_name!== undefined){
			doc.text(data.en_short_name,105,76);
		}
		
		

    doc.addImage(img, 'JPEG', 15, 15, 50, 15);
		doc.save('solicitud-beca-de-colaboración');
  } 
	
	
	
	async function getSchedules2(){
	
		const called = await $.ajax({
			url: '/generate-pdf2',
			type: 'get',
			dataType: 'json',
			/*headers: {
	      'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
	  	},*/
			});

		return called;
	}

	function generatePDF2(img,data){
		//console.log(data);
    const options2 = {format: 'a4'};
    const doc2 = new jsPDF(options2);
		doc2.setTextColor(1, 138, 200);
		doc2.text('BECAS FUNIBER-RESIDENCIA UNIVERSITARIA',200, 25,{align: 'right'});
		doc2.setFontSize(10);
		/* text('Text', X, Y) */
		/*line(x1,y1,x2,y2)*/
		/* Beggining of text fields */
		doc2.text('Impreso de solicitud de Beca',15, 43);
		doc2.text("Nombre:",15,50);
		doc2.line(29, 51, 200, 51, 'S')
		doc2.text("DNI/NIE o Pasaporte:",15,58);
		doc2.line(50, 59, 103, 59, 'S')
		doc2.text("Fecha de Nacimiento (aaaa/mm/dd):",105,58);
		doc2.line(163, 59, 200, 59, 'S')
		doc2.text("Nacionalidad:",15,64);
		doc2.line(37, 65, 200, 65, 'S')
		doc2.text("Dirección:",15,70);
		doc2.line(31, 71, 200, 71, 'S');
		doc2.text("Población/Ciudad:",15,76);
		doc2.line(44, 77, 93, 77, 'S');
		doc2.text("País:",95,76);
		doc2.line(103, 77, 143, 77, 'S');
		doc2.text("CP:",145,76);
		doc2.line(151, 77, 200, 77, 'S');
		doc2.text("E-mail:",15,82);
		doc2.line(26, 83, 200, 83, 'S');
		doc2.text("Teléfono de contacto: ",15,88);
		doc2.line(50, 89, 200, 89, 'S')
		doc2.text("Situación laboral: ",15,94);
		doc2.line(42, 95, 200, 95, 'S')
		doc2.text("Nivel de estudios actual (indicar):",15,102);
		doc2.line(15, 108, 200, 108, 'S');
		doc2.line(15, 116, 200, 116, 'S');
		doc2.text("Centro donde han sido cursados:", 15, 123);
		doc2.line(68, 123, 159, 123, 'S');
		doc2.text("Nota media obtenida:", 161, 123);
		doc2.line(193, 123, 200, 123, 'S');
		doc2.text("Preinscripción en los estudios de:", 15, 134);
		doc2.line(68, 134, 200, 134, 'S');
		doc2.text("Fecha inicio estudios en la Universidad:", 15, 144);
		doc2.line(78, 144, 200, 144);
		let checkbox2 = new jsPDF.API.AcroFormCheckBox();
		checkbox2.Rect = [50, 10, 20, 20];
		
		/* Ending of text fields */
		doc2.setTextColor(0, 0, 0);
		/* Here starts information */
		if(data.name !== null && typeof data.name !== undefined){
			doc2.text(data.name.toString(),30,50);
		}
		if(data.address !== null && typeof data.address !== undefined){
			doc2.text(data.address.toString(),50,70);
		}
		if(data.city !== null && typeof data.city !== undefined){
			doc2.text(data.city.toString(),50,76);
		}
		if(data.postal_code !== null && typeof data.postal_code !== undefined){
			doc2.text(data.postal_code.toString(),152,76);

		}
		if(data.identification_number !== null && typeof data.identification_number !== undefined){
			doc2.text(data.identification_number.toString(),50,58);
		}
		if(data.birthday !== null && typeof data.birthday !== undefined){
			doc2.text(data.birthday.toString(),165,58);
		}
		if(data.phone !== null && typeof data.phone !== undefined){
			doc2.text(data.phone.toString(),50,88);
		}
		if(data.studies !== null && typeof data.studies !== undefined){
			doc2.text(data.studies.toString(),15,107);

		}
		if(data.average_grade !== null && typeof data.average_grade !== undefined){
			doc2.text(data.average_grade.toString(),193,122);

		}
		if(data.studies_center !== null && typeof data.studies_center !== undefined){
			doc2.text(data.studies_center.toString(),69,122);

		}
		if(data.other_studies !== null && typeof data.other_studies !== undefined){
		doc2.text(data.other_studies.toString(),15,115);

		}
		if(data.email !== null && typeof data.email !== undefined){
		doc2.text(data.email.toString(),30,82);

		}
		if(data.country !== null && typeof data.country !== undefined){
			doc2.text(data.country.toString(),4,604);

		}
		if(data.en_short_namel !== null && typeof data.en_short_name!== undefined){
			doc2.text(data.en_short_name,105,76);
			
		}
		doc2.text("Ingenieria Informática",69,133);
		doc2.text("Estudiante",45,94);
		/* Here ends information */
    doc2.addImage(img, 'JPEG', 15, 15, 50, 15);
		doc2.save('solicitud-FUNIBER-RESIDENCIA');
  } 
	
	function print(id){
		getSchedules(id)
  	.then((data) => {
  		const logo_url = "{{ URL::asset('assets/img/logo.png')}}";
			getImgFromUrl(logo_url, function (img) {
			    generatePDF(img,data);

			});
			getImgFromUrl2(logo_url, function (img) {
			    generatePDF2(img,data);
			});
  		//console.log(JSON.stringify(data),"datos");
  		//console.log(schedules);
  	})
  	.catch((err)=>{
  		console.log(err)
  	});
  	
		/**/
	}
	
</script>
  @endsection
