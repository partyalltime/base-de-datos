@extends('layouts.app')
@section('content')
<script type="text/javascript">
  function sendData(id){
    if(id !== ''){
      $.ajax({
        type: 'put',
        url: '/prospect/'+id,
        headers: {
          'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
        },
        data: {
          'programing_experience': $('#programing_experience').val(),
          'informatics_experience':$('#informatics_experience').val(),
          'remarkable_experience':$('#remarkable_experience').val(),
          'programing_knowledge':$('#programing_knowledge').val(),
          'informatics_knowledge':$('#informatics_knowledge').val(),
          'user_id':1
        },
        success: function(response){
          console.log(response);
        }
      }); 
    }
    else{
      $.ajax({
        type: 'post',
        url: '/prospect',
        headers: {
          'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
        },
        data: {
          'programing_experience': $('#programing_experience').val(),
          'informatics_experience':$('#informatics_experience').val(),
          'remarkable_experience':$('#remarkable_experience').val(),
          'programing_knowledge':$('#programing_knowledge').val(),
          'informatics_knowledge':$('#informatics_knowledge').val(),
          'user_id':1
        },
        success: function(response){
          console.log(response);
        }
      });  
    }
    
  }
  $(document).ready(function(){
    $('#send').on('click',function(){
      sendData($("#id").val());
    });
  });
  
</script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 animated fadeIn slower">
            <div class="row justify-content-center">
              <!--titulo -->
              
              <div class="col-lg-12 col-md-12 col-sm-12">    
               <h1> Nuevo Prospecto</h1>
              </div>
              <!--fin titulo-->
              <div class="col-lg-12 col-md-12 col-sm-12 ">
                <div class="card  border-secondary ">
                  
                  <div class="card-body">
                    {{Form::hidden('id',(isset($prospect)) ? $prospect->id : '' , array('id' => 'id'))}}
                    <div class="form-group">
                      {{Form::label('programing_experience', 'Experiencia programador', array('class'=>'nel'))}}
                      {{Form::text('programing_experience',(isset($prospect))  ? $prospect->programing_experience : '',array('class' => 'form-control','placeholder'=>'Cursos, Trabajos,etc.','id'=>'programing_experience'))}}
                    </div>
                    <div class="form-group">
                      {{Form::label('informatics_experience','Experiencia informática')}}
                      {{Form::text('informatics_experience',(isset($prospect)) ? $prospect->informatics_experience : '',array('class'=>'form-control','placeholder'=>'Cursos de ofimatica, proyectos de escuela...','id'=>'informatics_experience'))}}
                    </div>
                    <div class="form-group">
                      {{Form::label('remarkable_experience','Experiencia remarcable')}}
                      {{Form::text('remarkable_experience',(isset($prospect)) ? $prospect->remarkable_experience : '',array('class'=>'form-control','placeholder'=>'Trabajos, proyectos individuales','id'=>'remarkable_experience'))}}
                    </div>
                    <div class="form-group">
                      {{Form::label('programing_knowledge','Conocimiento de programación')}}
                      {{Form::text('programing_knowledge',(isset($prospect)) ? $prospect->programing_knowledge : '',array('class'=>'form-control','placeholder'=>'Lenguajes de programación: php, javascript, ruby, java, c#, etc.','id'=>'programing_knowledge'))}}
                    </div>
                    <div class="form-group">
                      {{Form::label('informatics_knowledge','Conocimiento informático')}}
                      {{Form::text('informatics_knowledge',(isset($prospect)) ? $prospect->informatics_knowledge : '',array('class'=>'form-control','placeholder'=>'ofimatica, cmd, ect.','id'=>'informatics_knowledge'))}}
                    </div>
                    {{Form::close()}}
                    <input type="submit" class="btn btn-primary" id="send" text="enviar"/>
                  </div>
                </div>
              </div>

        </div>
        <!-- fin row -->
        </div>
      </div>
    </div>
  </div>
  @endsection
