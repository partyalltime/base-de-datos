@extends('layouts.app')
@section('content')
<script type="text/javascript">

function limpiar(){
            $('.bootstrap-datetimepicker-widget').css("display", "block");
          }
</script>
<style type="text/css">
	td{
		 text-align: center;
	}
</style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12 animated fadeIn slower">
      <div class="row justify-content-center" >
        <!--titulo -->
        <div class="col-lg-12 col-md-12 col-sm-12">
        </div>
        <!--fin titulo-->
        <div class="col-lg-12 col-md-12 col-sm-12 ">
          <div class="row">
            <div class="col-md-12">
              <div class="tile">
                <div class="row">
                  <div class="col-md-9">
                    <h1><i class="fa fa-th-list"></i>Nuevos Becarios</h1>
                  </div>
                	<div class="col-md-3">
                    <a id="change-view-btn" class="btn btn-primary icon-btn" href="/prospect/view">
                      <i class="fa fa-th-list"></i>
                      Mostrar prospectos
                    </a>
                  </div>
                </div>
	              <div class="tile-body">
	                <table class="table  table-hover table-striped table-condensed table-responsive " id="sampleTable">
	                  <thead>
	                    <tr>
	                      <th>Nombre</th>
	                      <th>Llegada</th>
	                      <th>Nivel de experiencia</th>
	                      <th>Compañia</th>
	                      <th>Matriculado</th>
	                      <th>Registrado anteriormente</th>
	                     {{--  <th>Beca concedida</th> --}}
	                      <th>Notificado</th>
	                      <th>Porcentaje</th>
	                      <th></th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                    @foreach($new_intern as $datos)
	                    <tr>
                        <td>{{$datos->name}}</td>
                        <td>
	                        <div class="col-md-2" >
	                        	<div class="input-group date selector1" id="datetimepicker{{$datos->id}}" data-target-input="nearest">
	                          	<input type="text" class="form-control" id="arrival-{{$datos->id}}" value="{{$datos->arrival}}" >
	                          	<div  class="input-group-append" data-target="#datetimepicker{{$datos->id}}" data-toggle="datetimepicker">
	                            	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                          	</div>
	                        	</div>
	                        </div>
                        </td>
                        <td>                                
                          <input class="form-control input-group-text" type="text" id="level-{{$datos->id}}" value="{{$datos->level}}">
                        </td>
                        <td>{{$datos->internship}}</td>
                        <td>
	                        @if($datos->enrolled == 1)
	                          <input class="form-check-input" type="checkbox" id="enrolled-{{$datos->id}}" checked>
	                        @else
	                          <input class="form-check-input" type="checkbox" id="enrolled-{{$datos->id}}">
	                        @endif
                        </td>
                        <td>
                        	@if($datos->pre_registered == 1)
	                          <input class="form-check-input" type="checkbox" id="pre_registered-{{$datos->id}}" checked>
	                        @else
	                          <input class="form-check-input" type="checkbox" id="pre_registered-{{$datos->id}}">
	                        @endif
                        </td>
                        {{-- <td>
	                        @if($datos->granted == 1)
	                          <input class="form-check-input" type="checkbox" id="granted-{{$datos->id}}" checked>
	                        @else
	                          <input class="form-check-input" type="checkbox" id="granted-{{$datos->id}}">
	                        @endif
                        </td> --}}
                        <td>
	                        @if($datos->notified == 1)
	                          <input class="form-check-input" type="checkbox" id="notified-{{$datos->id}}" checked>
	                        @else
	                          <input class="form-check-input" type="checkbox" id="notified-{{$datos->id}}">
	                        @endif
                        </td>
                        <td>{{$datos->scholarship}}%</td>
                        <td>
                          <a class="btn btn-primary update-btn" new-intern-id="{{$datos->id}}" style="color: white">Actualizar</a>
                        </td>
	                    </tr>
	                    @endforeach  
	                  </tbody>
	                </table>
	              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- fin row -->
    </div>
  </div>
</div>
  
  <script type="text/javascript">
    $(function () {
        $('.selector1').each(function(){
            $("#"+$(this)[0].id).datetimepicker({"format": "YYYY-MM-DD"});
        });
    });
    </script>
  <script src="{{ asset('js/web/updateNewIntern.js') }}"></script>
  @endsection
