@extends('layouts.app')
@section('content')
<link href="{{ asset('css/web/process.css') }}" rel="stylesheet" >
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
<!-- INICIO STEP BOOTSTRAP -->
<a id="anchor" style="top: 0; position: absolute"></a>
<div id="main-container" class="card text-center" style="min-height: 50vh; min-width: 20vw; width 100%">
  <div class="card-body" style="text-align: center; margin: auto; width: 60%">
    <div class="title-process">
    	<h2>Tu proceso para entrar a UNEATLÁNTICO</h2>
    </div>
      {{ Form::hidden('invisible', session('type'), array('id' => 'toast-activator')) }}
      <!-- Primer paso -->

      <div class="row setup-content {{ ($prospect->process_state== 1)  ? 'first-step' : '' }}" id="step-1" style="display: inline-block;">
        <div class="col-xs-9 col-md-12">
          <div class="row">
            <div class="col-md-12">
              <h1>Enviaremos un correo al encargado de tu sede de FUNIBER</h1>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4" style="margin: auto">
              {{-- <button type="submit" class="btn btn-outline-primary nextBtn btn-lg pull-right" style="width: 100%">Enviar</button> --}}
            </div>
          </div>
        </div>
      </div>
      <!-- Fin primer paso -->

      <!-- Segundo paso -->
      <div class="row setup-content {{ ($prospect->process_state== 2)  ? 'first-step' : '' }}" id="step-2" style="display: inline-block; width: 100%">
        <div class="col-xs-9 col-md-12">
          <div class="row">
            <div class="col-md-12">
              <h4 id="CVcreate" class="text-dark"> Crea tu CV en <a href="https://europass.cedefop.europa.eu/editors/es/cv/compose" target="_blank">Europass</a> y expórtalo en PDF</h4>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <form enctype="multipart/form-data" action="{{ route('uploadCV')}}" method="post">
                @csrf
                <div class="form-group">
                  <input type="file" id="cv" class="inputfile" accept=".pdf" name="cv" required="required"/>
                  @if(isset($data->documents->CV))
                  <label for="cv"><i class="fa fa-cloud-upload"></i>{{$data->documents->CV}}</label>
                  @else
                  <label for="cv"><i class="fa fa-cloud-upload"></i>Sube tu CV</label>
                  @endif
              		<h6 id="CVupload" class="text-datk">Sube tu CV en PDF al sistema</h6>
                  <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                  <input type="hidden" name="extension" value="pdf" />
                  <input type="hidden" name="type" value="cv" />
                </div>
                <div class="form-group">
                  <button class="btn btn-primary" type="submit">Subir</button>
                </div>
              </form>
            </div>
          </div>
          <div class="row justify-content-between">
          	<div class="col no-float ">
          		{{-- <button class="btn btn-outline-primary nextBtn btn-lg pull-right" type="button">Siguiente</button> --}}
          	</div>
          </div>

          <!-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left" type="button">Atrás</button> -->
        </div>
      </div>
      <!-- Fin segundo paso -->

      <!-- Tercer paso -->
      <div class="row setup-content {{ ($prospect->process_state== 3)  ? 'first-step' : '' }}" id="step-3" style="display: inline-block; width: 100%">
        <div class="col-xs-9 col-md-100 sign-up-form">
          <div class="row">
            <div class="col-md-100">
              <h3 id="CVcreate" class="text-dark">Preinscripción y reserva de plaza. Grados Oficiales</h3>
            </div>
          </div>
          <form method="post" action="{{ route('sendForm')}}" accept-charset="UTF-8">
            @csrf
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <!--parte 1-->
            @include('Process.SignUpForm')
            <br />
          </form>
          		{{-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left"   type="button">Atrás</button>
          		<button class="btn btn-outline-primary nextBtn btn-lg pull-right" type="button">Siguiente</button> --}}
        </div>
      </div>
      <!-- Fin tercer paso -->
      <!-- Cuarto paso -->
      <div class="row setup-content {{ ($prospect->process_state== 4)  ? 'first-step' : '' }}" id="step-4" style="display: inline-block; width: 100%">
        <div class="col-xs-12 col-md-12">
          <form method="post" action="{{ route('sendForm')}}" accept-charset="UTF-8">
            @csrf
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <!--parte 1-->
            <div class="titles">
            
              @include('Process.InternshipForm')

            </div>
          </form>
          {{-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left"   type="button">Atrás</button>
          <button class="btn btn-outline-primary nextBtn btn-lg pull-right"   type="button">Siguiente</button> --}}
        </div>
      </div>
      
      <!-- Fin cuarto paso --> 
      <!-- Inicio quinto paso -->
      <div class="row setup-content {{ ($prospect->process_state== 5)  ? 'first-step' : '' }}" id="step-5" style="display: inline-block; width: 100%">
        <div class="col-xs-9 col-md-100">
          <form method="post" action="{{ route('sendForm')}}" accept-charset="UTF-8">
            @csrf
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

            <!--parte 1-->
            <div>
              @include('Process.ResidencyInternshipForm')
            </div>

            <br />
          </form>
          {{-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left" type="button">Atrás</button>
          <button class="btn btn-outline-primary nextBtn btn-lg pull-right" type="button">Siguiente</button> --}}
        </div>
      </div>
      <!-- Fin quinto paso -->
			<!-- Inicio Sexto paso-->
			<div class="row setup-content {{ ($prospect->process_state== 6)  ? 'first-step' : '' }}" id="step-6" style="display: inline-block; width: 100%">
        <div class="col-xs-9 col-md-100">
          @include('Process.Identification')
          {{-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left" type="button">Atrás</button>
          <button class="btn btn-outline-primary nextBtn btn-lg pull-right" type="button">Siguiente</button> --}}
        </div>
      </div>
			<!-- Fin sexto paso-->
			<!-- Inicio Septimo paso-->
			<div class="row setup-content {{ ($prospect->process_state== 7)  ? 'first-step' : '' }}" id="step-7" style="display: inline-block; width: 100%">
        <div class="col-xs-9 col-md-100">
          @include('Process.Grades')
          {{-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left" type="button">Atrás</button>
          <button class="btn btn-outline-primary nextBtn btn-lg pull-right" type="button">Siguiente</button> --}}
        </div>
      </div>
			<!-- Fin séptimo paso-->
			<!-- Inicio Octavo paso-->
			<div class="row setup-content {{ ($prospect->process_state== 8)  ? 'first-step' : '' }}" id="step-8" style="display: inline-block; width: 100%">
        <div class="col-xs-9 col-md-100">
          @include('Process.Passport')
          {{-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left" type="button">Atrás</button>
          <button class="btn btn-outline-primary nextBtn btn-lg pull-right" type="button">Siguiente</button> --}}
        </div>
      </div>
			<!-- Fin octavo paso-->
      <!--Inicio Noveno paso -->
      <div class="row setup-content {{ ($prospect->process_state== 9)  ? 'first-step' : '' }}" id="step-9" style="display: inline-block; width: 100%">
        <div class="col-xs-6 col-md-offset-3">
          <div class="col-md-12">

            <div>
              <h3 id="CVupload" class="text-datk">Selecciona un rango de fechas para tu entrevista</h3>
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card  border-secondary ">

                  <div class="card-body">
                    <div class="row">
                      <div class='col-md-5'>
                        <div class="form-group">
                          <h3>Fecha inicial</h3>
                          <div class="input-group date" id="datetimepicker7" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker7" /> 
                            <div class="input-group-append" data-target="#datetimepicker7" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2"></div>
                      <div class='col-md-5'>
                        <h3>Fecha final</h3>
                        <div class="form-group">
                          <div class="input-group date" id="datetimepicker8" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker8" />
                            <div class="input-group-append" data-target="#datetimepicker8" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <button id="test" class="btn btn-outline-info">Guardar Fecha</button>
                  </div>
                </div>
              </div>
            </div>

            {{-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left" type="button">Atrás</button>
            <button class="btn btn-outline-primary nextBtn btn-lg pull-right" type="button">Siguiente</button> --}}
          </div>
        </div>
      </div>
      <!-- Fin noveno paso -->

      <!-- Décimo paso -->
      <div class="row setup-content {{ ($prospect->process_state== 10)  ? 'first-step' : '' }}" id="step-10" style="display: inline-block; width: 100%">
        <div class="col-xs-6 col-md-offset-3">
          <div class="col-md-12">
            <div>
              <input type="hidden" id="room_id" value="{{ $prospect->interview_room }}" />
              <h3 id="CVupload" class="text-datk">Entrevista</h3>
              @if ($prospect->interview_room !== NULL)
                <div id="video-conference" style="display: flex;"> </div>
              <br />
              @else
                <h6>Se te enviara un correo cuando la entrevista este lista.</h6>
              @endif
              @if ($prospect->requested_interview === 0)
                <form method="POST" action="{{ route('requestInterview') }}">
                  @csrf
                  <input type="hidden" name="user_id" value="{{ $prospect->user_id }}" />
                  <button class="btn btn-success" id="start-recording">Solicitar entrevista</button>
                </form>
              @endif
              <br />
            </div>

            {{-- <button class="btn btn-outline-primary prevBtn btn-lg pull-left" type="button">Atrás</button>
            <button class="btn btn-outline-primary nextBtn btn-lg pull-right" type="button">Siguiente</button> --}}
          </div>
        </div>
      </div>
      <!-- Fin décimo paso -->

      <!-- Onceavo paso -->
      <div class="row setup-content {{ ($prospect->process_state== 11)  ? 'first-step' : '' }}" id="step-11" style="display: inline-block; width: 100%">
        <div class="col-xs-6 col-md-offset-3">
          <div class="col-md-12">


            <div>
              <h3 id="CVupload" class="text-datk">Fin del proceso</h3>
              <h5> Gracias por completar el proceso, en breve nos comunicaremos contigo.</h5>

            </div>

            <form action="{{ route('triggerEmail')}}" method="post">
              @csrf
              <input type="hidden" name="id" value="9" />
              <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
              <button class="btn btn-outline-success btn-lg pull-right end-btn">Finalizar</button>
            </form>

          </div>

        </div>

      </div>
      <!-- Fin Onceavo paso -->
      

  </div>
  <div class="card-footer">
  	<div class="container">
  		<div class="row">
  			<div class="col">
  				<button id="prev" class="btn btn-outline-primary btn-lg">Anterior</button>
  			</div>
  			<div class="col">
  				<button id="next" class="btn btn-outline-primary btn-lg">Siguiente</button>
  			</div>
  		</div>
  	</div>
  </div>

</div>
<br>
<!-- Timeline se encuentra aqui -->
<div class="stepwizard setup-panel">

  <div class="step" data-step="0">

    <!-- <a href="#step-1" type="button" class="btn btn-primary btn-circular">1</a> -->
    <p for="step-1" class="not"><a href="#step-1">Enviar Correo</a></p>

    <!-- <p for="step-1">Description</p> -->
    <p for="step-2" class="not"><a href="#step-2">CV</a></p>
    <p for="step-3" class="not"><a href="#step-3">Pre-inscripción</a></p>
    <p for="step-4" class="not"><a href="#step-4">Beca Colaboración</a></p>
    <p for="step-5" class="not"><a href="#step-5">Beca Residencia</a></p>
    <p for="step-6" class="not"><a href="#step-6">Documento de identificación</a></p>
    <p for="step-7" class="not"><a href="#step-7">Notas</a></p>
    <p for="step-8" class="not"><a href="#step-8">Pasaporte</a></p>
    <p for="step-9" class="not"><a href="#step-9">Fecha entrevista</a></p>
    <p for="step-10" class="not"><a href="#step-10">Grabar entrevista</a></p>
    <p for="step-11" class="not"><a href="#step-11">Fin</a></p>
  </div>

  <!-- <div class="stepwizard-step">
          <a href="#step-1" type="button" class="btn btn-primary btn-circular">1</a>
          <p>1. Crear CV</p>
        </div>
        <div class="stepwizard-step">
          <a href="#step-2" type="button" class="btn btn-default btn-circular" disabled>2</a>
          <p>2. Subir CV</p>
        </div>
        <div class="stepwizard-step">
          <a href="#step-3" type="button" class="btn btn-default btn-circular" disabled="disabled">3</a>
          <p>3. Fecha entrevista</p>
        </div>
        <div class="stepwizard-step">
          <a href="#step-4" type="button" class="btn btn-default btn-circular" disabled="disabled">4</a>
          <p>4. Grabar entrevista</p>
        </div>
        <div class="stepwizard-step">
          <a href="#step-5" type="button" class="btn btn-default btn-circular" disabled="disabled">5</a>
          <p>5. Fin</p>
        </div> -->
</div>
<!-- Timeline finaliza aqui -->
<!-- FIN STEPS BOOTSTRAP -->
<script type="text/javascript" src='https://meet.jit.si/external_api.js'></script>
<script type="text/javascript" src="{{ URL::asset('js/recordInterview.js')}}"></script>
<script src='https://meet.jit.si/external_api.js'></script>
<script src="{{ asset('js/web/process.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<footer style="margin-top: 20px; text-align: left;"><small id="send-message"></small></footer>
<script src="{{ asset('js/main.js') }}"></script>
<script>
if (window.addEventListener) {
  window.addEventListener('load', function() {
    let type = document.getElementById('toast-activator').value;
    if (type === '1') {
      toastFunction('Archivo subido exitosamente.');
    }
  });
}
</script>
@endsection