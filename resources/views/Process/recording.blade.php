@extends('layouts.app')
@section('content')

<style>
    html, body {
        margin: 0!important;
        padding: 0!important;
        text-align: center;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        font-size: 1em;
    }
    video {
        width: 30%;
        border-radius: 5px;
        border: 1px solid black;
    }
</style>

<div class="container">

<video controls autoplay playsinline style="width: 40%;"></video>

</div>

<script type="text/javascript" src="{{ URL::asset('js/RecordRTC.js')}}"></script>
<script>
if(!navigator.getDisplayMedia && !navigator.mediaDevices.getDisplayMedia) {
    var error = 'Your browser does NOT support getDisplayMedia API.';
    document.querySelector('h1').innerHTML = error;
     document.querySelector('video').style.display = 'none';
     document.getElementById('btn-start-recording').style.display = 'none';
     document.getElementById('btn-stop-recording').style.display = 'none';
    throw new Error(error);
}
function invokeGetDisplayMedia(success, error) {
    var displaymediastreamconstraints = {
        video: {
            displaySurface: 'monitor', // monitor, window, application, browser
            logicalSurface: true,
            cursor: 'always' // never, always, motion
        }
    };
    // above constraints are NOT supported YET
    // that's why overridnig them
    displaymediastreamconstraints = {
        video: true
    };
    if(navigator.mediaDevices.getDisplayMedia) {
        navigator.mediaDevices.getDisplayMedia(displaymediastreamconstraints).then(success).catch(error);
    }
    else {
        navigator.getDisplayMedia(displaymediastreamconstraints).then(success).catch(error);
    }
}
function captureScreen(callback) {
    invokeGetDisplayMedia(function(screen) {
        addStreamStopListener(screen, function() {
            if(window.stopCallback) {
                window.stopCallback();
            }
        });
        callback(screen);
    }, function(error) {
        console.error(error);
        alert('Unable to capture your screen. Please check console logs.\n' + error);
    });
}
function captureCamera(cb) {
    navigator.mediaDevices.getUserMedia({audio: true, video: true}).then(cb);
}
function keepStreamActive(stream) {
    var video = document.createElement('video');
    video.muted = true;
    video.srcObject = stream;
    video.style.display = 'none';
    (document.body || document.documentElement).appendChild(video);
}
captureScreen(function(screen) {
    keepStreamActive(screen);
    captureCamera(function(camera) {
        keepStreamActive(camera);
        screen.width = window.screen.width;
        screen.height = window.screen.height;
        screen.fullcanvas = true;
        
        camera.width = 320;
        camera.height = 240;
        camera.top = screen.height - camera.height;
        camera.left = screen.width - camera.width;
        
        var recorder = RecordRTC([screen, camera], {
            type: 'video',
            mimeType: 'video/webm',
            previewStream: function(s) {
                document.querySelector('video').muted = true;
                document.querySelector('video').srcObject = s;
            }
        });
        recorder.startRecording();
        window.stopCallback = function() {
            window.stopCallback = null;
            recorder.stopRecording(function() {
                var blob = recorder.getBlob();
                document.querySelector('video').srcObject = null;
                document.querySelector('video').src = URL.createObjectURL(blob);
                document.querySelector('video').muted = false;
                [screen, camera].forEach(function(stream) {
                    stream.getTracks().forEach(function(track) {
                        track.stop();
                    });
                });
                recorder.save("prueba");
            });
        };
        window.timeout = setTimeout(window.stopCallback, 10 * 1000);
    });
});
function addStreamStopListener(stream, callback) {
    stream.addEventListener('ended', function() {
        callback();
        callback = function() {};
    }, false);
    stream.addEventListener('inactive', function() {
        callback();
        callback = function() {};
    }, false);
    stream.getTracks().forEach(function(track) {
        track.addEventListener('ended', function() {
            callback();
            callback = function() {};
        }, false);
        track.addEventListener('inactive', function() {
            callback();
            callback = function() {};
        }, false);
    });
}
</script>

<footer style="margin-top: 20px; text-align: left;"><small id="send-message"></small></footer>
<script src="https://www.webrtc-experiment.com/common.js"></script>

@endsection