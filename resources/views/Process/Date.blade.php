@extends('layouts.app')
@section('content')

<script type="text/javascript">


  
</script>
<div class="container">
	
    <div class="row justify-content-center">
        <div class="col-md-12 animated fadeIn slower">
            <div class="row justify-content-center">
              <!--titulo -->
              
              <div class="col-lg-12 col-md-12 col-sm-12">    
               <h1> Asignar fecha entrevista</h1>
              </div>
              <!--fin titulo-->
              <div class="col-lg-12 col-md-12 col-sm-12 ">
                <div class="card  border-secondary ">
                  <div class="card-body">
                  	<div class="form">
                  		{{ Form::hidden('secret', '', array('id'=>'secret'))}}
                  		<div class="form-group">
                  			{{Form::Label('headquarter','Sede:')}}
                  			{{Form::select('headquarter', array(), '',array('class'=>'form-control','id'=>'headquarter'))}}
                  		</div>
                  		<div class="form-group">
                  			{{Form::Label('user','Postulante:')}}
                  			{{Form::select('user',$prospects,'',array('class'=>'form-control','id'=>'user',"disabled"))}}
                  			
                        
                  		</div>
                  		<div class="form-group">
                  			{{Form::Label('startDate','Fecha y hora de inicio:')}}
                  			<div class="input-group date"  >
                  				{{Form::text('date',null,array('class'=>'form-control datetimepicker-input','data-target'=>'#datetimepicker1','id'=>'datetimepicker1','data-toggle'=>'datetimepicker'))}}
                        </div>
                  		</div>
                  		<div class="form-group">
                  			{{Form::Label('endDate','Fecha y hora de fin:')}}
                  			<div class="input-group date"  >
                  				{{Form::text('date',null,array('class'=>'form-control datetimepicker-input','data-target'=>'#datetimepicker2','id'=>'datetimepicker2','data-toggle'=>'datetimepicker'))}}
                        </div>
                  		</div>
                  		<div class="form-group">
                  			{{Form::button('Actualizar',array('class'=> 'btn btn-primary','id'=>'save'))}}
                  		</div>
                  	</div>
                  	<h2 id="monthDate"></h2>
                    <div class="controles">
                    	<button id="back" class="btn btn-link">Anterior</button>
                      <button id="month" class="btn btn-light">Mes</button>
                      <button id="week" class="btn btn-light">Semana</button>
                      <button id="day" class="btn btn-light">Día</button>
                      <button id="next" class="btn btn-link">Siguiente</button>
                    </div>
                    <div id="calendar" style="height: 75vh;">
                    </div>
                  </div>
                </div>
              </div>
        </div>
        <!-- fin row -->
        </div>
      </div>
    </div>
    <script src="{{ asset('js/web/calendar.js') }}"></script>
  </div>
  
  @endsection
