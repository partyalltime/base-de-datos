<div class="row">
    <div class="col-md-12">
        <h4 id="CVcreate" class="text-dark"> Sube tu documento de identificación nacional</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form enctype="multipart/form-data" action="{{ route('uploadCV')}}" method="post">
            @csrf
            <div class="form-group">
                <input type="file" id="identification_document" class="inputfile" accept=".pdf" name="identification_document" required="required" />
                @if(isset($data->documents->identification))
                <label for="identification_document"><i class="fa fa-upload"></i>{{$data->documents->identification}}</label>
                @else
                <label for="identification_document"><i class="fa fa-upload"></i>Sube tu documento de identificación</label>
                @endif
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input type="hidden" name="extension" value="pdf" />
                <input type="hidden" name="type" value="identification_document" />
        				<h6 id="CVupload" class="text-gray">Solo se soportan archivos pdf</h6>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Subir</button>
            </div>
        </form>
    </div>
</div>