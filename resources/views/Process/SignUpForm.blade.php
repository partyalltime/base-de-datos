<div class="sign">

	<div class="titles">
		<div id="printSignUp">
			<div class="form-group row">
				<div class="col-md-6">
					{{Form::label('name','Nombre y apellidos'), array('class'=>'name')}}
					@if($prospect->name != null)
					{{Form::text('name', $prospect->name ,array('class'=>'form-control'))}}
					@else
					{{Form::text('name', "",array('class'=>'form-control'))}}
					@endif
				</div>
				<div class="col-md-6">
					{{Form::label('identification','DNI / NIE o Pasaporte')}}
					@if($prospect->identification_number != null)
					{{Form::text('identification_number', $prospect->identification_number ,array('class'=>'form-control'))}}
					@else
					{{Form::text('identification_number', "",array('class'=>'form-control'))}}
					@endif
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-6">
					{{Form::label('birthday','Fecha de nacimiento')}}
					<div class="input-group date selector1" id="datetimepicker{{$prospect->birthday}}" data-target-input="nearest">
					@if($prospect->birthday != null)
					{{Form::text('birthdayDate', $prospect->birthday ,array('class'=>'form-control'))}}
						<div  class="input-group-append" data-target="#datetimepicker{{$prospect->birthday}}" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
					@else
					{{Form::text('birthdayDate', "",array('class'=>'form-control'))}}
						<div  class="input-group-append" data-target="#datetimepicker{{$prospect->birthday}}" data-toggle="datetimepicker">
							<div class="input-group-text"><i class="fa fa-calendar"></i></div>
						</div>
					@endif
          </div>
				</div>
				<div class="col-md-6">
					{{Form::label('country',"Nacionalidad")}}
					@if($prospect->country != null)
					{{Form::text('country', $prospect->country ,array('class'=>'form-control'))}}
					@else
					{{Form::text('country', "",array('class'=>'form-control'))}}
					@endif
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-6">
					{{Form::label('city',"Ciudad")}}
					@if($prospect->city != null)
					{{Form::text('place', $prospect->city ,array('class'=>'form-control'))}}
					@else
					{{Form::text('place', "",array('class'=>'form-control'))}}
					@endif
				</div>
				<div class="col-md-6">
					{{Form::label('cp',"Código Postal")}}
					@if($prospect->postal_code != null)
					{{Form::text('cp', $prospect->postal_code ,array('class'=>'form-control numbers-validation', 'maxlength'=>'10'))}}
					@else
					{{Form::text('cp', "",array('class'=>'form-control numbers-validation', 'maxlength'=>'10'))}}
					@endif
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-12">
					{{Form::label('address',"Dirección")}}
					@if($prospect->address != null)
					{{Form::text('address', $prospect->address ,array('class'=>'form-control'))}}
					@else
					{{Form::text('address', "",array('class'=>'form-control'))}}
					@endif
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-6">
					{{Form::label('email',"Correo electrónico")}}
					@if($prospect->email != null)
					{{Form::text('email', $prospect->email ,array('class'=>'form-control email-validation'))}}
					@else
					{{Form::text('email', "",array('class'=>'form-control email-validation'))}}
					@endif
					{{Form::label('email','',array('class'=>'result'))}}
					
				</div>
				<div class="col-md-6">
					{{Form::label('phone',"Teléfono de contacto")}}
					@if($prospect->phone != null)
					{{Form::text('phone', $prospect->phone ,array('class'=>'form-control numbers-validation', 'maxlength'=>'64'))}}
					@else
					{{Form::text('phone', "",array('class'=>'form-control numbers-validation', 'maxlength'=>'64'))}}
					@endif
				</div>
			</div>
			<div class="form-group row">
				<div class="col-md-12">
					{{Form::label('pre-sign',"Grados Oficiales")}}
					<select class="selectpicker col-sm-12 col-form-label" name="studies[]" id="studies" multiple>
						<option value="II" selected="Ingeniería Informática">Ingeniería Informática</option>
					</select>
				</div>
			</div>

		</div>

		<div class="form-group">
			{{Form::submit('Guardar datos',['class'=> 'btn btn-sm btn-primary']) }}
		</div>

	</div>
</div>

<script>
	function printDiv(divName) {
		// var printContents = document.getElementById(divName).innerHTML;
		// var originalContents = document.body.innerHTML;
		// document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
	}
</script>

<script type="text/javascript">

         
          
$(function () {



	$('.selector1').each(function(){
   
		$("#"+$(this)[0].id).datetimepicker({"format": "YYYY-MM-DD"});
	  
	 
	});

  
});
</script>