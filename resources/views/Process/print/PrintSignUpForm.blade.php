<!DOCTYPE html>
<html>

<head><!--editarlo-->
    <meta charset="utf-8">
    <title></title>

    <style>
    .header{
    	width: 100%;
    	height: auto;
    }
    .logo{
    	width: 
    }
    .title{
        font-family:'PT Sans Caption';
        color: rgb(1, 138, 200);
        font-size: 20px;
    }
    .form-body{
        width:100%;
        /* height: 450px; */
        overflow: hidden;
        position: relative;
        
    }
    .general-text{
        font-family:'PT Sans Caption';
        color: rgb(1, 138, 200);
        font-size: 14px;
    }
    .inputs{
        width: 100%;
        border-bottom: 0.5px solid gray;
        border-top: 0px;
        border-right: 0px;
        border-left: 0px;
    }
  </style>
</head>
<body>

	<div class="header">
	    <div class="logo-container">
	        <img src="https://psicologiabalance.com/wp-content/uploads/2016/06/logo-horizontal-Universidad-Europea-del-Atl%C3%A1ntico-2ok.png" width="180px" height="56px">
	    </div>
	    <div class="form-title">
	        <p class="title">PREINSCRIPCIÓN Y RESERVA DE PLAZA </p>
	        <p>GRADOS OFICIALES</p>
	        
	    </div>
	</div>
		<!--MODIFICANDO, EL DE ARRIBA ES EL ANTIGUO-->
		<!--
		<div class="row">
			 	<div class="col-md-12">
			 		<p class="general-text"></p>
			 		<label class = "general-text"> Nombre y apellidos:</label>
	        		<input type="text" name="name" class="inputs" value="">
			 	</div>
			 	<div class="col-md-12">
			 		<div class="row ">
			 			<div class="col-md-6 ">
			 				<label class = "general-text">DNI / NIE o Pasaporte:</label>
	        		<input type="text" name="identification_number" class="inputs">
			 			</div>
			 			<div class="col-md-6">
			 				<label class = "general-text">Fecha de nacimiento</label>
							<label class = "">(dd/mm/aaaa):</label>
	        		<input type="text" name="birthdate" class="inputs">
			 			</div>
			 		</div>
			 	</div>
			 	<div class="col-md-12">
			 		<label class="general-text">Nacionalidad:</label>
	        <input type="text" name="nacionality" class="inputs">
			 	</div>
			 	<div class="col-md-12">
			 		<p class = "general-text">Dirección:</p>
	        <input type="text" name="address" class="inputs">
			 	</div>
			 	<div class="col-md-12">
			 		<div class="row">
			 			<div class="col-md-4">
			 				<p class = "general-text">Población / Ciudad:</p>
	        		<input type="text" name="city" class="inputs">
			 			</div>
			 			<div class="col-md-4">
			 				<p class = "general-text">País:</p>
	        		<input type="text" name="postal_code" class="inputs">
			 			</div>
			 			<div class="col-md-4">
			 				<p class = "general-text">Código postal:</p>
	        		<input type="text" name="postal_code" class="inputs">
			 			</div>
			 		</div>
			 	</div>
			 	<div class="col-md-12">
			 		<label class = "general-text">E-mail:</label>
	        <input type="text" name="email" class="inputs">
			 	</div>
			 	<div class="col-md-12">
			 		<label class = "general-text">Teléfono de contacto:</label>
	        <input type="text" name="phone" class="inputs">
			 	</div>
			 	<div class="col-md-12">
			 		<label class = "general-text">Grados oficiales:</label>
					 <br>
			<input type="checkbox" name="" value="Check Value" readonly="readonly" checked onclick="javascript: return false;"/>
			<label>Ingeniería Informática</label>
			 	</div>
		</div>-->

	    <div id="date-signature" class="bottom-container">
	        <div class="date-container">

	        </div>
	        <div class="signature-container">

	        </div>
	    </div>

	</div>
	<br>
	<button id="imprime">PDF</button>
	<!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="js/main.js"></script>-->
</body>

<!--EDITANDOOOOOOOOOOOOOOOOOOOOOOOO-->
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
<script type="text/javascript">
	function getImgFromUrl(logo_url, callback) {
    const img = new Image();
    img.src = logo_url;
    img.onload = function () {
        callback(img);
    };
	}
	async function getSchedules(){
	
		const called = await $.ajax({
			url: '/generate-pdf2',
			type: 'get',
			dataType: 'json',
			/*headers: {
	      'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
	  	},*/
			});

		return called;
	}

	function generatePDF(img,data){
		console.log(data);
    const options = {format: 'a4'};
    const doc = new jsPDF(options);
		doc.setTextColor(1, 138, 200);
		doc.text('PREINSCRIPCIÓN Y RESERVA DE PLAZAS',200, 25,{align: 'right'});
		doc.setFontSize(10);
		/* text('Text', X, Y) */
		/*line(x1,y1,x2,y2)*/
		/* Beggining of text fields */
		doc.text('GRADOS OFICIALES',200, 30,{align: 'right'});
		doc.text("Nombre y Apellidos:",15,50);
		doc.line(47, 51, 200, 51, 'S')
		doc.text("DNI / NIE o Pasaporte:",15,58);
		doc.line(51, 59, 103, 59, 'S')
		doc.text("Fecha de Nacimiento (dd/mm/aaaa):",105,58);
		doc.line(163, 59, 200, 59, 'S')
		doc.text("Nacionalidad:",15,64);
		doc.line(37, 65, 200, 65, 'S')
		doc.text("Dirección:",15,70);
		doc.line(31, 71, 200, 71, 'S')
		doc.text("Población / Ciudad:",15,76);
		doc.line(46, 77, 93, 77, 'S')
		doc.text("País:",95,76);
		doc.line(103, 77, 143, 77, 'S')
		doc.text("Código Postal:",145,76);
		doc.line(168, 77, 200, 77, 'S')
		doc.text("E-mail:",15,82);
		doc.line(26, 83, 200, 83, 'S')
		doc.text("Teléfono de contacto: ",15,88);
		doc.line(50, 89, 200, 89, 'S')
		doc.text("GRADOS OFICIALES ",15,105);
		doc.setTextColor(0, 0, 0);
		doc.text("ingeniería Informática ",21,111);
		
		//Para agregar checkBox
		var checkBox = new jsPDF.API.AcroFormCheckBox();
        checkBox.fieldName = "CheckBox2";
        checkBox.x = 15;
        checkBox.y = 107;
        checkBox.width = 5;
        checkBox.height = 5;
        checkBox.maxFontSize = 20;
		//checkBox.readOnly = true;
        doc.addField(checkBox);


		//doc.text(data.other_studies,25,115);		
		/* Ending of text fields */
		doc.setTextColor(0, 0, 0);
		/* Here starts information */
		doc.text(data.name,50,50);
		doc.text(data.address,40,70);
		doc.text(data.city,50,76);
		doc.text(data.postal_code,170,76);
		doc.text(data.identification_number,55,58);
		doc.text(data.birthday,165,58);
		doc.text(data.phone,55,88);
		doc.text("España",105,76);
		doc.text(data.email,30,82);
		
		

    doc.addImage(img, 'JPEG', 15, 15, 50, 15);
		doc.save('solicitud-beca-de-colaboración');
  } 
	
	$("#imprime").on('click',function(){
		getSchedules()
  	.then((data) => {
  		const logo_url = "{{ URL::asset('assets/img/logo.png')}}";
			getImgFromUrl(logo_url, function (img) {
			    generatePDF(img,data);
			});
  		console.log(JSON.stringify(data));
  		//console.log(schedules);
  	})
  	.catch((err)=>{
  		console.log(err)
  	});
  	
		/**/
	});
</script>


</html>
