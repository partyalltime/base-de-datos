<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    
    <style>
    .header{
    	width: 100%;
    	height: auto;
    }
    .logo{
    	width: 
    }
    .title{
        font-family:'PT Sans Caption';
        color: rgb(1, 138, 200);
        font-size: 20px;
    }
    .form-body{
        width:100%;
        /* height: 450px; */
        overflow: hidden;
        position: relative;
        
    }
    .general-text{
        font-family:'PT Sans Caption';
        color: rgb(1, 138, 200);
        font-size: 14px;
    }
    .inputs{
        width: 100%;
        border-bottom: 0.5px solid gray;
        border-top: 0px;
        border-right: 0px;
        border-left: 0px;
    }
  </style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
	     <img src="https://psicologiabalance.com/wp-content/uploads/2016/06/logo-horizontal-Universidad-Europea-del-Atl%C3%A1ntico-2ok.png" width="180px" height="56px">
				
			</div>
			<div class="col-md-6">
	        <p class="title">PREINSCRIPCIÓN Y RESERVA DE PLAZA </p>
	        <p class="title">Grados Oficiales</p>
				
			</div>
		</div>
		
	

	<div class="form-body">

	    <div id="date-signature" class="bottom-container">
	        <div class="date-container">

	        </div>
	        <div class="signature-container">

	        </div>
	    </div>

	</div>
	<button id="hey">IMPRIMIR FORMULARIO BECA</button>
</body>

<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
<script type="text/javascript">
	function getImgFromUrl(logo_url, callback) {
    const img = new Image();
    img.src = logo_url;
    img.onload = function () {
        callback(img);
    };
	}
	async function getSchedules(){
	
		const called = await $.ajax({
			url: '/generate-pdf2',
			type: 'get',
			dataType: 'json',
			/*headers: {
	      'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
	  	},*/
			});

		return called;
	}

	function generatePDF(img,data){
		console.log(data);
    const options = {format: 'a4'};
    const doc = new jsPDF(options);
		doc.setTextColor(1, 138, 200);
		doc.text('BECAS FUNIBER-RESIDENCIA UNIVERSITARIA',200, 25,{align: 'right'});
		doc.setFontSize(10);
		/* text('Text', X, Y) */
		/*line(x1,y1,x2,y2)*/
		/* Beggining of text fields */
		doc.text('Impreso de solicitud de Beca',15, 43);
		doc.text("Nombre:",15,50);
		doc.line(29, 51, 200, 51, 'S')
		doc.text("DNI/NIE o Pasaporte:",15,58);
		doc.line(50, 59, 103, 59, 'S')
		doc.text("Fecha de Nacimiento (aaaa/mm/dd):",105,58);
		doc.line(163, 59, 200, 59, 'S')
		doc.text("Nacionalidad:",15,64);
		doc.line(37, 65, 200, 65, 'S')
		doc.text("Dirección:",15,70);
		doc.line(31, 71, 200, 71, 'S');
		doc.text("Población/Ciudad:",15,76);
		doc.line(44, 77, 93, 77, 'S');
		doc.text("País:",95,76);
		doc.line(103, 77, 143, 77, 'S');
		doc.text("CP:",145,76);
		doc.line(151, 77, 200, 77, 'S');
		doc.text("E-mail:",15,82);
		doc.line(26, 83, 200, 83, 'S');
		doc.text("Teléfono de contacto: ",15,88);
		doc.line(50, 89, 200, 89, 'S')
		doc.text("Situación laboral: ",15,94);
		doc.line(42, 95, 200, 95, 'S')
		doc.text("Nivel de estudios actual (indicar):",15,102);
		doc.line(15, 108, 200, 108, 'S');
		doc.line(15, 116, 200, 116, 'S');
		doc.text("Centro donde han sido cursados:", 15, 123);
		doc.line(68, 123, 159, 123, 'S');
		doc.text("Nota media obtenida:", 161, 123);
		doc.line(193, 123, 200, 123, 'S');
		doc.text("Preinscripción en los estudios de:", 15, 134);
		doc.line(68, 134, 200, 134, 'S');
		doc.text("Fecha inicio estudios en la Universidad:", 15, 144);
		doc.line(78, 144, 200, 144);
		let checkbox = new jsPDF.API.AcroFormCheckBox();
		checkbox.Rect = [50, 10, 20, 20];
		
		/* Ending of text fields */
		doc.setTextColor(0, 0, 0);
		/* Here starts information */
		doc.text(data.name,30,50);
		doc.text(data.address,50,70);
		doc.text(data.city,50,76);
		doc.text(data.postal_code,152,76);
		doc.text(data.identification_number,50,58);
		doc.text(data.birthday,165,58);
		doc.text(data.phone,50,88);
		doc.text(data.studies,15,107);
		doc.text("España",105,76);
		doc.text("Ingenieria Informática",69,133);
		doc.text(data.average_grade.toString(),193,122);
		doc.text(data.studies_center.toString(),69,122);
		doc.text(data.other_studies,15,115);
		doc.text(data.email,30,82);
		doc.text("Estudiante",45,94);
		doc.text("Salvadoreño",4,604);
		/* Here ends information */
    doc.addImage(img, 'JPEG', 15, 15, 50, 15);
		doc.save('solicitud-beca-de-colaboración');
  } 
	
	$("#hey").on('click',function(){
		getSchedules()
  	.then((data) => {
  		const logo_url = "{{ URL::asset('assets/img/logo.png')}}";
			getImgFromUrl(logo_url, function (img) {
			    generatePDF(img,data);
			});
  		console.log(JSON.stringify(data));
  		//console.log(schedules);
  	})
  	.catch((err)=>{
  		console.log(err)
  	});
  	
		/**/
	});
</script>
</html>
