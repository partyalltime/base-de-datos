<div id="resindency">
	<div class="form-group row">
		<div class="col-md-12">
			<h3>Becas Funiber Residencia Universitaria</h3>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6">
			{{Form::label('identification_number','DNI / NIE o Pasaporte:',array('class'=>'col-form-label'))}}
			@if($prospect->identification_number != null)
			{{Form::text('identification_number', $prospect->identification_number ,array('class'=>'form-control'))}}
			@else
			{{Form::text('identification_number', "",array('class'=>'form-control'))}}
			@endif
		</div>
		<div class="col-md-6">
			{{Form::label('address','Dirección: ',array('class'=>'col-form-label'))}}
			@if($prospect->address != null)
			{{Form::text('address', $prospect->address ,array('class'=>'form-control'))}}
			@else
			{{Form::text('address', "",array('class'=>'form-control'))}}
			@endif
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6">
			{{Form::label('place','Poblacion / Ciudad: ',array('class'=>'col-form-label'))}}
			@if($prospect->city != null)
			{{Form::text('place', $prospect->city ,array('class'=>'form-control'))}}
			@else
			{{Form::text('place', "",array('class'=>'form-control'))}}
			@endif
		</div>
		<div class="col-md-6">
			{{Form::label('cp','CP: ',array('class'=>'col-form-label'))}}
			@if($prospect->postal_code != null)
			{{Form::text('cp', $prospect->postal_code ,array('class'=>'form-control numbers-validation'))}}
			@else
			{{Form::text('cp', "",array('class'=>'form-control numbers-validation'))}}
			@endif
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6">
			{{Form::label('phone','Teléfono de contacto: ',array('class'=>'col-form-label '))}}
			@if($prospect->phone != null)
			{{Form::text('phone', $prospect->phone ,array('class'=>'form-control numbers-validation'))}}
			@else
			{{Form::text('phone', "",array('class'=>'form-control numbers-validation'))}}
			@endif
		</div>
		<div class="col-md-6">
			{{Form::label('work','Situación Laboral: ',array('class'=>'col-form-label'))}}
			{{Form::select('work', array('D' => 'Desempleado', 'E' => 'Empleado', 'S' => 'Estudiante'), 'S', array('class'=>'form-control'))}}
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<h4>Estudios</h4>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6">
			{{Form::label('studies-level','Nivel de estudios actual (señalar): ',array('class'=>'col-form-label'))}}
			@if($prospect->studies != null)
			{{Form::text('superior', $prospect->studies ,array('class'=>'form-control'))}}
			@else
			{{Form::text('superior', "",array('class'=>'form-control'))}}
			@endif
		</div>
		<div class="col-sm-6">
			{{Form::label('places-studies','Centro donde han sido cursados: ',array('class'=>'col-form-label'))}}
			@if($prospect->studies_center != null)
			{{Form::text('places_studies', $prospect->studies_center ,array('class'=>'form-control'))}}
			@else
			{{Form::text('places_studies', "",array('class'=>'form-control'))}}
			@endif
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-6">
			{{Form::label('obtained-grade','Nota media obetenida (PAU/CFGS/Otros): ',array('class'=>'col-form-label'))}}
			@if($prospect->average_grade != null)
			{{Form::text('obtained_grade', $prospect->average_grade ,array('class'=>'form-control numbers-with-decimal'))}}
			@else
			{{Form::text('obtained_grade', "",array('class'=>'form-control numbers-with-decimal'))}}
			@endif
		</div>
		<div class="col-sm-6">
			{{Form::label('other-studies','Otras titulaciones (idiomas, cursos, publicaciones): ',array('class'=>'col-form-label'))}}
			@if($prospect->other_degrees != null)
			{{Form::text('other_studies', $prospect->other_degrees ,array('class'=>'form-control'))}}
			@else
			{{Form::text('other_studies', "",array('class'=>'form-control'))}}
			@endif

		</div>
	</div>


	<input name="residency_scholarship" value="true" hidden />

	<div class="form-group">
		{{Form::submit('Guardar datos',['class'=> 'btn btn-sm btn-primary']) }}
	</div>
</div>