<div class="row">
    <div class="col-md-12">
        <h3 id="CVcreate" class="text-dark">Pasaporte</h3>
    </div>
</div>
<div class="row">
    
</div>
<div class="row">
    <div class="col-md-12">
        <h3 id="CVupload" class="text-datk">Solo se soportan archivos pdf</h3>
        <form enctype="multipart/form-data" action="{{ route('uploadCV')}}" method="post">
            @csrf
            <div class="form-group">
                <input type="file" id="passport" class="inputfile" accept=".pdf" name="passport" required="required" />
                @if(isset($data->documents->passport))
                <label for="passport"><i class="fa fa-upload"></i>{{$data->documents->passport}}</label>
                @else
                <label for="passport"><i class="fa fa-upload"></i>Sube tu pasaporte</label>
                @endif
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input type="hidden" name="extension" value="pdf" />
                <input type="hidden" name="type" value="passport" />
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Subir</button>
            </div>
        </form>
    </div>
</div>