<div class="row">
    <div class="col-md-12">
        <h3 id="CVcreate" class="text-dark">Notas obtenidas en los últimos 4 años </h3>
        <h6>Si ya eres graduado de bachiller adjunta titulo de bachiller, certificación de notas de primero básico a bachiller, cierre de pensum (Hoja que da el colegio)</h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3 id="CVupload" class="text-datk">Solo se soportan archivos pdf</h3>
        <form enctype="multipart/form-data" action="{{ route('uploadCV')}}" method="post">
            @csrf
            <div class="form-group">
                <input type="file" id="grades" class="inputfile" accept=".pdf" name="grades" required="required" />
                @if(isset($data->documents->grades))
                <label for="grades"><i class="fa fa-upload"></i>{{$data->documents->grades}}</label>
                @else
                <label for="grades"><i class="fa fa-upload"></i>Sube tus notas</label>
                @endif
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input type="hidden" name="extension" value="pdf" />
                <input type="hidden" name="type" value="grades" />
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Subir</button>
            </div>
        </form>
    </div>
</div>