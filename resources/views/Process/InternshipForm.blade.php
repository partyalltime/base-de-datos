 <div class="intership">
	<div class="form-group row">
		<div class="col-md-12">
			<h4>Solicitud de beca</h4>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6">
			{{Form::label('identification_number','Identificación: ',array('class'=>'col-form-label'))}}
			<div class="row">
				<div class="col-md-6">
					{{Form::select('identification_type', array(
						'1' => 'Passport', 
						'2' => 'NIE', 
						'3' => 'DUI', 
						'4' => 'DNI', 
						'5' =>'CI', 
						'6' => 'RG',
						'7' => 'CC',
						'8' => 'DPI',
						'9' => 'TDI',
						'10' => 'CURP',
						'11' => 'CIP',
						'12' => 'CIC/CI',
						'13' => 'Cartão de Cidadão',
						'14' => 'CIE',
						), '1', array('class'=>'form-control'))}}
				</div>
				<div class="col-md-6">
					@if($prospect->identification_number != null)
					{{Form::text('identification_number', $prospect->identification_number ,array('class'=>'form-control'))}}
					@else
					{{Form::text('identification_number', "",array('class'=>'form-control'))}}
					@endif

				</div>
			</div>
		</div>

		<div class="col-md-6">
			{{Form::label('address','Dirección: ',array('class'=>'col-form-label'))}}
			@if($prospect->address != null)
			{{Form::text('address', $prospect->address ,array('class'=>'form-control'))}}
			@else
			{{Form::text('address', "",array('class'=>'form-control'))}}
			@endif
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6">
			{{Form::label('place','Poblacion / Ciudad: ',array('class'=>'col-form-label'))}}
			@if($prospect->city != null)
			{{Form::text('place', $prospect->city ,array('class'=>'form-control'))}}
			@else
			{{Form::text('place', "",array('class'=>'form-control'))}}
			@endif
		</div>
		<div class="col-md-6">
			{{Form::label('cp','Código Postal: ',array('class'=>'col-form-label'))}}
			@if($prospect->postal_code != null)
			{{Form::text('cp', $prospect->postal_code ,array('class'=>'form-control numbers-validation'))}}
			@else
			{{Form::text('cp', "",array('class'=>'form-control numbers-validation'))}}
			@endif
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6">
			{{Form::label('phone','Teléfono de contacto: ',array('class'=>'col-form-label'))}}
			@if($prospect->phone != null)
			{{Form::text('phone', $prospect->phone ,array('class'=>'form-control numbers-validation'))}}
			@else
			{{Form::text('phone', "",array('class'=>'form-control numbers-validation'))}}
			@endif
		</div>
		<div class="col-md-6">
			{{Form::label('work','Situación Laboral: ',array('class'=>' col-form-label'))}}
			{{Form::select('work', array('D' => 'Desempleado', 'E' => 'Empleado', 'S' => 'Estudiante'), 'S', array('class'=>'form-control'))}}
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<h4>Nivel de estudios actual (señalar): </h4>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-sm-6">
			{{Form::label('superior',' Ciclo Formativo Superior / Universitarios: ',array('class'=>'col-form-label'))}}
			@if($prospect->studies != null)
			{{Form::text('superior', $prospect->studies ,array('class'=>'form-control'))}}
			@else
			{{Form::text('superior', "",array('class'=>'form-control'))}}
			@endif
		</div>
		<div class="col-md-6">
			{{Form::label('other','Otros: ',array('class'=>'col-form-label'))}}
			@if($prospect->other_studies != null)
			{{Form::text('other', $prospect->other_studies ,array('class'=>'form-control'))}}
			@else
			{{Form::text('other', "",array('class'=>'form-control'))}}
			@endif

		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-6">
			{{Form::label('places-studies','Centro donde han sido cursados: ',array('class'=>'col-form-label'))}}
			@if($prospect->studies_center != null)
			{{Form::text('places_studies', $prospect->studies_center ,array('class'=>'form-control'))}}
			@else
			{{Form::text('places_studies', "",array('class'=>'form-control'))}}
			@endif

		</div>
		<div class="col-md-6">
			{{Form::label('obtained-grade','Nota Media Obtenida / Promedio Académico: ',array('class'=>'col-form-label'))}}
			@if($prospect->average_grade != null)
			{{Form::text('obtained_grade', $prospect->average_grade ,array('class'=>'form-control numbers-with-decimal'))}}
			@else
			{{Form::text('obtained_grade', "",array('class'=>'form-control numbers-with-decimal'))}}
			@endif
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			{{Form::label('other-studies','Otras titulaciones (idiomas, cursos, publicaciones): ',array('class'=>' col-form-label'))}}
			@if($prospect->other_degrees != null)
			{{Form::text('other_studies', $prospect->other_degrees ,array('class'=>'form-control'))}}
			@else
			{{Form::text('other_studies', "",array('class'=>'form-control'))}}
			@endif
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<h1>Preinscripción en los estudios de grado</h1>
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			{{Form::label('pre-login','Preinscripción en los estudios de Grado: ',array('class'=>'col-form-label'))}}
			{{Form::select('pre-login', array('1' => 'Ingeniería Informática', '2' => 'Empleado'), '1', array('class'=>'form-control'))}}
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<h4>Documentación a presentar</h4>
			{{Form::label('academic-record','Expediente académico personal, de los estudios que den acceso al Grado. Deberá figurar la nota media obtenida o, en su defecto las calificaciones de cada materia. ',array('class'=>'col-form-label'))}}
		</div>
	</div>

	<input name="collaboration_scholarship" value="true" hidden />

	<div class="form-group">
		{{Form::submit('Guardar datos',['class'=> 'btn btn-sm btn-primary']) }}
	</div>
</div>