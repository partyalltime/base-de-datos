@extends('layouts.app')
@section('content')


<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12 animated fadeIn slower">
      <div class="row justify-content-center">
        <!--titulo -->
        <div class="col-lg-10 col-md-10 col-sm-10">
          <h1 class="title line-head">Perfil</h1>

        </div>
        <!--fin titulo-->
        <div class="col-lg-6 col-md-10 col-sm-10 mt-3">
          <div class="tab-content">
            <div class="tab-pane active" id="basicdata">

              <div class="tile-body text-muted">
                <!--parte 1-->
                <div class="tile user-timeline">
                  <h3>Noticias</h3>
                  <strong>Se irá agregando noticias o información importante por aquí</strong>

                </div>

                <form method="post" action="{{ route('readAllNotif')}}">
                  <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                  <button class="btn btn-info"> Marcar todas como leídas </button>
                </form>
                @foreach($notifications as $key => $value)
                <div class="alert alert-dismissible alert-success">
                  @csrf
                  <form method="post" action="{{ route('readNotification')}}">
                    <input type="hidden" name="notId" value="{{ $value['id']}}" />
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <input class="close" type="submit" value="x" />
                  </form>
                  <h4>{{$value['type']}}</h4>
                  <p>{{ $value['date']}}</p>
                  <p>{{$value['message']}}</p>

                </div>
                @endforeach

                <div class="alert alert-dismissible alert-success">
                  <button class="close" type="button" data-dismiss="alert">×</button>
                  <h4>¡Release V.1!</h4>
                  <p>Si encuentra un error en la plataforma escribir un correo con captura de pantalla a <strong>elder.bolcaal@alumnos.uneatlantico.es</strong></p>
                </div>


                <div class="alert alert-dismissible alert-warning">
                  <button class="close" type="button" data-dismiss="alert">×</button>
                  <h4>¡Importante!</h4>
                  <p>Si es la primera vez que entra a este sitio, por favor rellenar el formulario que encontrará en la opción de entrevista. <a class="alert-link" href="{{url('interview')}}">Ok!, llevamé ahí!</a></p>
                </div>




                <!--fin parte 1-->
              </div>
            </div>
            <!--parte 2-->
            <div class="tab-pane fade" id="advancedata">

              <div class="tile user-timeline">
                <h4>Datos acerca de su perfil</h4>
                <a class="nav-link" href="{{url('myCoursesView')}}">Cursos a los que estoy inscrito</a>
              </div>

            </div>
            <!--fin parte 2-->
          </div>
        </div>

        <!--side bar -->
        <div class="col-lg-4 col-md-10 col-sm-10 mt-3">
          <div class="tile p-0">
            <ul class="nav flex-column nav-tabs user-tabs">
              <li class="nav-item"><a class="nav-link active" href="#basicdata" data-toggle="tab">Información General</a></li>
              <li class="nav-item"><a class="nav-link" href="#advancedata" data-toggle="tab">Acerca de mí</a></li>
            </ul>
          </div>
        </div>
        <!--fin side bar-->
      </div>
      <!-- fin row -->
    </div>
  </div>
</div>
</div>
@endsection