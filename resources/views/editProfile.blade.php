@extends('layouts.app')
@section('content')
<style type="text/css">
	.form-control {
		background-color: transparent;
	}

	.contacts {
		font-size: 1.2rem;
		color: gray;
	}

	.no-edit {
		-webkit-user-select: none !important;
		-moz-user-select: none !important!important;
		-ms-user-select: none !important;
		user-select: none !important; 
		pointer-events: none; /** Buscar una mejor opcion */
	}

	.data-group {
		display: table;
		width: auto;
		left: 0;
		right: 0;
		margin-left: auto;
		margin-right: auto;
		text-align: left;
		overflow-wrap: break-word;
  	    word-wrap: break-word;
		hyphens: auto;  

	}

	.data-group div {
		display: table-row;
		text-align: left;
	}

	.data-group#preline  {
  white-space: pre-line;  
  }

	.data-group div label {
		display: table-cell;
		text-align: left;
	}

	.field {
		font-weight: bold;
	}

	a:hover,
	a:focus {
		text-decoration: none;
		outline: none;
	}

	.tab-content {
		min-height: 97%;
		min-width: 300px;
		-webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
	}

	.nav-tabs li a {
		padding: 10px 20px;
		margin: 0 10px -1px 0;
		font-weight: 600;
		color: #293241;
		text-transform: uppercase;
		border-right: 2px solid #e6e5e1 !important;
		border-left: 2px solid #e6e5e1 !important;
		border-top: 2px solid #e6e5e1 !important;
		border-radius: 5px 5px 0 0;
		z-index: 1;
		position: relative;
		transition: all 0.3s ease 0s;
		top: -1px;
	}

	.nav-tabs .nav-item {
		margin-bottom: -1px;
	}

	.nav-tabs li a:hover,
	.nav-item a.nav-link.active {
		background: #fff;
		color: #0081c6;
		border-right: 2px solid #0081c6 !important;
		border-left: 2px solid #0081c6 !important;
		border-top: 2px solid #0081c6 !important;
	}

	#top-pic {
		height: 160px;
		width: auto;
		background: url(../images/bg-02.png) no-repeat right bottom;
		background-size: contain;
		background-color: #0081c6 !important;
		margin: 0;

	}

	#avatar {
		width: 170px;
		height: 170px;
		top: -148px;
		left: 0;
		right: 0;
		margin-left: auto;
		margin-right: auto;
		border-radius: 100%;
		box-shadow: 0 0 0 7px #f8f9fa;
		position: absolute;
		z-index: 1;
		background-color: #ddd;
	}

	.info {
		height: 50%;
		width: 100%;
		display: table;
		left: 0;
		bottom: 0;

	}

	.titles hr {
		width: 157px;
		margin-top: 0;
		margin-bottom: 7px;
		border: 1px solid #e6e5e1;
	}

	#student-name {
		font-size: 1.7rem;
		margin-top: 8px;
	}

	#student-user {
		font-size: 13px;
	}

	#country {
		font-size: 1.2rem;
		margin-bottom: 5px;
		color: gray;
	}

	.punctuation-score {
		font-size: 1.9rem;
		color: #0081c6;
	}
</style>
<div class="container mt-3">
	<div class="row justify-content-center">
		<div class="col-md-9">
			<!--Tabs-->
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Acerca de mí</a>
				</li>
				@if(isset($info["user_data"]))
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Mis Aptitudes</a>
				</li>
				@endif
			</ul>
			<!--titulo -->
			<!--fin titulo-->
			<form method="post" action="{{ route('sendUpdate')}}" accept-charset="UTF-8">
				@csrf
				<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
				<!--Tabs content-->
				<div class="tab-content" id="myTabContent">
					<!--Tab 1--->
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="row">
							<div class="col-12">
								<div id="top-pic">
								</div>
							</div>
						</div>


						<div class="row p-3">
							<!--Parte  1-->
							<div class="col-lg-5 col-sm-12 text-center" id="basicdata">
								<div id="avatar" class="mt-3">
									<img src="https://image.flaticon.com/icons/svg/1974/1974887.svg" class="rounded-circle" width="170px" height="170px">
								</div>
								<div id="info-box" class="mt-5">
									<div class="titles">
										<div id="student-name">{{Form::label('name',$info['user'][0]['name'],array('class'=>'name'))}}</div>
										<div id="student-user" class="text-muted">{{Form::label('mail',$info['user'][0]['email'],array('class'=>'email'))}}</div>
										<hr>
									</div>
									<div id="country"><i class="fa fa-map-marker" aria-hidden="true" style="color: #0081c6;"></i>
									@if($info['user'][0]['residency'] != null)
										{{Form::label('data',$info['user'][0]['residency'])}}
										@else
										{{Form::label('data',"Información no disponible")}}
										@endif</div>
									<div id="punctuations">
										<div class="row">
											{{-- <div class="col-6 text-right">
												<div class="punctuation-score">1</div>
												<div class="punctuation-name">Curso/s</div>

											</div>	

											<div class="col-6 text-left">
												<div class="punctuation-score">5</div>
												<div class="punctuation-name">Nivel</div>                            
											</div>	--}}
										</div> 
									</div>
								</div>
							</div>
							<!--Part 1 closes--->

							<!--Part 2-->
							<div class="col-lg-7 col-sm-12 mt-5 text-center px-4">

								<div class="form text-left">
									<div>
										{{Form::label('contact','Información básica',array('class'=>'contacts'))}}
									</div>
									<div class="form-group">
										{{Form::label('birthday','Fecha de nacimiento: ',array('class'=>'field'))}}
										<div class="input-group date"id="datetimepicker3" data-target-input="nearest">
										{{Form::text('birthdayDate',$info['user'][0]["birthday"],array('class'=>'form-control datetimepicker-input no-edit', 'id'=>"fecha-nacimiento"))}}
										<div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
            								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
      								    </div>
										</div>
									</div>
									<div class="form-group">
										{{Form::label('gender','Género: ',array('class'=>'field'))}}
										{{Form::label('data',($info['user'][0]['gender'] == 0)? 'Hombre': 'Mujer')}}
									</div>
								  <div class="form-group">
										{{Form::label('academic_level','Nivel Académico: ',array('class'=>'field'))}}
										<select name="acdmc_lvl" class="form-control">
											@if(!empty($info['country_settings']['academic_level_restrictions']))
											@foreach($info['country_settings']['academic_level_restrictions'] as $restriction)
												<option value="{{$restriction}}">{{$restriction}}</option>
											@endforeach
											@endif
										</select>
									</div>
									<div class="form-group">
										{{Form::label('academic_cycle','Ciclo Académico: ',array('class'=>'field'))}}
										@if(!empty($info['country_settings']['academic_cycle_restrictions']))
										<select name="acdmc_cycle" class="form-control">
											@foreach($info['country_settings']['academic_cycle_restrictions'] as $restriction)
												<option value="{{$restriction}}">{{$restriction}}</option>
											@endforeach
											@endif
										</select>
									</div>
									<br>
									<div class="form-group">
										{{Form::label('contact','Información de contacto',array('class'=>'contacts'))}}
									</div>
									<div class="form-group">
										{{Form::label('phone','Teléfono:',array('class'=>'field'))}}
										{{Form::text('phoneNumber', $info['user'][0]["phone"],array('class'=>'form-control'))}}
									</div>
									<div class="form-group">
										{{Form::label('country','Residencia:',array('class'=>'field'))}}
										{{Form::select('pais', $info['countries'], $info['user'][0]['code'],array("class"=>'form-control'))}}
									</div>
									<br>
								</div>
								<button class="btn btn-primary mb-2">Guardar</button>
							</div>
							<!--Part 2 closes--->
						</div>
						<!--row tab 1 closes--->

					</div>
					<!--Tab 1 closes--->

					@if(isset($info["user_data"]))
					<!--Tab 2--->
					<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="row">
							<div class="col-12">
								<div id="top-pic">
								</div>
							</div>
						</div>


						<div class="row p-3">
							<!--Parte  1-->
							<div class="col-lg-5 col-sm-12 text-center" id="basicdata">
								<div id="avatar" class="mt-3">
									<img src="https://image.flaticon.com/icons/svg/1974/1974887.svg" class="rounded-circle" width="170px" height="170px">
								</div>
								<div id="info-box" class="mt-5">
									<div class="titles">
										<div id="student-name">{{Form::label('name',$info['user'][0]['name'],array('class'=>'name'))}}</div>
										<div id="student-user" class="text-muted">{{Form::label('mail',$info['user'][0]['email'],array('class'=>'email'))}}</div>
										<hr>
									</div>
									<div id="country"><i class="fa fa-map-marker" aria-hidden="true" style="color: #0081c6;"></i> @if($info['user'][0]['residency'] != null)
										{{Form::label('data',$info['user'][0]['residency'])}}
										@else
										{{Form::label('data',"Información no disponible")}}
										@endif</div>
									<div id="punctuations">
										{{-- <div class="row">
											<div class="col-6 text-right">
												<div class="punctuation-score">1</div>
												<div class="punctuation-name">Curso/s</div>
											</div>
											<div class="col-6 text-left">
												<div class="punctuation-score">5</div>
												<div class="punctuation-name">Nivel</div>
											</div>
										</div> --}}
									</div>
								</div>
							</div>
							<!--Part 1 closes--->

							<!--Part 2-->
							<div class="col-lg-7 col-sm-12 mt-5 text-center px-4">
								<div class="data-group">
									@foreach($info["user_data"] as $key => $value)
									<div class="col-lg-7 col-sm-12 mt-5 text-left px-4">
										<div class="row">
											{{Form::label('key',$key.':',array('class'=>'form'))}}
											@if($key=="Pre-inscrito" || $key=="Pre-registrado" || $key=="Nivel" || $key=="Otorgado" || $key=="Notificado")
												{{Form::label('data',$value)}}
											@else
												{!! Form::textarea($key,$value,['class'=>'form-control', 'rows' => 2, 'cols' => 40]) !!}
											@endif	
										</div>
										
									</div>
									<br>
									<br>
									@endforeach
								</div>
								<button class="btn btn-primary mb-2">Guardar</button>
							</div>
							<!--Part 2 closes--->
						</div>
						<!--row tab 2 closes--->
					</div>
					<!--Tab 2 closes--->
					@endif
					<!--<button class="btn btn-primary mb-2">Guardar</button>--->
				</div>
				<!--Tab content closes-->
			</form>
			<!-- fin row -->
		</div>
		<!--col-md-9 closes-->
		<div id="notifications"></div>
	</div>
	<!--row justify-content-center closes-->
</div>
<!--container mt-3 closes-->

<script type="text/javascript">
  $(document).ready(function() {

	
	

    let position = {{ $info["id"] }};
    if(position == 1){
      $("#profile-tab").tab('show');
    }
    else{
      $("#home-tab").tab('show');
    }
    $('#datetimepicker3').datetimepicker({
      "format": "YYYY-MM-DD",
	  "required": false,
	  "viewMode": 'years',
	  "ignoreReadOnly": false,
      "defaultDate": "{{$info['user'][0]["birthday"]}}"
	});
	


  });
</script>

@endsection
