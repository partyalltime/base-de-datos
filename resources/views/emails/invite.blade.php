<html>
  <head/>
  <body style="margin: 0;padding: 0;mso-line-height-rule: exactly;min-width: 100%;background-color: #fff">
    <center class="wrapper" style="display: table;table-layout: fixed;width: 100%;min-width: 620px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #fff">
    <table class="top-panel center" width="602" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;border-spacing: 0;margin: 0 auto;width: 602px">
        <tbody>
        <tr>
            <td class="title" width="300" style="padding: 8px 0;vertical-align: top;text-align: left;width: 300px;color: #616161;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 12px;line-height: 14px">
            	Ulabs
          	</td>
            <td class="subject" width="300" style="padding: 8px 0;vertical-align: top;text-align: right;width: 300px;color: #616161;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 12px;line-height: 14px">
            	<a class="strong" href="#" target="_blank" style="font-weight: 700;text-decoration: none;color: #616161">
            		https://www.uneatlantico.es/
          		</a>
          	</td>
        </tr>
        <tr>
            <td class="border" colspan="2" style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #e0e0e0;width: 1px"> </td>
        </tr>
        </tbody>
    </table>

    <div class="spacer" style="font-size: 1px;line-height: 16px;width: 100%"> </div>

    <table class="main center" width="602" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;border-spacing: 0;margin: 0 auto;width: 602px;-webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);-moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24)">
        <tbody>
        <tr>
            <td class="column" style="padding: 0;vertical-align: top;text-align: left;background-color: #fff;font-size: 14px">
                <div class="column-top" style="font-size: 24px;line-height: 24px"> </div>
                <table class="content" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;border-spacing: 0;width: 100%">
                    <tbody>
                    <tr>
                        <td class="padded" style="padding: 0 24px;vertical-align: top">
                          <h1 style="margin-top: 0;margin-bottom: 16px;color: #212121;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 20px;line-height: 28px">
                          	Mucho gusto. Gracias por tomarte el tiempo de aplicar.
                          </h1>
                          
                          <p style="margin-top: 0;margin-bottom: 16px;color: #212121;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 16px;line-height: 24px">
                          	Has iniciado el proceso para adquirir una beca universitaria en la Universidad Europea del Atlántico. Formarás parte de nuestro equipo de desarrollo en donde se otorga al alumnado becario la posibilidad de colaborar en proyectos reales.
                          </p>
                          <p style="margin-top: 0;margin-bottom: 16px;color: #212121;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 16px;line-height: 24px">
                          	Se te ha enviado este correo de invitación para crear tu cuenta y así iniciar el proceso para la beca universitaria que anteriormente se te ha descrito.
                          </p>
                          
                          <p style="text-align: center;margin-top: 0;margin-bottom: 16px;color: #212121;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 16px;line-height: 24px">
                          	<a href="{{ route('accept', $invite->token) }}" class="btn" style="text-decoration: none;color: #fff;background-color: #2196F3;border: 1px solid #2196F3;border-radius: 2px;display: inline-block;font-family: Roboto, Helvetica, sans-serif;font-size: 14px;font-weight: 400;line-height: 36px;text-align: center;text-transform: uppercase;width: 200px;height: 36px;padding: 0 8px;margin: 0;outline: 0;outline-offset: 0;-webkit-text-size-adjust: none;mso-hide: all">
                          		Haz click aquí
                          	</a>
                          </p>
                          <p style="text-align: center;margin-top: 0;margin-bottom: 16px;color: #212121;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 16px;line-height: 24px">
                            Si en caso no puedes acceder copia y pega este enlace: <a href="{{ route('accept', $invite->token) }}" class="strong" style="font-weight: 700;text-decoration: none;color: #616161">{{ route('accept', $invite->token) }}</a>
                          </p>
                          <p class="caption" style="margin-top: 0;margin-bottom: 16px;color: #616161;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 12px;line-height: 20px">
                          	Si no has solicitado la beca o este correo no es correspondiente solo ignoralo.
                          </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="column-bottom" style="font-size: 8px;line-height: 8px"> </div>
            </td>
        </tr>
        </tbody>
    </table>

    <div class="spacer" style="font-size: 1px;line-height: 16px;width: 100%"> </div>

    <table class="footer center" width="602" border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse;border-spacing: 0;margin: 0 auto;width: 602px">
        <tbody>
        <tr>
            <td class="border" colspan="2" style="padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #e0e0e0;width: 1px"> </td>
        </tr>
        <tr>
            <td class="signature" width="300" style="padding: 0;vertical-align: bottom;width: 300px;padding-top: 8px;margin-bottom: 16px;text-align: left">
                <p style="margin-top: 0;margin-bottom: 8px;color: #616161;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 12px;line-height: 18px">
                    Universidad Europea del Atlantico<br/>
                    +34 942 244 244<br/>
                    C/Isabel Torres 21<br/>
										39011 Santander, España
                </p>
                <p style="margin-top: 0;margin-bottom: 8px;color: #616161;font-family: Roboto, Helvetica, sans-serif;font-weight: 400;font-size: 12px;line-height: 18px">
                    Información: <a class="strong" href="mailto:#" target="_blank" style="font-weight: 700;text-decoration: none;color: #616161">info@uneatlantico.es</a>
                </p>
            </td>
            <td class="subscription" width="300" style="padding: 0;vertical-align: bottom;width: 300px;padding-top: 8px;margin-bottom: 16px;text-align: right">
                <div class="logo-image" style="">
                    <a href="https://www.uneatlantico.es/" target="_blank" style="text-decoration: none;color: #616161"><img src="https://pbs.twimg.com/profile_images/950299714645262336/3EfAXr92.jpg" alt="Universidad Europea del Atlantico" width="70" height="70" style="border: 0;-ms-interpolation-mode: bicubic"/></a>
                </div>
                
            </td>
        </tr>
        </tbody>
    </table>
</center>
</html>

	 	
