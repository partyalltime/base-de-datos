<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="form-group">
                            <label for="filtroCategorias">Filtrar por Categorías: </label>
                            <select class="form-control" id="filtroCategorias">
                            <option selected value="0">Todas las categorías</option>
                                @foreach ($categories as $item)
                                    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="col-md-12">
                <div class="row mt-4" id="postsContainer">
                    @foreach ($posts as $item)
                        <div class="col-lg-4 col-sm-12 col-md-6 d-flex">
                            <div class="tile p-0 cursos flex-fill">
                                <div class="tile-title-w-btn justify-content-end bg-primary">
                                    <div class="btn-group">
                                        <a class="btn btn-primary" href="{{ './itemTools/' . $item->id }}">
                                        <i class="fa fa-lg fa-plus"></i></a>
                                        <a class="btn btn-primary" href="{{ './editItems/' . $item->id }}">
                                            <i class="fa fa-lg fa-edit"></i></a>
                                        <a class="btn btn-primary" href="{{ './deletePost/' . $item->id }}"><i class="fa fa-lg fa-trash"></i></a>
                                    </div>
                                </div>
                                <div class="card-header bg-default flex-fill">
                                    @foreach ($categories as $itemC)
                                        @if ($itemC->id === $item->category_id)
                                            <div class="card-category mb-2">{{ $itemC->name }}</div>
                                        @endif
                                    @endforeach
                                     <h3 class="title">{{ $item->name }}</h3>
                                </div>
                                <div class="tile-body p-4" style="overflow: hidden; text-overflow: ellipsis; max-height: 150px">
                                {{ $item->body }}
                                </div>
                                    <div class="tile-footer p-4">
                                        <div>
                                            <div class=" d-flex justify-content-between">
                                                <a class="btn btn-secondary" href="{{ './itemTools/' . $item->id }}">Comenzar</a>
                                                <p class="text-muted text-right mt-2">Duración: {{$item->total_time}} minutos</p>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
        <div class="col-md-12" id="postsContainer">
        </div>
    </div>