<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" sizes="32x32">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">


    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/web/tutorial.css') }}" rel="stylesheet" >


    <script type="text/javascript" src="{{ asset('js/prism.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/prism.css') }}">

    <!-- Scripts -->
    <!-- ********************* JS JQUERY LINKS ********************* -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/prism.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/prism.css') }}">
    
<!-- The javascript plugin to display page loading on top-->
    <script src="{{ asset('js/plugins/pace.min.js') }}"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="{{ asset('js/plugins/chart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
</head>
<body class="app sidebar-mini rtl">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel app-header">
                @auth
                <!-- Sidebar toggle button-->
                <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
                @endauth

                <a class="navbar-brand" href="{{ url('/home') }}">
                   <img class="pr-2" height="25px" src="{{ asset('images/logo.png') }}"/>
                </a>


                 <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    Tiempo restante: {{$post->total_time}} mins 
                                </a>
                            </li>
                    </ul>
        </nav>
        @auth
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
        <ul class="app-menu" >
            <li>
                <a class="app-menu__item {{ (request()->is('home'))  ? 'active' : '' }}" href="{{url('home')}}">
                    <i class="app-menu__icon fa fa-arrow-left" aria-hidden="true"></i>
                    <span class="app-menu__label">Volver home</span>
                </a>
            </li>
        </ul>
     
        <ol class="app-menu" id="items">
        @foreach ($items as $elem)
            @if($loop->first)
                <li number="{{$loop->index+1}}" id="slide-ref-{{$elem->id}}" onClick="setLiToHash({{$elem->id}})" completed selected>
            @else
                <li number="{{$loop->index+1}}" id="slide-ref-{{$elem->id}}" onClick="setLiToHash({{$elem->id}})">
            @endif
                <a class="app-menu__item {{ (request()->is('tutorialView/$post->id#slide-item-$elem->id'))  ? 'selected' : '' }}" href="#slide-item-{{$elem->id}}">
                    <span class="step app-menu__icon">
                        <span class="app-menu__label">{{$elem->title}}</span>
                    </span>
                </a>
            </li>
        @endforeach
        </ol>
    </aside>

        @endauth
        <main class="py-4 {{ Auth::check() ? 'app-content' : '' }}">
            @yield('content')
        </main>
    </div>
    <!-- <script src="{{ asset('js/main.js') }}"></script> -->
    <script src="{{ asset('js/web/tutorial.js') }}"></script>
</body>
</html>
