<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" sizes="32x32">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">


    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/web/tutorial.css') }}" rel="stylesheet" >


    <script type="text/javascript" src="{{ asset('js/prism.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/prism.css') }}">

    <!-- Scripts -->
    <!-- ********************* JS JQUERY LINKS ********************* -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/prism.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/prism.css') }}">
    
<!-- The javascript plugin to display page loading on top-->
    <script src="{{ asset('js/plugins/pace.min.js') }}"></script>
    <!-- Page specific javascripts-->
</head>
<body class="app sidebar-mini rtl">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel app-header">

                <a class="navbar-brand"">
                   <img class="pr-2" height="25px" src="{{ asset('images/logo.png') }}"/>
                </a>
        </nav>
        <main class="py-4">
            @yield('content')
        </main>
    <!-- <script src="{{ asset('js/main.js') }}"></script> -->
    <script src="{{ asset('js/web/tutorial.js') }}"></script>
</body>
</html>
