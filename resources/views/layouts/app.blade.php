 
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" sizes="32x32">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />

    <!-- Scripts -->
    <!-- ********************* JS JQUERY LINKS ********************* -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{ asset('js/plugins/pace.min.js') }}"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="{{ asset('js/plugins/chart.js') }}"></script>
    <script  type="text/javascript" src="{{ asset('editor/dist/trumbowyg.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/dist/langs/es.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/dist/plugins/colors/trumbowyg.colors.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/dist/plugins/noembed/trumbowyg.noembed.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/dist/plugins/preformatted/trumbowyg.preformatted.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/dist/plugins/lineheight/trumbowyg.lineheight.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/dist/plugins/fontsize/trumbowyg.fontsize.js') }}"></script>

    <script type="text/javascript" src="{{ asset('editor/dist/plugins/upload/trumbowyg.upload.js') }}"></script>


    <script type="text/javascript" src="{{asset('js/moment.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/moment-with-locales.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/prism.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tempusdominus-bootstrap-4.js') }}"></script>
    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
    
    <link rel="stylesheet" href="{{ asset('css/prism.css') }}">


    <link rel="stylesheet" href="{{ asset('editor/dist/ui/trumbowyg.min.css') }}">
    <link rel="stylesheet" href="{{ asset('editor/dist/plugins/colors/ui/trumbowyg.colors.css') }}">
    
    
    <!-- Calendar -->
    <script src="https://uicdn.toast.com/tui.code-snippet/latest/tui-code-snippet.js"></script>
    <script src="https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.js"></script>
    <script src="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.js"></script>
    <script src="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.css" />
    <link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.css" />
    <link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.css" />
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
    <!-- Font PT Sans  google-->
     <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">
     <style type="text/css">
     	#confirmBox
			{
			  display: none;
			  background-color: #ccc;
			  border-radius: 5px;
			  border: 1px solid #aaa;
			  position: fixed;
			  width: 300px;
			  left: 50%;
			  margin-left: -150px;
			  padding: 6px 8px 8px;
			  box-sizing: border-box;
			  text-align: center;
			  z-index: 3;
			}
			#confirmBox .button {
			  background-color: #ccc;
			  display: inline-block;
			  border-radius: 3px;
			  border: 1px solid #aaa;
			  padding: 2px;
			  text-align: center;
			  width: 80px;
			  cursor: pointer;
			}
			#confirmBox .button:hover
			{
			  background-color: #ddd;
			}
			#confirmBox .message
			{
			  text-align: left;
			  margin-bottom: 8px;
			}
			hr { 
				display: block; 
				height: 1px;
    		border: 0; 
    		border-top: 1px solid #ccc;
    		margin: 1em 0; 
    		padding: 0; 
    	}
    .separator {
    	display: flex;
    	align-items: center;
    	text-align: center;
    	color: white;
		}
		.separator::before, .separator::after {
		    content: '';
		    flex: 1;
		    border-bottom: 1px solid #fff;
		}
		.separator::before {
		    margin-right: .25em;
		}
		.separator::after {
		    margin-left: .25em;
		}
     </style>
</head>
<body class="app sidebar-mini rtl">
  <div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel app-header">
      @auth
      <!-- Sidebar toggle button-->
      <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <a class="navbar-brand" href="{{ url('/home') }}">
         <img class="pr-2" height="25px" src="{{ asset('images/logo.png') }}"/>
      </a>
      @endauth
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">
        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto" style="margin-top: -2%; margin-bottom: -2%; ">
          <a href="https://uneatlantico.es/">
              <img class="pr-2" style="height: 5vw; width: 7vw; margin-top: -2%; margin-bottom: -2%;" src="{{ asset('images/logo-UNEA.svg') }}"/>
          </a>
          <!-- Authentication Links -->
          @guest
             
          @endguest
        </ul>
      </div>
    </nav>
    @auth

      <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://www.flaticon.es/premium-icon/icons/svg/1146/1146324.svg" alt="User Image">
      <div>
        <p class="app-sidebar__user-name">{{ Auth::user()->name }}</p>
          <p class="app-sidebar__user-designation">
          @if(Auth::user()->role_id==1)
              Admin
          @else
              User
          @endif
          </p>
      </div>
    </div>
    <ul class="app-menu">
      @if(Auth::user()->isProspect() || Auth::user()->isNewIntern() || Auth::user()->isIntern())
      <li>
        <a class="app-menu__item {{ (request()->is('profile'))  ? 'active' : '' }}" href="{{url('individualInfo/'.Auth::user()->id)}}">
          <i class="app-menu__icon fa fa-user" aria-hidden="true"></i>
          
          <span class="app-menu__label">Perfil</span>
          @if(!(Auth::user()->has_filled_att))
          <span class="app-menu__label"  style="font-size: 10px; color: red">❗️Completa tus atributos</span>
          @endif
        </a>
      </li>
      @endif
      {{-- <li>
        <a class="app-menu__item {{ (request()->is('interview'))  ? 'active' : '' }}" href="{{url('interview')}}">
          <i class="app-menu__icon fa fa-address-card" aria-hidden="true"></i>
          <span class="app-menu__label">Queremos conocerte</span>
        </a>
      </li> --}}
      <li>
      	<div class="separator">Cursos</div>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('home'))  ? 'active' : '' }}" href="{{url('home')}}">
          <i class="app-menu__icon fa fa-book" aria-hidden="true"></i>
          <span class="app-menu__label">Cursos</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('myCoursesView'))  ? 'active' : '' }}" href="{{url('myCoursesView')}}">
          <i class="app-menu__icon fa fa-bookmark" aria-hidden="true"></i>
          <span class="app-menu__label">Mis cursos</span>
        </a>
      </li>
      @if(Auth::user()->isAdmin() || Auth::user()->isEditor())
      
      <li>
        <a class="app-menu__item {{ (request()->is('postTools'))  ? 'active' : '' }}" href="{{url('postTools')}}">
          <i class="app-menu__icon fa fa-puzzle-piece"></i>
          <span class="app-menu__label">Editar Cursos</span>
        </a>
      </li>
      @endif
      {{ $intern_data = \App\NewIntern::where('prospect_id', Auth::user()->id)->first() }}
      @if(Auth::user()->hasBeenRequested() || $intern_data !== null)
      <li>
      	<div class="separator">Proceso de beca</div>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('process'))  ? 'active' : '' }}" href="{{url('process')}}">
          <i class="app-menu__icon fa fa-th-list"></i>
          <span class="app-menu__label">Proceso de beca</span>
        </a>
      </li>
      @endif
      @if(Auth::user()->isAdmin() || Auth::user()->isManager())
      <li>
      	<div class="separator">Listados de usuarios</div>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('lista'))  ? 'active' : '' }}" href="{{url('lista')}}">
          <i class="app-menu__icon fa fa-th-list"></i>
          <span class="app-menu__label">Lista por Sede</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('listByResponsible'))  ? 'active' : '' }}" href="{{url('ListByResponsible')}}">
          <i class="app-menu__icon fa fa-th-list"></i>
          <span class="app-menu__label">Lista por Responsable</span>
        </a>
      </li>
      
      
      <li>
        <a class="app-menu__item {{ (request()->is('newIntern/view'))  ? 'active' : '' }}" href="{{url('/newIntern/view')}}">
          <i class="app-menu__icon fa fa-th-list"></i>
          <span class="app-menu__label">Lista de nuevos becarios</span>
        </a>
      </li>
      <li>
      	<div class="separator">Administrador de proceso</div>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('pendingApproval/view'))  ? 'active' : '' }}" href="{{url('/pendingApproval/view')}}">
          <i class="app-menu__icon fa fa-th-list"></i>
          <span class="app-menu__label">Pendientes aprobación</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('process/date'))  ? 'active' : '' }}" href="{{url('/process/date')}}">
          <i class="app-menu__icon fa fa-calendar"></i>
          <span class="app-menu__label">Fechas Entrevistas</span>
        </a>
      </li>         
      <li>
        <a class="app-menu__item {{ (request()->is('/interviews/view'))  ? 'active' : '' }}" href="{{url('/interviews/view')}}">
          <i class="app-menu__icon fa fa-camera"></i>
          <span class="app-menu__label">Pendientes entrevista</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('prospect/view'))  ? 'active' : '' }}" href="{{url('/prospect/view')}}">
          <i class="app-menu__icon fa fa-th-list"></i>
          <span class="app-menu__label">Lista de prospectos</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('invite'))  ? 'active' : '' }}" href="{{url('invite')}}">
          <i class="app-menu__icon fa fa-paper-plane"></i>
          <span class="app-menu__label">Invitar</span>
        </a>
      </li>
      <li>
      	<div class="separator">Configuraciones</div>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('/headquarter/admin'))  ? 'active' : '' }}" href="{{url('/headquarter/admin')}}">
          <i class="app-menu__icon fa fa-cogs"></i>
          <span class="app-menu__label">Configuración de sede</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item {{ (request()->is('/users/admin'))  ? 'active' : '' }}" href="{{url('/users/admin')}}">
          <i class="app-menu__icon fa fa-cogs"></i>
          <span class="app-menu__label">Administración de usuarios</span>
        </a>
      </li>
      @endif
      <li>
      	<li>
      		<div class="separator">Sesión</div>
      	</li>
        <a class="app-menu__item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
    			<i class="app-menu__icon fa fa-sign-out"></i>
          <span class="app-menu__label"> {{ __('Cerrar sesión') }}</span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
      </li>        
    </ul>
  </aside>
      @endauth
      <main class="py-4 {{ Auth::check() ? 'app-content' : '' }}">
          @yield('content')
          <div id="snackbar"></div>
          <div id="confirmBox">
					    <div class="message"></div>
					    <span class="button yes">Yes</span>
					    <span class="button no">No</span>
					</div>
      </main>
  </div>
		

    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/validations.js') }}"></script>
    

</body>
</html>
