@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">  
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
             <div class="tile-title-w-btn mb-4">
              <h3 id="titleTable" class="title">Listado de alumnos a mi cargo</h3>
              
            </div>
            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered r display" id="tablaUsuarios">
                  <thead>
                    <tr>
                    	<th></th>
                      <th>Nombre</th>
                      <th>Email</th>
                      <th>Pais de Residencia</th>
                      <th>Estado del proceso</th>
                      <th>Responsable</th>

                    </tr>
                  </thead>
                  <tbody>

                    @foreach($users as $user)
                    
                    <tr data-value="{{$user->data}}">
                    	<td class="details-control">+</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->residency}}</td>
                      <td>{{Form::label('process_state',($user->proccess == 1) ? 'Iniciando proceso': ($user->proccess == 2) ? 'Curriculum vitae': ($user->proccess == 3) ? 'Preinscripción y reserva de plaza': ($user->proccess ==4) ? 'Solicitud de beca' : ($user->proccess == 5) ? 'Beca Residencia' : ($user->proccess == 6) ? 'Documento de identificación' : ($user->proccess == 7) ? 'Notas' : ($user->proccess == 8) ? 'Pasaporte' : ($user->proccess == 9) ? 'Fecha de entrevista' : ($user->proccess == 10) ? 'Entrevista' : 'Proceso terminado') }}</td>
                      <td>{{ $user->responsible }}</td>
                     
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <input id="usersVal" value="{{$users}}" type="hidden">
      </div>
      <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

      <script src="{{ asset('js/admin/lista.js') }}"></script>
      <script type="text/javascript">
      	$(document).ready(function(){
      		const table = $('#tablaUsuarios').DataTable({
      			"retrieve": true,
    				"paging": false,
      			"columns":[
      				{
      					"className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
      				}
      			]
      		});
      		
      		$('#tablaUsuarios tbody').on('click','td.details-control',function(){
      			const tr = $(this).closest('tr');
      			
      			const row = table.row(tr);
      			if(row.child.isShown()){
      				row.child.hide();
      				tr.removeClass('shown');
      			}
      			else{
      				getProspectData(tr.data('value')).then(function(response) {
      					row.child(format(response)).show();
		      		}).catch(function(fail){
		      			console.log(fail);
		      		});
      				tr.addClass('shown');
      			}
      		});
      	});
      	function format ( d ) {
			    // `d` is the original data object for the row
			    let startDate = null;
					console.log('Test: ' + d);
			    if(d.start_date && d.start_date !== null && typeof d.start_date !== undefined){
			    	startDate = d.start_date;
			    }else{
			    	startDate = 'Sin asignar';
			    }
			    let endDate = null;
			    if(d.end_date !== null && typeof d.end_date !== undefined){
			    	endDate = d.end_date;
			    }else{
			    	endDate = 'Sin asignar';
			    }
			    
			    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
			        '<tr>'+
			            '<td>CV:</td>'+
			            '<td>'+d.CV+'</td>'+
									(d.CV !== 'Ingrese un documento' ? '<td><button type="button" class="btn btn-primary" onclick="downloadDocument(\'cv\',' + d.user_id + ', ' + `'${d.CV}'` + "" + ')">Descargar</button></td>' : '')+	
			        '</tr>'+
			        '<tr>'+
			            '<td>Notas:</td>'+
			            '<td>'+d.grades+'</td>'+
									(d.grades !== 'ingrese un documento' ? '<td><button type="button" class="btn btn-primary" onclick="downloadDocument(\'grades\',' + d.user_id + ', ' + `'${d.grades}'` + "" + ')">Descargar</button></td>' : '')+	
			        '</tr>'+
			        '<tr>'+
			            '<td>Fecha de entrevista:</td>'+
			            '<td>'+startDate+' - '+endDate+'</td>'+
			        '</tr>'+
			        '<tr>'+
			            '<td>Entrevistado:</td>'+
			            '<td>'+((d.interviewed == "1") ? 'Entrevistado' : 'Sin entrevistar')+'</td>'+
			        '</tr>'+
			    '</table>';
			}

			async function getProspectData(id){
				const data = await $.ajax({
					url: 'prospectData/'+id,
					type: 'get',
					dataType: 'json'
				});

				return data;
			}

			async function downloadDocument(type, userId, name) {
				const response = await fetch(`downloadDocument/${type}/${userId}`, {
					method: 'GET',
				});

				let file = await response.blob();

				let download = document.createElement('a');
				download.href = window.URL.createObjectURL(new Blob([file]));
				download.download = name;
				download.click();
			}
      </script>
    
@endsection
