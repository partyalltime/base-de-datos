@extends('layouts.app')

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
             <div class="tile-title-w-btn mb-4">
              <h3 id="titleTable" class="title">Listado de alumnos a mi cargo</h3>
            </div>
            <div class="tile-body">
              <div style="display: flex; flex-direction: row;">
                <button type="button" class="btn btn-primary" onclick="exportTableToCsv()">Exportar como CSV</button>
                &nbsp;&nbsp;
                <button type="button" class="btn btn-primary" disabled>Exportar como PDF</button>
              </div>
              <br />
              <div style="display: flex; flex-direction: row;">
                <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
                  <label class="field">Buscar</label>
                  <input type="text" class="form-control" id="search" onkeyup="search()" placeholder="Ingrese un termino de busqueda (nombre)" />
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;
                @if ($headquarters !== null)
                  <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
                    <label class="field">Sede</label>
                    <select class="form-control" onchange="setHeadquarterFilter(this.value)">
                      <option value="Todas las sedes">Todas las sedes</option>
                      @foreach($headquarters as $headquarter)   
                        <option value="{{$headquarter->name}}">{{$headquarter->name}}</option>
                      @endforeach
                    </select>
                  </div>
                @endif
              </div>
              <br />
              <div class="table-responsive">
                <table class="table table-hover table-bordered r display" id="tablaUsuarios">
                  <thead>
                    <tr class="main-table">
                      <th></th>
                      <th>Nombre</th>
                      <th>Email</th>
                      <th>Pais de Residencia</th>
                      <th>Estado del proceso</th>
                      <th>Responsable</th>
                    </tr>
                  </thead>
                  <tbody id="renderer-container" />
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script type="text/javascript">
        let userList = @json($users);

        if (window.addEventListener) {
          window.addEventListener('load', updateArray());
        }

        async function updateArray() {
          document.getElementById('renderer-container').innerHTML = null;

          await renderArray()
        }

        async function renderArray() {
          let container = document.getElementById('renderer-container');

          for (let x = 0, size = userList.length; x < size; x += 1) {
            let newElement = document.createElement('tr');

            newElement.id = window.btoa(x + new Date().getTime()).replace(/=/g, '');
            newElement.className = "main-table";
            let attr = document.createAttribute('rendered');
            attr.value = null;
            newElement.setAttributeNode(attr);

            newElement.innerHTML = `
              <td onclick="showData(${userList[x].data}, '${newElement.id}')" class="details-control"><i class="fa fa-caret-down"></i></td>
              <td>${userList[x].name}</td>
              <td>${userList[x].email}</td>
              <td>${userList[x].residency}</td>
              <td>${userList[x].proccess === 1 ? 'Iniciando proceso': userList[x].proccess === 2 ? 'Curriculum vitae': userList[x].proccess === 3 ? 'Preinscripción y reserva de plaza': userList[x].proccess === 4 ? 'Solicitud de beca' : userList[x].proccess === 5 ? 'Beca Residencia' : userList[x].proccess === 6 ? 'Documento de identificación' : userList[x].proccess === 7 ? 'Notas' : userList[x].proccess === 8 ? 'Pasaporte' : userList[x].proccess === 9 ? 'Fecha de entrevista' : userList[x].proccess === 10 ? 'Entrevista' : 'Proceso terminado' }</td>
              <td>${userList[x].responsible}</td>  
            `;

            container.appendChild(newElement);
          }
        }

      async function showData(id, elementId) {
        let data = await getProspectData(id);
        let startDate = null;
        let endDate = null;

        let newElement = document.createElement('table');
        newElement.id = (new Date().getTime().toString()).replace(/=/g, '');
        newElement.setAttribute('border', 0);
        newElement.style = 'padding-left: 50px;';
        newElement.innerHTML = `
          <thead>
            <tr>
              <th>CV</th>
              <th>Notas</th>
              <th>Documento de identidad</th>
              <th>Pasaporte</th>
              <th>Fecha de entrevista</th>
              <th>Entrevistado</th>
            </tr>
          </thead>
          <tbody>
            <tr style="padding: 3%;">
              <td>
                ${((data.documents) ? data.documents.CV : "Ingrese un documento")}
                <br />
                <br />
                ${((data.documents) ? '<button type="button" class="btn btn-primary" onclick="downloadDocument(\'cv\',' + data.prospect.user_id + ', ' + `'${data.documents.CV}'` + "" + ')">Descargar</button>' : '')}  
              </td>
              <td>
                ${((data.documents) ? data.documents.grades : "Ingrese un documento")}
                <br />
                <br />
                ${((data.documents) ? '<button type="button" class="btn btn-primary" onclick="downloadDocument(\'grades\',' + data.prospect.user_id + ', ' + `'${data.documents.grades}'` + "" + ')">Descargar</button>' : '')}  
              </td>
              <td>
                ${((data.documents) ? data.documents.identification_document : "Ingrese un documento")}
                <br />
                <br />
                ${((data.documents) ? '<button type="button" class="btn btn-primary" onclick="downloadDocument(\'identification_document\',' + data.prospect.user_id + ', ' + `'${data.documents.identification_document}'` + "" + ')">Descargar</button>' : '')}  
              </td>
              <td>
                ${((data.documents) ? data.documents.passport : "Ingrese un documento")}
                <br />
                <br />
                ${((data.documents) ? '<button type="button" class="btn btn-primary" onclick="downloadDocument(\'passport\',' + data.user_id + ', ' + `'${data.documents.passport}'` + "" + ')">Descargar</button>' : '')}  
              </td>
              <td>
                ${startDate} - ${endDate}
              </td>
              <td>
                ${((data.prospect.interviewed == "1") ? 'Entrevistado' : 'Sin entrevistar')}
              </td>
            </tr>
          </tbody>
        `;

        let parentElement = document.getElementById(elementId);
        let rendered = parentElement.getAttribute('rendered');

        if (rendered === 'null') {
          parentElement.setAttribute('rendered', newElement.id);
          parentElement.parentNode.insertBefore(newElement, parentElement.nextSibling);
        } else {
          let elementToRemove = document.getElementById(rendered);
          elementToRemove.remove();
          parentElement.setAttribute('rendered', null);
        }
      }

      async function setHeadquarterFilter(headquarterId) {
        if (headquarterId === 'Todas las sedes') {
          userList = @json($users);
          await updateArray();
        } else {
          userList = @json($users);
          userList = userList.filter(user => {
            return user.residency === headquarterId;
          });

          await updateArray();
        }
      }
        
      async function search() {
        let input, filter, table, tr, td, value;

        input = document.getElementById('search');
        filter = input.value.toUpperCase();
        table = document.getElementById('tablaUsuarios');
        tr = table.getElementsByTagName('tr');

        for (let x = 0, length = tr.length; x < length; x += 1) {
          td = tr[x].getElementsByTagName('td')[1];

          if (td) {
            value = td.textContent || td.innerText;

            if (value.toUpperCase().indexOf(filter) > -1) {
              tr[x].style.display = '';
            } else {
              tr[x].style.display = 'none';
            }
          }
        }
      }

      async function getProspectData(id){
        const data = await $.ajax({
          url: 'prospectData/'+id,
          type: 'get',
          dataType: 'json'
        });

        return data;
      }

      async function downloadDocument(type, userId, name) {
        const response = await fetch(`downloadDocument/${type}/${userId}`, {
          method: 'GET',
        });

        let file = await response.blob();
        let download = document.createElement('a');

        download.href = window.URL.createObjectURL(new Blob([file]));
        download.download = name;
        download.click();
      }

      function exportTableToCsv() {
        let csv = [];
        let table = document.getElementById("tablaUsuarios");
        let rows = table.querySelectorAll("tr.main-table");

        for (let x = 0, size = rows.length; x < size; x += 1) {
          let row = [];
          let cols = rows[x].querySelectorAll("td, th");

          for (let y = 0; y < cols.length; y += 1) {
            if (cols[y].innerText)
              row.push(cols[y].innerText);
          }

          csv.push(row.join(","));
        }

        csv = csv.join('\n');
        let csvFile = new Blob([csv], {type: 'text/csv'});
        let download = document.createElement('a');

        download.download = 'Tabla-de-responsables.csv';
        download.href = window.URL.createObjectURL(csvFile);

        download.click();
      }
      </script>
    
@endsection
