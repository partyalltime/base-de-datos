<div class="form-group">
    {{Form::label('name','Nombre de la Categoría')}}
    {{Form::text('name', null, ['class'=> 'form-control', 'id'=>'name'])}}
</div>

<div class="form-group">
    {{Form::label('body', 'Descripción')}}
    {{Form::textarea('body', null, ['class'=> 'form-control'])}}
</div>
<div class="form-group">
    {{Form::submit('Guardar',['class'=> 'btn btn-sm btn-primary']) }}
</div>
@section('scripts')
@section('js')
<script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
 <script>
        CKEDITOR.replace( 'body' );
</script>
@endsection
@endsection