@extends('layouts.app')
@section('content')
<div class="container">
  <h1>Configuración de sede</h1>
  <br />
  <div class="tile">
    <div class="tile-body">
      <form method="POST" action="{{route('updateHeadquarterPreferences')}}">
        @csrf
        <input type="hidden" name="country" value="{{$country_settings['country']}}" />
        <input type="hidden" name="headquarter_country" value="{{$headquarter_country}}" />
        <h5>Grados academicos admitidos por la sede</h5>
        <div class="tag-border" id="tag-container-1" style="display: flex; flex-direction: column;">
          <input type="hidden" id="tags-input-1" name="tags-input-1" value="" />
          <input type="text" class="form-control" id="tags-1" style="width: 100%; margin-bottom: 10px;" onkeydown="addTag(this, 'tags_lvl', 1)" />
          <div id="after-tag-1" style="display: flex; flex-direction: row; flex-wrap: wrap;"></div>
        </div>
        <br />
        <h5>Ciclos academicos admitidos por la sede</h5>
        <div class="tag-border" id="tag-container-2" style="display: flex; flex-direction: column;">
          <input type="hidden" id="tags-input-2" name="tags-input-2" value="" />
          <input type="text" class="form-control" id="tags-2" style="width: 100%; margin-bottom: 10px;" onkeydown="addTag(this, 'tags_cly', 2)" />
          <div id="after-tag-2" style="display: flex; flex-direction: row; flex-wrap: wrap;"></div>
        </div>
        <br />
        <h5>Entrevistador de la sede</h5>
        <div style="display: flex; flex-direction: column">
          {{Form::select('interviewer', $interviewers, null, array("class"=>'form-control', "placeholder" => 'Seleccione un entrevistador'))}}
        </div>
        <br />
        <br />
        @yield('tags_lvl')
        <button type="submit" class="btn btn-primary">Guardar preferencias</button>
      </form>
    </div>
  </div>
</div>
@endsection
<script type="text/javascript">
  let tags_lvl = [];
  let tags_cly = [];

  function tagsUpdated() {
    document.getElementById('tags-input-1').value = tags_lvl;
    document.getElementById('tags-input-2').value = tags_cly;
  }

  window.onload = () => {
    tags_cly = @json($country_settings['academic_cycle_restrictions']) !== null ? @json($country_settings['academic_cycle_restrictions']) : [];
    renderTags('tags_cly', 2);
    tags_lvl = @json($country_settings['academic_level_restrictions']) !== null ? @json($country_settings['academic_level_restrictions']) : [];
    renderTags('tags_lvl', 1);
    tagsUpdated();
  };

  function returnTagArray(tagType) {
    if (tagType === 'tags_lvl') {
      return tags_lvl;
    } else {
      return tags_cly;
    }
  }

  function renderTag(value, tagType, number) {
    let newTagDiv = document.createElement("div");
    let newTagText = document.createElement("span");

    newTagDiv.style = `
      background-color: #0081c6;
      color: white;
      text-align: center;
      margin: 2px;
      padding-top: 4px;
      padding-bottom: 4px;
      padding-left: 8px;
      padding-right: 8px;
      border-radius: 15px;`;

    newTagDiv.title = "Borrar";
    newTagDiv.id = `${Date.now() + Math.random().toString()}-tag-div-${number}`;
    newTagText.innerText = value;
    newTagText.id = `${Date.now() + Math.random().toString()}-tag-text-${number}`; 

    document.getElementById(`after-tag-${number}`).appendChild(newTagDiv);
    document.getElementById(newTagDiv.id).appendChild(newTagText);
    document.getElementById(newTagDiv.id).addEventListener('click', () => {
      returnTagArray(tagType).splice(returnTagArray(tagType).indexOf(newTagDiv.innerText), 1);
      document.getElementById(newTagDiv.id).remove();
      document.getElementById(`tags-input-${number}`).value = returnTagArray(tagType);
    });
  }

  function renderTags(tagType, number) {
    let tags = returnTagArray(tagType);

    for (let x = 0, length = tags.length; x < length; x += 1) {
      renderTag(tags[x], tagType, number);
    }
  }

  function addTag(e, tagType, number) {
    if (event.key === ',' && e.value.trim().length) {
      event.preventDefault();
      returnTagArray(tagType).push(e.value);

      renderTag(e.value, tagType, number);

      e.value = null;
      tagsUpdated();
    }
  }
</script>