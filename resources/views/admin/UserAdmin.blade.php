@extends('layouts.app')

@section('content')
<div>
  <div class="container">
    <h1>Administración de usuarios</h1>
    <br />
    <div class="tile">
      <div class="tile-title-w-btn">
        <h3>Usuarios actuales</h3>
        <button class="btn btn-primary" onclick="window.location='{{ url("users/admin/create") }}'">Crear usuario</button>
      </div>
      <br />
      <div class="tile-body">
        <table class="table table-hover table-striped"> 
          <thead>
            <tr>
              <th>Nombre</th>
              <th>E-Mail</th>
              <th>Rol</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
              <tr>
                <td>{{$user['name']}}</td>
                <td>{{$user['email']}}</td>
                @if($user['role_id'] === 1)
                  <td>Administrador</td>
                @elseif($user['role_id'] === 2)
                  <td>Gerente</td>
                @elseif($user['role_id'] === 3)
                  <td>Editor</td>
                @elseif($user['role_id'] === 4)
                  <td>Prospecto</td>
                @elseif($user['role_id'] === 5)
                  <td>Nuevo interno</td>
                @elseif($user['role_id'] === 6)
                  <td>Interno</td>
                @elseif($user['role_id'] === 13)
                  <td>Entrevistador</td>
                @endif
                <td>
                  <button class="btn btn-primary" onclick="updateUser({{$user['id']}})">Editar</button>
                </td>
                <td>
                  <button class="btn btn-danger" onclick="deleteUser({{$user['id']}})">Eliminar</button>
                </td>
              <tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  async function deleteUser(userId) {
    const response = await fetch(`/user/${userId}`, {
      method: 'DELETE',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
      },
    });

    if (response) {
      location.reload();
    }
  }

  function updateUser(userId) {
    location.assign(`/users/admin/update/${userId}`);
  } 
</script>
@endsection