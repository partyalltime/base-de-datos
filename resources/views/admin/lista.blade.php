@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">  
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
             <div class="tile-title-w-btn mb-4">
              <h3 id="titleTable" class="title">Listado por cursos</h3>
              <p><a id="change-view-btn" class="btn btn-primary icon-btn" href="#"><i class="fa fa-th-list"></i><span>Mostrar por inscritos</span></a></p>
            </div>
            <div class="tile-body">
              <div class="table-responsive">
                <table class="table table-hover table-bordered r" id="tablaUsuarios">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Email</th>
                      <th>Pais de Residencia</th>
                      <th>Curso</th>
                      <th>Responsable</th>
                      <th>Estado</th>
                    </tr>
                  </thead>
                  <tbody>

                    @foreach($users as $user)
                    
                    <tr>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->residency}}</td>
                      <td>{{ $user->post }}</td>
                      <td>{{ $user->responsible }}</td>
                      <td>
                      @if ($user->completed === 1)
                        {!!$user->completed='<span class="badge badge-pill badge-secondary">Terminado</span>'!!}
                      @else
                        {!!$user->completed='<span class="badge badge-pill badge-primary">En curso</span>'!!}
                      @endif
                      </td>
                    </tr>

                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <input id="usersVal" value="{{$users}}" type="hidden">
      </div>
      <script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('js/plugins/dataTables.bootstrap.min.js') }}"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

      <script src="{{ asset('js/admin/lista.js') }}"></script>
    
@endsection

    