@extends('layouts.app')
@section('content')
<style type="text/css">

 .contacts{
   font-size: 1.2rem;
   color: gray;
 }
 .data-group{
   
   width:auto;
   left: auto; 
   margin-left: auto;
   margin-right: auto; 
   text-align: center;
   overflow-wrap: break-word;
   hyphens: auto;
 }

 .data-group div { 
   display: table-row;
   text-align: left;  
   overflow-wrap: break-word;
   word-break: break-all;
   word-wrap: break-word;
 }

 .data-group#preline  {
  white-space: pre-line;  
  }

 .data-group div label { 
   text-align: left;
   overflow-wrap: break-word;
   word-wrap: break-word;
   hyphens: auto; 
   
 }

 .field{
   font-weight: bold;
   white-space: pre-line;
 }

 a:hover,a:focus{
   text-decoration: none;
   outline: none;
 }
 .tab-content{ f
   min-height: 97%;
   min-width: 300px;
   -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
 }

 .nav-tabs li a {
  padding: 10px 20px;
  margin: 0 10px -1px 0;
  font-weight: 600;
  color: #293241;
  text-transform: uppercase;
  border-right: 2px solid #e6e5e1 !important;
  border-left: 2px solid #e6e5e1 !important;
  border-top: 2px solid #e6e5e1 !important;
  border-radius: 5px 5px 0 0;
  z-index: 1;
  position: relative;
  transition: all 0.3s ease 0s;
  top: -1px;
}
.nav-tabs .nav-item {
  margin-bottom: -1px;
}
.nav-tabs li a:hover, .nav-item a.nav-link.active {
  background: #fff;
  color: #0081c6;
  border-right: 2px solid #0081c6 !important;
  border-left: 2px solid #0081c6 !important;
  border-top: 2px solid #0081c6 !important;
}

#top-pic{
  height: 160px;
  width: auto;
  background: url(../images/bg-02.png) no-repeat right bottom;
  background-size: contain;
  background-color: #0081c6 !important;
  margin: 0;

}
#avatar {
  width: 170px;
  height: 170px;
  top: -148px;
  left: 0; 
  right: 0;
  margin-left: auto;
  margin-right: auto; 
  border-radius: 100%;
  box-shadow: 0 0 0 7px #f8f9fa;
  position: absolute;
  z-index: 1;
  background-color: #ddd;
}
.info{
  height: 50%;
  width: 100%;
  display: table;
  left: 0;
  bottom: 0;

}
.titles hr{
  width: 157px;
  margin-top:0;
  margin-bottom:7px;
  border: 1px solid #e6e5e1;
}
#student-name{
  font-size: 1.7rem;
  margin-top: 8px;
}

#student-user{
  font-size: 13px;
}

#country{
  font-size: 1.2rem;
  margin-bottom: 5px;
  color: gray;
}
.punctuation-score{
  font-size: 1.9rem;
  color: #0081c6;
}
/*Inicio Notificaciones*/
/*
.notifications{
  position: absolute;
  rigth: 0;
  left: 96vw;
  top: 90vh;
  border-radius: 0.2em;
  min-width: 55px;
  min-height: 45px;
  background-color: #bbb;
  font-size: 2.2em;
  text-align: center;
  padding-top: .4%;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
}
.notifications::before, 
.notifications::after {
    color: #fff;
    text-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);
}

.notifications::before {
    display: block;
    content: "\f0f3";
    font-family: "FontAwesome";
    transform-origin: top center;
}

.notifications::after {
    font-family: Arial;
    font-size: 0.7em;
    font-weight: 700;
    position: fixed;
    top: -15px;
    right: -15px;
    padding: 5px 8px;
    line-height: 100%;
    border: 2px #fff solid;
    border-radius: 60px;
    background: #3498db;
    opacity: 0;
    content: attr(data-count);
    opacity: 0;
    transform: scale(0.5);
    transition: transform, opacity;
    transition-duration: 0.3s;
    transition-timing-function: ease-out;
}

.notifications:hover{
  background-color: #bbbb;
}
.notification{
	width: 30vw;
	height: 55vh;
	right: 1%;
  top:32vh;
	border: none;
	border-radius: 0.5em;
	box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);  derecha, abajo, radio de difuminación, color 
	position: absolute;
	outline: none;
	margin-right: 5px;
	margin-bottom: 5px;
	background-color: #ffffff;
	transform: scale(1);	
  font-size: 1em;
  text-align: justify;
  padding: 0.5%;
  display: flex;
  flex-direction: column;
  font-family: sans-serif;
}
.noVisible{
	visibility: hidden;
}
.notification-box{
  background-color: #ffffff;
  min-width: 96%;
	min-height: 20%;
  border-radius: 0.5em;
  margin: 2%;
  box-shadow: 0 0 0.4em rgba(0, 0, 0, 0.5);
}
.box-header{
  background-color: #287cc0;
  border-radius: 0.5em 0.5em 0 0;
  margin: 0;
  display: block;
  padding: 0.9%;
  color: white;
}
.box-close{
  min-width: 50%;  
  left: 90%;
  color: white;
  right: 0;
}
.box-header .box-close:hover{
  color: black !important;
  cursor: pointer;
}
.box-header .box-titulo{
  color: white;
  font-size: 1.1em;
  position: relative;
  left: 2%;
}
.box-header .box-titulo:hover{
  color: #cdcdcd;
  font-size: 1.1em;
  cursor: pointer;
}
.all-notifications{
  color: #3333cc;
  position: absolute;
  top: 96%;
  left: 50%;
}
.all-notifications:hover{
  color: #8888cc;
  cursor: pointer;
}
.box-body{
  padding: 2.5%;
  font-size: calc(3px + (23 - 3) * ((100vw - 800px) / (2900 - 800)));
}*/

/*Fin notificaciones*/ 
</style>
<div class="container mt-3" >
  <div class="row justify-content-center">
    <div class="col-md-9">
     <!--Tabs-->
     <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" onClick="changeValue(2)">Acerca de mí</a>
      </li>
      @if(isset($info["user_data"]))
      <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" onClick="changeValue(1)">Mis Aptitudes</a>
      </li>
      @endif
    </ul>
    <!--titulo -->
    <!--fin titulo-->
    <!--Tabs content-->
    <div class="tab-content" id="myTabContent">
      <!--Tab 1--->
      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
       <div class="row">
        <div class="col-12">
         <div id="top-pic">
           <div class="tile-title-w-btn text-right bg-primary">
            <div class="btn-group">
              <a class="btn btn-primary go"  >
                <i class="fa fa-lg fa-edit" id="pencil"></i> 
              </a>
            </div>
          </div>       
        </div>                    
      </div>
    </div>


    <div class="row p-3">
      <!--Parte  1-->
      <div class="col-lg-5 col-sm-12 text-center" id="basicdata">
       <div id="avatar" class="mt-3">
        <img src="https://image.flaticon.com/icons/svg/1974/1974887.svg" class="rounded-circle" width="170px" height="170px">
      </div>
      <div id="info-box" class="mt-5">       
        <div class="titles">
          <form action="{{url('requestScholarship')}}" method="GET">
            @if ($info['user'][0]["requested_scholarship"] === 0)
            	{{Form::submit(($info['user'][0]["approved"] == 0) ? 'Solicitar Beca': 'Becado',array('class'=>($info['user'][0]["approved"] == 0) ? 'btn btn-outline-info beca': 'hidden d-none', 'href'=>'/requestScholarship'))}}
            @else
            	{{Form::submit(($info['user'][0]["approved"] == 0) ? 'Seguir proceso': 'Becado',array('class'=>($info['user'][0]["approved"] == 0) ? 'btn btn-outline-info beca': 'hidden d-none', 'href'=>'/process'))}}
            @endif
          </form>
          <div id="student-name">{{Form::label('name',$info['user'][0]['name'],array('class'=>'name'))}}</div> 
          <div id="student-user" class="text-muted">{{Form::label('mail',$info['user'][0]['email'],array('class'=>'email'))}}</div>
          <hr> 
        </div>
        <div id="country"><i class="fa fa-map-marker" aria-hidden="true" style="color: #0081c6;"></i> @if($info['user'][0]['residency'] != null)
          {{Form::label('data',$info['user'][0]['residency'])}}
          @else
          {{Form::label('data',"Información no disponible")}}
          @endif</div>
        <div id="punctuations">
         <div class="row">
          {{-- <div class="col-6 text-right">
           		<div class="punctuation-score">1</div>
           <div class="punctuation-name">Curso/s</div>
         </div>
         <div class="col-6 text-left">
           <div class="punctuation-score">5</div>
           <div class="punctuation-name">Nivel</div>                            
         </div> --}}
       </div>
     </div>
   </div>  
 </div><!--Part 1 closes--->

 <!--Part 2-->
 <div class="col-lg-7 col-sm-12 mt-5 text-center px-4">

      <div class="data-group">
        <div>
          {{Form::label('contact','Información básica',array('class'=>'contacts'))}}
        </div>

        <div>
          {{Form::label('phone','Fecha de nacimiento:',array('class'=>'field'))}}
          @if($info['user'][0]['birthday'] != null)
          {{Form::label('data',$info['user'][0]['birthday'])}}
          @else
          {{Form::label('data',"Información no disponible")}}
          @endif
        </div>
        <div>
          {{Form::label('academic_level','Nivel académico:',array('class'=>'field'))}}
          @if($info['user'][0]['academic_level'] != null)
          {{Form::label('data',$info['user'][0]['academic_level'])}}
          @else
          {{Form::label('data',"Información no disponible")}}
          @endif
        </div>
        <div>
          {{Form::label('academic_cycle','Ciclo académico:',array('class'=>'field'))}}
          @if($info['user'][0]['academic_cycle'] != null)
          {{Form::label('data',$info['user'][0]['academic_cycle'])}}
          @else
          {{Form::label('data',"Información no disponible")}}
          @endif
        </div>
        <div>
          {{Form::label('gender','Genero: ',array('class'=>'field'))}}
          {{Form::label('data',($info['user'][0]['gender'] == 0)? 'Hombre': 'Mujer')}}
        </div>

        <br>

        <div>
          {{Form::label('contact','Información de contacto',array('class'=>'contacts'))}}
        </div>

        <div>
          {{Form::label('phone','Telefono:',array('class'=>'field'))}}
          @if($info['user'][0]['phone'] != null)
          {{Form::label('data',$info['user'][0]['phone'])}}
          @else
          {{Form::label('data',"Información no disponible")}}
          @endif
        </div>
      </div>
    </div><!--Part 2 closes--->

</div><!--row tab 1 closes--->
</div><!--Tab 1 closes--->

    @if(isset($info["user_data"]))
      <!--Tab 2--->
      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
       <div class="row">
        <div class="col-12">
         <div id="top-pic">
           <div class="tile-title-w-btn text-right bg-primary">
            <div class="btn-group">
              <a class="btn btn-primary go">
                <i class="fa fa-lg fa-edit" id="pencil"></i>
              </a>
            </div>
          </div>       
        </div>                    
      </div>
    </div>


    <div class="row p-3">
      <!--Parte  1-->
      <div class="col-lg-5 col-sm-12 text-center" id="basicdata">
       	<div id="avatar" class="mt-3">
        	<img src="https://image.flaticon.com/icons/svg/1974/1974887.svg" class="rounded-circle" width="170px" height="170px">
      	</div>
      	<div id="info-box" class="mt-5">       
        	<div class="titles">
            <form action="{{url('requestScholarship')}}" method="GET">
	            @if ($info['user'][0]["requested_scholarship"] === 0)
	            	{{Form::submit(($info['user'][0]["approved"] == 0) ? 'Solicitar Beca': 'Becado',array('class'=>($info['user'][0]["approved"] == 0) ? 'btn btn-outline-info beca': 'hidden d-none', 'href'=>'/requestScholarship'))}}
	            @else
	            	{{Form::submit(($info['user'][0]["approved"] == 0) ? 'Seguir proceso': 'Becado',array('class'=>($info['user'][0]["approved"] == 0) ? 'btn btn-outline-info beca': 'hidden d-none', 'href'=>'/process'))}}
	            @endif
          	</form>
            <div id="student-name">{{Form::label('name',$info['user'][0]['name'],array('class'=>'name'))}}</div> 
          	<div id="student-user" class="text-muted">{{Form::label('mail',$info['user'][0]['email'],array('class'=>'email'))}}</div>
        		<hr> 
        	</div>
        	<div id="country"><i class="fa fa-map-marker" aria-hidden="true" style="color: #0081c6;"></i> @if($info['user'][0]['residency'] != null)
          	{{Form::label('data',$info['user'][0]['residency'])}}
          	@else
          		{{Form::label('data',"Información no disponible")}}
          	@endif
        	</div>
      		<div id="punctuations">
         		<div class="row">
          		<div class="col-6 text-right">
           			{{-- <div class="punctuation-score">1</div>
           			<div class="punctuation-name">Curso/s</div> --}}
         			</div>
         			<div class="col-6 text-left">
           			{{-- <div class="punctuation-score">5</div>
           			<div class="punctuation-name">Nivel</div>  --}}                           
         			</div>
       			</div>
     			</div>
   			</div>  
 			</div>
 <!--Part 1 closes--->
 <!--Part 2-->
		<div class="col-lg-7 col-sm-12  text-center px-4">
      <div class="data-group">
      <div class="data-group">
        @foreach($info["user_data"] as $key => $value)
        <div>
          {{Form::label('key',$key.':',array('class'=>'field'))}}   
          {{Form::label('Información no disponible',$value)}}
        </div>
        <br/>
        @endforeach
      </div>
      @endif
    </div><!--Part 2 closes--->
  <!-- FALTA ENDIF -->
		</div><!--row tab 2 closes--->
</div><!--Tab 2 closes--->

</div><!--Tab content closes-->
<!-- fin row -->
</div><!--col-md-9 closes-->
{{-- <div class="notifications"></div>
<div class="notification noVisible" id="box"><a class="all-notifications">Ver todas las notificaciones</a></div> --}}
</div><!--row justify-content-center closes-->
</div><!--container mt-3 closes-->

<script type="text/javascript">
	var valor= 2;

  function readNotification(id){
    $.ajax({
      url:'/readNotification',
      type:'post',
      dataType: 'json',
      headers: {
        'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
      },
      data: {"notId": id}
    }).done(function(){
      $("#notId-"+id).remove();
    }).fail(function(){
      toastFunction("There was an error in the petition");
    })
  }
  function getNotifications(){
    $.ajax({
      url:'/profile',
      type:'get',
      dataType: 'json',
      headers: {
        'X-CSRF-TOKEN': $("meta[name=csrf-token]").attr("content")
      },
    }).done(function(response){
      if(response.length > 0){
        $( "#notifications" ).addClass("alert alert-dismissible alert-success");
      }
      let notificate= '';
      for(const [key,value] of  Object.entries(response)){
      	notificate += '<div class="notification-box" id="notId-'+value.id+'">';
        notificate += '<div class="box-header">';
        notificate += '<a class="box-titulo">'+value.type+' - '+value.date+' <i class="fa fa-times-circle"></i></a>';
        //notificate += '<a class="box-close"><i class="fa fa-times-circle"></i></a>';
        //notificate += '<a class="box-close" onClick="readNotification('+value.id+')><i class="fa fa-times-circle"></i></a>';
        notificate += '</div>';
        notificate += '<div class="box-body" class="close" aria-label="Close">'+value.message+'</div>';
      	notificate +=   '</div>';
      }
      $(notificate).appendTo(".notification");
    })
  }
  function changeValue(value){
  	let val = value;
  	valor = val;
  }
  $(".go").on('click',function(){
  	let url = '{{ route("editarPerfil", ":slug") }}';
		url = url.replace(':slug', valor);
		//console.log(url);
		window.location.href=url;
  });
  $(document).ready(function(){
  	
    /*getNotifications();
    $('.notifications').on('click',()=>{
		  if($('#box').hasClass('noVisible')){
		     $('#box').removeClass('noVisible');
		  }else{
		    $('#box').addClass('noVisible');
		  }
		});*/
  });
</script>


@endsection