@extends('layouts.app')

@section('content')
<div class="row">
<div class="col-md-2">
</div>
        <div class="col-md-8">
          <div class="tab-content">
            <div class="tab-pane active" id="user-settings">
              <div class="tile user-settings">
                <h4 class="line-head">{{$user->name}} - {{$user->email}}</h4>
                <input id="prospect_id" type="hidden" value="{{$id}}">
                <form>
                  <div class="form-group">                    
                      <label class="col-form-label">Dia de llegada</label>
                      <input id="arrival" class="form-control datetime-local" type="date">
                  </div>
                  <div class="form-group">
                    <label for="level">Nivel de experiencia del 1 al 5</label>
                    <select class="form-control" id="level">
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="internship">Compañia asignada</label>
                    <select class="form-control" id="internship">
                      <option value="---" disabled selected>---</option>
                      @foreach($companies as $company)
                        <option value="{{$company->id}}">{{$company->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="enrolled" id="enrolled" value="enrolled">
                        Matriculado en universidad
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="pre_registered" id="pre_registered" value="pre_registered">
                        Registrado anteriormente
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="granted" id="granted" value="granted">
                        Beca concedida
                      </label>
                    </div>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="notified" id="notified" value="notified">
                        Notificado
                      </label>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="scholarship">Porcentaje de beca concedida</label>
                    <select class="form-control" id="scholarship">
                      <option value="25">25%</option>
                      <option value="50">50%</option>
                      <option value="75">75%</option>
                      <option value="100">100%</option>
                    </select>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <button id="create-btn" class="btn btn-primary" type="button" style="float: right">Agregar nuevo becario <i class="fa fa-fw fa-lg fa-save"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script src="{{ asset('js/admin/createNewIntern.js') }}"></script>
</div>
@endsection
