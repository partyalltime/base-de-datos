

{{ Form::hidden('user_id',auth()->user()->id) }}

<div class="form-group">
    {{Form::label('category_id','Categorías')}}
    {{Form::select('category_id', $categories, null,  ['class'=> 'form-control', 'id'=>'category'])}}
</div>
<div class="form-group">
    {{Form::label('name','Nombre del Post')}}
    {{Form::text('name', null, ['class'=> 'form-control', 'id'=>'name'])}}
</div>

<div class="form-group">
    {{Form::label('file','Images')}}
    {{Form::file('file')}}
</div>

<div class="form-group">
    {{Form::label('status', 'Estado')}}
        <label >
            {{ Form::radio('status', 'PUBLISHED')}} Publicado  
        </label>
        <label >
            {{ Form::radio('status', 'DRAFT')}} Borrador 
        </label>
</div>
<div class="form-group">
    {{Form::label('tags', 'Etiquetas')}}
    <div>
    @foreach($tags as $tag)
        <label >
            {{ Form::checkbox('tags[]', $tag->id)}} {{ $tag->name}}
        </label>
    @endforeach
   </div>
</div>

<div class="form-group">
    {{Form::label('excerpt','Extracto')}}
    {{Form::textarea('excerpt', null, ['class'=> 'form-control', 'rows'=>'2'])}}
</div>
<div class="form-group">
    {{Form::label('body', 'Description')}}
    {{Form::textarea('body', null, ['class'=>'form-control','id'=>'body'])}}
   
</div>
<div class="form-group">
    {{Form::submit('Guardar',['class'=> 'btn btn-sm btn-primary']) }}
</div>


@section('js')
<script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
 <script>
        CKEDITOR.replace( 'body' );
</script>
@endsection
                   