@extends('layouts.app')

@section('content')
<div class="container">
	 <div class="row justify-content-center">
        <div class="col-md-12">
        	<pre><code class="language-php">location / {
    try_files $uri $uri/ /index.php?$query_string;
}
});</code></pre>





        	 <a id="btn" class="btn btn-secondary" href="#">Code</a>
        	<div id="editor" placeholder="Escribe aquí"></div>
        </div>
     </div>
</div>
<script type="text/javascript">
	$('#editor').trumbowyg({
		plugins: {
		    upload: {
		    	serverPath: '{{ asset('/') }}file/upload',
		    	headers:{
		    		'X-CSRF-TOKEN': "{{ csrf_token() }}"
		    	},
		    	urlPropertyName: 'url'
		    }
		},
		btnsDef: {
	        // Create a new dropdown
	        image: {
	            dropdown: ['insertImage', 'upload', 'noembed'],
	            ico: 'insertImage'
	        },
	        formato:{
	        	dropdown: ['preformatted', 'strong', 'em', 'del','superscript', 'subscript'],
	            ico: 'preformatted'
	        }
	    },
		btns: [
			['viewHTML'],
	        ['undo', 'redo'], // Only supported in Blink browsers
	        ['formatting'],
	        ['foreColor', 'backColor'],
	        ['formato'],
	        ['fontsize'],
	        ['lineheight'],
	        ['link'],
	        ['image'],
	        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
	        ['unorderedList', 'orderedList'],
	        ['horizontalRule'],
	        ['removeformat'],
	        ['fullscreen']
	    ],
		lang: 'es',
		autogrow: true,
		imageWidthModalEdit: true,
		tagsToRemove: ['script', 'link']
	});
	$("#btn").click(function(e) {
		console.log($('#editor').trumbowyg('html'));
	});

	/*var $modal = $('#editor').trumbowyg('openModal', {
    title: 'A title for modal box',
    content: '<p>Content in HTML which you want include in created modal box</p>'
});
	$('#editor').trumbowyg('closeModal'); */
</script>
@endsection