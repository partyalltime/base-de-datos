@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
       <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Lista de Post
                    <a href="{{ route('posts.create') }}" class="btn btn-sm btn-primary pull-right">  {{ __('Crear') }}  </a>
                </div>
            
            <div class= "panel-body">
                    <table class="table table-striped table-hover" >
                    <thead>
                    <tr>
                    <th width = "10px">ID</th>
                    <th>user_id</th>
                    <th>category_id</th>
                    <th>name</th>
                    <th>slug</th>
                    <th>excerpt</th>
                    <th>body</th>
                    <th>status</th>
                    <th>file</th>
                    <th>Creado</th>
                    <th>Modificado</th>
                    <th colspan="3">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                    <tr>
                    <td>{{ $post->id}}</td>
                    <td>{{ $post->user_id}}</td>
                    <td>{{ $post->category_id}}</td>
                    <td>{{ $post->name}}</td>
                    <td>{{ $post->slug}}</td>
                    <td>{{ $post->excerpt}}</td>
                    <td>{{ $post->body}}</td>
                    <td>{{ $post->status}}</td>
                    <td>{{ $post->file}}</td>
                    <td>{{ $post->created_at}}</td>
                    <td>{{ $post->update_at}}</td>
                    <td width="10px">
                    <a href="{{ route('posts.show',$post->id) }}" class="btn btn-sm btn-default">ver</a>
                    </td>
                    <td width="10px">
                    <a href="{{ route('posts.edit',$post->id) }}" class="btn btn-sm btn-default">editar</a>
                    </td>
                    <td width="10px">
                    {!! Form::open(['route'=> ['posts.destroy',$post->id],
                     'method' => 'DELETE']) !!}
                     <button class="btn btn-sm btn-danger">eliminar</button>
                     {!! Form::close() !!}
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    {{ $posts->render() }}
                </div>
            </div>   
        </div>
    </div>
</div>
 
@endsection