@extends('layouts.app')

@section('content')
<div class="container">
    <input id="post-id" type="hidden" value="{{$post->id}}">
    <style>
        .card-header{
            min-height: 0 !important;
        }
    </style>
    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
    <!-- Modal -->
    
    <div class="modal" tabindex="-1" role="dialog" id="element-form">
        <div class="modal-dialog" role="document" style="max-width: 700px;">
            <div class="modal-content">
                <div class="alert alert-danger" style="display:none"></div>
                <div class="modal-header">
                    <h5 class="modal-title" id="item-modal-title">Add Item</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-4">
                        <label for="item-title">Title:</label>
                        <input type="text" class="form-control" name="item-title" id="item-title">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="item-time">Time (in minutes):</labeonclick="dl>
                            <input type="number" class="form-control" name="item-time" id="item-time">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                        <label for="item-body">Body:</label>
                        <div name="item-body" id="item-body"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button  class="btn btn-success" id="submit-item">Save</button>
                    <div id="footer-modify" style="display: none;">
                        <input type="hidden" id="item-modify-id">
                        <button  class="btn btn-danger" id="delete-item">Delete</button>
                        <button  class="btn btn-success" id="update-item">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End-->
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-title-w-btn">
                        <h3 class="title">{{$post->name}}</h3>
                        <div>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#element-form" id="add-item-button">
                                <i class="fa fa-plus"></i>
                                Añadir preguntar
                            </button>
                            <button class="btn btn-primary" onclick="openModal()">
                                <i class="fa fa-plus"></i>
                                Añadir examen
                            </button>
                        </div>
                    </div>
                    <div class="tile-footer text-muted">
                        <div class="card" style="width: 100%;">
                            @if(count($items) <= 0)
                                <div>
                                    <h3>No hay preguntas en este curso.</h3>
                                </div>
                            @endif
                            <ul class="list-group list-group-flush" id="elementsContainer">
                                @if(count($items) > 0)
                                    @foreach ($items as $element)
                                        <li class="list-group-item" id="item-li-{{$element->id}}">
                                            <input type="hidden" class="info" id="item-info-{{$element->id}}" value="{{$element}}">
                                            <h4>
                                                {{$element->order}}. {{$element->title}}
                                                <button class="btn btn-primary post-items" style="float: right" item-id="{{$element->id}}">Modificar</button>
                                            </h4>
                                            
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 100%; padding-left: 30px; padding-right: 30px;">
          <div class="tile" style="width: 100%;">
            <h2>Examenes en este curso</h2>
            <br />
            @if(count($exams) <= 0)
              <h3>No hay examenes en este curso.</h3>
            @endif
            @if(count($exams) > 0)
              @foreach ($exams as $exam)
                <li class="list-group-item" id="item-li-{{$exam->id}}" style="display: flex; justify-content: space-between;">
                  <h4>
                    {{$exam->name}}
                  </h4>
                  <div>
                    <button class="btn btn-primary" onclick="editTest({{$exam->id}})">Editar</button>
                    <button class="btn btn-danger" onclick="deleteTest({{$exam->id}})">Borrar</button>
                  </div>
                </li>
              @endforeach
            @endif
          </div>
        </div>
    </div>
    <div class="test-modal-dialog" id="test-modal-dialog">
        <div class="modal-dialog" role="document" style="max-width: 700px;">
            <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="item-modal-title">Crear nuevo examen</h5>
                </div>
                <div class="modal-body">
                    <div class="test-items-container" id="test-items-container"></div>
                    <div style="display: flex; flex-direction: row; justify-content: flex-end;">
                      <button type="button" class="btn btn-primary" onclick="addQuestion()">Agregar pregunta</button>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" onclick="dismissModal()">Descartar</button>
                  <button type="button" class="btn btn-primary" onclick="postTest()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/itemTools.js') }}"></script>
</div>
<style>
  .test-modal-dialog {
    left: 0;
    right: 0;
    z-index: 1050;
    position: fixed;
    background: rgba(0,0,0,0.5);
    height: 100%;
    width: 100%;
    top: 0;
    bottom: 0;
    display: none;
    overflow-y: auto;
    align-self: center;
  }
</style>
<script type="text/javascript">
  let testTitle = '';
  let testId = null;
  let testPostId = null;
  let testQuestions = [];
  let updateTestFlag = false;

  if (window.addEventListener) {
    window.addEventListener('load', updateArray());
  }

  async function deleteTest(testId) {
    const response = await fetch(`/test/${testId}`, {
      method: 'DELETE',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
      },
    }).catch((e) => {});

    if (response) {
      location.reload();
    }
  }

  async function editTest(examId) {
    const response = await fetch(`/quiz/${examId}`, {
      method: 'GET',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
      },
    }).catch((e) => {});

    updateTestFlag = true;

    let payload = await response.json();

    testId = payload.id
    testTitle = payload.name;
    testPostId = payload.id_post;
    testQuestions = payload.questions;

    updateArray();
    renderArray();
    openModal();
  }

  function dismissModal() {
    let modal = document.getElementById('test-modal-dialog');
    eraseTest();

    modal.style.display = 'none';
  }

  function openModal() {
    let modal = document.getElementById('test-modal-dialog');

    modal.style.display = 'block';
  }

  function updateArray() {
    document.getElementById('test-items-container').innerHTML = null;

    renderArray()
  }

  function eraseTest() {
    testQuestions = [];
    testTitle = '';

    updateArray();
  }

  function addQuestion() {
    let newSimpleQuestion = {
      id: window.btoa(new Date().getTime()).replace(/=/g, ''),
      content: '',
      answers: [],
    };

    testQuestions.push(newSimpleQuestion);

    updateArray();
  }

  function eraseQuestion(questionId) {
    testQuestions = testQuestions.filter((question) => {
      return question.id !== questionId;
    });

    updateArray();
  }

  function renderArray() {
    let container = document.getElementById('test-items-container');

    if (testQuestions.length === 0) {
      container.innerHTML = `
        <div>
          <h4>No hay preguntas en este examen, agrega algunas para comenzar.</h4>
          <br />
          <br />
        </div>
      `;
    } else {
      document.getElementById('test-items-container').innerHTML = null;

      let title = document.createElement('h3');
      title.innerText = 'Titulo del examen';

      let newTestTitle = document.createElement('input');
      newTestTitle.value = testTitle;
      newTestTitle.placeholder = 'Examen sin titulo';
      newTestTitle.className = 'form-control';
      newTestTitle.onkeyup = (event) => {
        let timeout = null;

        clearTimeout(timeout);

        timeout = setTimeout(() => {
          testTitle = newTestTitle.value;
        }, 500);
      }

      container.appendChild(title);
      container.appendChild(newTestTitle);
      container.appendChild(document.createElement('br'));

      for (let x = 0, size = testQuestions.length; x < size; x += 1) {
        let newQuestionContainer = document.createElement('div');
        newQuestionContainer.id = `question-${testQuestions[x].id}`;
        newQuestionContainer.style.padding = '4%';
        newQuestionContainer.style.borderStyle = 'solid';
        newQuestionContainer.style.borderWidth = '1px';
        newQuestionContainer.style.marginBottom = '2%';
        newQuestionContainer.style.borderColor = '#e9ecef';
        newQuestionContainer.style.borderRadius = '0.3rem';

        let newQuestionTitleContainer = document.createElement('div');
        newQuestionTitleContainer.style.display = 'flex';
        newQuestionTitleContainer.style.justifyContent = 'space-between';
        newQuestionTitleContainer.style.marginBottom = '3%';

        let newQuestionTitle = document.createElement('h3');
        newQuestionTitle.innerText = `Pregunta ${x + 1}`;
        newQuestionTitle.style.flexGrow = 1;

        newQuestionTitleContainer.appendChild(newQuestionTitle);

        let newQuestionEraseButton = document.createElement('button');
        newQuestionEraseButton.className = 'btn btn-danger';
        newQuestionEraseButton.innerText = 'Eliminar pregunta';
        newQuestionEraseButton.id = testQuestions[x].id;
        newQuestionEraseButton.onclick = () => eraseQuestion(newQuestionEraseButton.id);

        newQuestionTitleContainer.appendChild(newQuestionEraseButton)

        let newQuestionTitleInput = document.createElement('input');
        newQuestionTitleInput.className = 'form-control';
        newQuestionTitleInput.placeholder = `Ingrese el contenido de la pregunta ${x + 1}`;
        newQuestionTitleInput.value = testQuestions[x].content;
        newQuestionTitleInput.style.marginBottom = '5%';
        newQuestionTitleInput.onkeyup = (event) => {
          let timeout = null;

          clearTimeout(timeout);

          timeout = setTimeout(() => {
            testQuestions[x].content = newQuestionTitleInput.value;
          }, 500);
        }

        let newQuestionAnswerContainer = document.createElement('div');
        newQuestionAnswerContainer.style.display = 'flex';
        newQuestionAnswerContainer.style.flexDirection = 'column';
        newQuestionAnswerContainer.style.marginBottom = '5%';

        let answersTitle = document.createElement('h4');
        answersTitle.innerText = 'Respuestas';

        let newQuestionAddAnswer;

        if (testQuestions[x].answers.length === 0) {
          newQuestionAddAnswer = document.createElement('button');
          newQuestionAddAnswer.className = 'btn btn-primary';
          newQuestionAddAnswer.innerText = 'Agregar respuesta';
          newQuestionAddAnswer.onclick = () => {
            testQuestions[x].answers.push({content: '', goal: false});
            updateArray();
          };

          let newQuestionAnswerCorrect = document.createElement('button');

          newQuestionAnswerCorrect.style.marginLeft = '3%';

          let newQuestionAnswer = document.createElement('input');

          let newQuestionNewAnswerContainer = document.createElement('div');
          newQuestionNewAnswerContainer.style.display = 'flex';
          newQuestionNewAnswerContainer.style.flexDirection = 'row';
          newQuestionNewAnswerContainer.style.marginBottom = '3%';

          newQuestionAnswer.className = 'form-control';
          newQuestionAnswer.value = '';
          newQuestionAnswer.id = `answer-${x}-${testQuestions[x].id}`;
          newQuestionAnswer.placeholder = `Respuesta ${1}`;
          newQuestionAnswer.onkeyup = (event) => {
          let timeout = null;

          clearTimeout(timeout);

          timeout = setTimeout(() => {
            testQuestions[x].answers[0].content = newQuestionAnswer.value;
          }, 500);
        }

          newQuestionAnswerContainer.appendChild(newQuestionNewAnswerContainer);
          newQuestionNewAnswerContainer.appendChild(newQuestionAnswer);

          testQuestions[x].answers.push({ content: newQuestionAnswer.innerText, goal: true });

          newQuestionAnswerCorrect.onclick = () => {
            if (testQuestions[x].answers[0].goal === true) {
              testQuestions[x].answers[0].goal = false; 
            } else {
              testQuestions[x].answers[0].goal = true;
            }
            
            updateArray();
          };
          
          if (!(testQuestions[x].answers[0].goal === true || testQuestions[x].answers[0].goal === 1)) {
            newQuestionAnswerCorrect.className = 'btn btn-success';
            newQuestionAnswerCorrect.innerText = 'Marcar como correcta';
          } else {
            newQuestionAnswerCorrect.className = 'btn btn-danger';
            newQuestionAnswerCorrect.innerText = 'Marcar como incorrecta';
          }

          newQuestionNewAnswerContainer.appendChild(newQuestionAnswerCorrect);
        } else {
          newQuestionAddAnswer = document.createElement('button');
          newQuestionAddAnswer.className = 'btn btn-primary';
          newQuestionAddAnswer.innerText = 'Agregar respuesta';
          newQuestionAddAnswer.onclick = () => {
            testQuestions[x].answers.push({content: '', goal: false});
            updateArray();
          };

          for (let y = 0; y < testQuestions[x].answers.length; y += 1) {
            let newQuestionAnswerCorrect = document.createElement('button');

            newQuestionAnswerCorrect.onclick = () => {
              if (testQuestions[x].answers[y].goal === true) {
                testQuestions[x].answers[y].goal = false; 
              } else {
                testQuestions[x].answers[y].goal = true;
              }
              
              updateArray();
            };

            if (!(testQuestions[x].answers[y].goal === true || testQuestions[x].answers[y].goal === 1)) {
              newQuestionAnswerCorrect.className = 'btn btn-success';
              newQuestionAnswerCorrect.innerText = 'Marcar como correcta';
            } else {
              newQuestionAnswerCorrect.className = 'btn btn-danger';
              newQuestionAnswerCorrect.innerText = 'Marcar como incorrecta';
            }

            newQuestionAnswerCorrect.style.marginLeft = '3%';

            let newQuestionNewAnswerContainer = document.createElement('div');
            newQuestionNewAnswerContainer.style.display = 'flex';
            newQuestionNewAnswerContainer.style.flexDirection = 'row';
            newQuestionNewAnswerContainer.style.marginBottom = '3%';

            let newQuestionAnswer = document.createElement('input');
            newQuestionAnswer.className = 'form-control';
            newQuestionAnswer.value = testQuestions[x].answers[y].content;
            newQuestionAnswer.id = `answer-${x}-${testQuestions[x].id}`;
            newQuestionAnswer.placeholder = `Respuesta ${y + 1}`;
            newQuestionAnswer.onkeyup = (event) => {
              let timeout = null;

              clearTimeout(timeout);

              timeout = setTimeout(() => {
                testQuestions[x].answers[y].content = newQuestionAnswer.value;
              }, 500);
            } 

            newQuestionAnswerContainer.appendChild(newQuestionNewAnswerContainer);
            newQuestionNewAnswerContainer.appendChild(newQuestionAnswer);
            newQuestionNewAnswerContainer.appendChild(newQuestionAnswerCorrect);
          }
        }

        newQuestionContainer.appendChild(newQuestionTitleContainer);
        newQuestionContainer.appendChild(newQuestionTitleInput);
        newQuestionTitleContainer.appendChild(document.createElement('br'));
        newQuestionContainer.appendChild(answersTitle);
        newQuestionContainer.appendChild(newQuestionAnswerContainer);
        newQuestionContainer.appendChild(newQuestionAddAnswer);

        container.appendChild(newQuestionContainer);
      }
    }

    console.log(testQuestions);
  }

  async function createTest() {
    const response = await fetch('/test', {
      method: 'POST',
      body: JSON.stringify({
        post_id: @json($post->id),
        _token: '{{ csrf_token() }}',
        test_name: testTitle,
        test: testQuestions,
      }), 
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).catch((e) => {});

    if (response) {
      location.reload();
    }
  }

  async function updateTest() {
    const response = await fetch(`/test/${testId}`, {
      method: 'PATCH',
      body: JSON.stringify({
        post_id: testPostId,
        _token: '{{ csrf_token() }}',
        name: testTitle,
        test: testQuestions,
      }), 
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    }).catch((e) => {});

    if (response) {
      location.reload();
    }
  }

  async function postTest() {
    console.log(testId);

    if (updateTestFlag === false) {
      await createTest();
      dismissModal();
    } else {
      await updateTest();
      dismissModal();
    }
  }
</script>
@endsection
