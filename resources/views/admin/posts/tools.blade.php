@extends('layouts.app')

@section('content')
<div class="container">
    <!-- Modal -->
    <div class="modal" tabindex="-1" role="dialog" id="post-form">
        <div class="modal-dialog" role="document" style="max-width: 700px;">
            <div class="modal-content">
                <div class="alert alert-danger" style="display:none"></div>
                <div class="modal-header">
                    <h5 class="modal-title" id="item-modal-title">Add Post</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-4">
                        <label for="post-name">Name:</label>
                        <input type="text" class="form-control" name="post-name" id="post-name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="post-category">Category:</label>
                            <select class="form-control" name="post-category" id="post-category">
                            <option selected disabled>---</option>
                                @foreach ($categories as $item)
                                    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div>Body:</div>
                            <textarea name="post-body" id="post-body" style="height: 100px; width: 100%"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button  class="btn btn-success" id="submit-post">Save</button>
                    <div id="footer-modify" style="display: none;">
                        <input type="hidden" id="item-modify-id">
                        <button  class="btn btn-danger" id="delete-item">Delete</button>
                        <button  class="btn btn-success" id="update-item">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End-->
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="form-group">
                            <label for="filtroCategorias">Filtrar por Categorías: </label>
                            <select class="form-control" id="filtroCategorias">
                            <option selected value="0">Todas las categorías</option>
                                @foreach ($categories as $item)
                                    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-primary" style="float: right" data-toggle="modal" data-target="#post-form" id="add-item-button">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                <div class="col-md-12">
                <div class="row mt-4" id="postsContainer">
                    @foreach ($posts as $item)
                        <div class="col-lg-4 col-sm-12 col-md-6 d-flex">
                            <div class="tile p-0 cursos flex-fill">
                                <div class="tile-title-w-btn justify-content-end bg-primary">
                                    <div class="btn-group">
                                        <a class="btn btn-primary" href="{{ './itemTools/' . $item->id }}">
                                        <i class="fa fa-lg fa-plus"></i></a>
                                        <!-- <a class="btn btn-primary" href="{{ './editPost/' . $item->id }}">
                                            <i class="fa fa-lg fa-edit"></i>
                                        </a> -->
                                        <a class="btn btn-primary" href="{{ './deletePost/' . $item->id }}"><i class="fa fa-lg fa-trash"></i></a>
                                    </div>
                                </div>
                                <div class="card-header bg-default flex-fill">
                                    @foreach ($categories as $itemC)
                                        @if ($itemC->id === $item->category_id)
                                            <div class="card-category mb-2">{{ $itemC->name }}</div>
                                        @endif
                                    @endforeach
                                     <h3 class="title">{{ $item->name }}</h3>
                                     

                                    
                                </div>
                                <div class="tile-body p-4" style="overflow: auto; text-overflow: ellipsis; max-height: 150px">
                                {{ $item->body }}
                                </div>
                                    <div class="tile-footer p-4">
                                        <div>
                                            <div class=" d-flex justify-content-between">
                                                <a class="btn btn-secondary" href="{{ './itemTools/' . $item->id }}">Agregar items</a>
                                                <p class="text-muted text-right mt-2">Duración: {{$item->total_time}} minutos</p>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        @endforeach

                </div>
            </div>

        </div>
        <div class="col-md-12" id="postsContainer">
        </div>
    </div>
    <script src="{{ asset('js/admin/tools.js') }}"></script>
</div>
@endsection