@extends('layouts.app')

@section('content')
<div>
  <div class="container">
    <h1>Editar usuario</h1>
    <br />
    <div class="tile">
      <div class="tile-body">
        {{ @Form::open(array('url' => 'users/admin/update/'.$user['id'])) }}
        <h4>Nombre</h4>
        <br />
        <input type="text" class="form-control" placeholder="Nombre" name="name" value="{{$user['name']}}" />
        <br />
        <br />
        <h4>E-Mail</h4>
        <input type="text" class="form-control" placeholder="E-Mail" name="email" value="{{$user['email']}}" />
        <br />
        <br />
        <h4>Rol</h4>
        {{ @Form::select('rol', array(1 => 'Administrador', 2 => 'Gerente', 3 => 'Editor'), $user['role_id'], ['class'=>'form-control']) }}
        <br />
        <br />
        <h4>Sede</h4>
        {{ @Form::select('headquarter', $headquarters->pluck('name', 'id'), $user['headquarters_id'], ['class'=>'form-control']) }}
        <br />
        <br />
        <button type="submit" class="btn btn-primary">Guardar</button>
        {{ @Form::close() }}
      </div>
    </div>
  </div>
</div>
@endsection