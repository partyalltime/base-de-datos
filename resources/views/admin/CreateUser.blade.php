@extends('layouts.app')

@section('content')
<div>
  <div class="container">
    <h1>Crear nuevo usuario</h1>
    <br />
    <div class="tile">
      <div class="tile-body">
        {{ @Form::open(array('url' => 'user', 'method' => 'post')) }}
        <h4>Nombre para el nuevo usuario</h4>
        <br />
        <input type="text" class="form-control" placeholder="Nombre" name="name" />
        <br />
        <h4>E-Mail del nuevo usuario</h4>
        <br />
        <input type="text" class="form-control" placeholder="E-Mail" name="email" />
        <br />
        <h4>Sede para el nuevo usuario</h4>
        {{ @Form::select('headquarter', $headquarters->pluck('name', 'id'), null, ['class'=>'form-control']) }}
        <br />
        <h4>Rol del nuevo usuario</h4>
        {{ @Form::select('rol', array(1 => 'Administrador', 2 => 'Gerente', 3 => 'Editor'), null, ['class'=>'form-control']) }}
        <br />
        <button type="submit" class="btn btn-primary">Crear usuario</button>
        {{ @Form::close() }}
      </div>
    </div>
  </div>
</div>
@endsection