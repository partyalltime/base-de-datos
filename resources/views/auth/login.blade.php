@extends('layouts.app')

@section('content')
<section class="material-half-bg">
      <div class="cover "></div>
    </section>
    <section class="login-content">
      <div class="login-box">
        <form class="login-form" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf  
        <h3 class="login-head"><img class="pr-2" src="{{ asset('images/logo.png') }}"/></h3>
          <div class="form-group">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" autofocus>

            @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
          </div>
          <div class="form-group mb-4">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>
          </div>
          <div class="form-group btn-container my-3">
            <button class="btn btn-primary btn-block">ENTRAR</button>
          </div>

          <div class="form-group pb-5">
            <div class="utility">
              <div class="animated-checkbox">
                <label>
                  <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><span class="label-text">Recordarme</span>
                </label>
              </div>
              <p class="semibold-text mb-2"><a href="{{ route('password.request') }}">¿Olvidaste tu contraseña?</a></p>
            </div>
          </div>
          
        </form>
        <form class="forget-form">
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Nueva contraseña</h3>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
            <input class="form-control" type="text" placeholder="Email">
          </div>
          <div class="form-group btn-container">
            <a class="btn btn-primary btn-block" href="{{ route('password.request') }}"> RESET </a>
          </div>
          <div class="form-group mt-3">
            <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
          </div>
        </form>
      </div>
    </section>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript">
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
    </script>
@endsection
