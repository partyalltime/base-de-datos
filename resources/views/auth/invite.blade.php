@extends('layouts.app')
<style type="text/css">
.btn-container{
    
}    
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="tile p-0 cursos flex-fill">
                <div class="tile-title-w-btn justify-content-end bg-primary">                   
                </div>
                <div class="card-header bg-default flex-fill">                    
                    <h4 class="title flex-grow-1 my-2">¡Invita a más personas a ser parte de Ulabs!</span></h4>
                    <div class="card-category">Vía email</div>                          
                </div>
                <div class="p-4" style="overflow: hidden; text-overflow: ellipsis; max-height: 150px">
                    <form action="{{ route('invite') }}" method="post">
                        {{ csrf_field() }}
                        <input class="form-control" type="email" placeholder="Ingresa la dirección de email" name="email" />
                        <div class="text-center"><input class="btn btn-primary mt-4" type="submit" value="Enviar"/></div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
