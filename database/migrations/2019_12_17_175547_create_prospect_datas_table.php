<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProspectDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospect_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('programing_experience',255)->nullable();
            $table->string('informatics_experience')->nullable();
            $table->text('remarkable_experience')->nullable();
            $table->string('programing_knowledge',255)->nullable();
            $table->string('informatics_knowledge',255)->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->tinyInteger('has_been_approved')->default('0');
            $table->tinyInteger('rejected')->default('0');
            $table->tinyInteger('collaboration_scholarship')->nullable();
            $table->tinyInteger('residency_scholarship')->nullable();
            $table->string('studies',255)->nullable();
            $table->string('other_studies',255)->nullable();
            $table->string('studies_center',255)->nullable();
            $table->double('average_grade',3,1)->nullable();
            $table->string('other_degrees',255)->nullable();
            $table->tinyInteger('requested_scholarship')->default('0');
            $table->integer('process_state')->default('1');
            $table->tinyInteger('interviewed')->default('0');
            $table->tinyInteger('requested_interview')->default('0');
            $table->string('interview_room',100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospect_datas');
    }
}
