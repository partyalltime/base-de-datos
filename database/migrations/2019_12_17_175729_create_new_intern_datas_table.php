<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewInternDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_intern_data', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('arrival');
            $table->tinyInteger('enrolled');
            $table->tinyInteger('pre_registered');
            $table->integer('level');
            $table->tinyInteger('granted');
            $table->tinyInteger('notified');
            $table->integer('internship');
            $table->foreign('internship')->references('id')->on('companies');
            $table->integer('prospect_id');
            $table->foreign('prospect_id')->references('id')->on('prospect_data');
            $table->integer('scholarship');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_intern_datas');
    }
}
