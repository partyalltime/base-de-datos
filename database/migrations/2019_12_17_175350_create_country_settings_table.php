<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountrySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country');
            $table->foreign('country')->references('id')->on('countries');
            $table->string('academic_level_restriccions',100);
            $table->string('academic_cycle_restriccions',100);
            $table->integer('interviewer')->unsigned();
            $table->foreign('interviewer')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_settings');
    }
}
