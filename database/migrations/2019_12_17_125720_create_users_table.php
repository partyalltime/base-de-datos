<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('email',128);
            $table->string('password',255);
            $table->string('remember_token',100)->nullable();
            $table->integer('role_id')->unsigned();
            $table->integer('responsible')->unsigned()->nullable();
            $table->string('phone',64)->nullable();
            $table->string('image_url',256)->nullable();
            $table->integer('residency_id')->unsigned()->nullable();
            $table->date('birthday')->nullable();
            $table->integer('gender')->nullable();
            $table->integer('headquarters_id')->unsigned();
            $table->string('academic_level',50)->nullable();
            $table->string('academic_cycle',50)->nullable();
            $table->tinyInteger('has_filled_att')->nullable();
            $table->string('postal_code',10)->nullable();
            $table->integer('identification_id')->unsigned()->nullable();
            $table->string('identification_number',100)->nullable();
            $table->string('address',255)->nullable();
            $table->string('city',100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
