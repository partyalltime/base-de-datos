<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function change()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles');
            //$table->foreign('responsible')->references('id')->on('users');
            $table->foreign('residency_id')->references('id')->on("countries");
            $table->foreign('headquarters_id')->references('id')->on('headquarters');
            $table->foreign('identification_id')->references('id')->on('identification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
