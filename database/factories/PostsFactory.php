<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
  return [
    'name' => $faker->words($nb = 3, $asText = true),
    'slug' => $faker->slug,
    'body' => $faker->realText($maxNbChars = 200, $indexSize = 2),
    'status' => 'public',
    'category_id' => factory(App\Category::class),
  ];
});
