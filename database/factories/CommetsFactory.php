<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\commets;
use Faker\Generator as Faker;

$factory->define(commets::class, function (Faker $faker) {
  return [
    'subject' => $faker->words($nb = 3, $asText = true),
    'body' => $faker->realText($maxNbChars = 200, $indexSize = 2),
  ];
});
