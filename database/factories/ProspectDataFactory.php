<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\prospect_data;
use Faker\Generator as Faker;

$factory->define(prospect_data::class, function (Faker $faker) {
  return [
    'process_state' => 1,
    'interviewed' => false,
    'requested_interview' => false,
  ];
});
