<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
  return [
    'name' => $faker->name,
    'email' => $faker->unique()->safeEmail,
    'password' => Hash::make(Str::random(10)),
    'remember_token' => Str::random(10),
    'role_id' => $faker->randomElement([1, 2, 3, 4 ,5]),
    'responsible' => 1,
    'gender' => $faker->randomElement([true, false]),
    'birthdate' => $faker->date(),
    'residency_id' => $faker->randomElement([1, 2, 3]),
    'phone' => $faker->phoneNumber,
    'country' => $faker->country,
    'city' => $faker->city,
    'address' => $faker->address,
  ];
});

$factory->afterCreating(App\User::class, function (User $user) {
  factory(App\ProspectData::class)->create(['user_id' => $user->id]);
});
