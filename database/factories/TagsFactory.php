<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\tags;
use Faker\Generator as Faker;

$factory->define(tags::class, function (Faker $faker) {
  return [
    'name' => $faker->word,
    'slug' => $faker->slug,
    'body' => $faker->realText($maxNbChars = 200, $indexSize = 2),
  ];
});
