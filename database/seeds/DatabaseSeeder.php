<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      DB::unprepared(file_get_contents(base_path('database/countries_201912181422.sql')));
      DB::unprepared(file_get_contents(base_path('database/headquarters_201912181422.sql')));
      DB::unprepared(file_get_contents(base_path('database/roles_201912181430.sql')));
      DB::table('users')->insert([
        [
          'name' => 'Diego Francisco',
          'email' => 'diegofranciscoh@gmail.com',
          'password' => Hash::make('123456'),
          'remember_token' => Str::random(10),
          'role_id' => 1,
          'headquarters_id' => 2,
          'residency_id' => 222,
          'responsible'=> null,
          'gender' => 0,
        ],
      ]);
    }
}
