<?php

use App\User;
use App\Prospect;
use App\Http\Controllers\Admin\ProspectController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/
//EVERY ROUTE MADE ONLY FOR ADMIN HERE
Route::group(['prefix'=> '/', 'middleware' => 'admin'], function () {

});

//EVERY ROUTE MADE ONLY FOR MANAGERS AND ADMINS HERE
Route::group(['prefix'=> '/', 'middleware' => 'manager'], function () {
    //NewIntern
    Route::get('newIntern', 'Admin\UserControl\NewInternController@getAll');
    Route::get('newIntern/view', 'Admin\UserControl\NewInternController@newIntern');
    Route::get('newIntern/create/{slug}', 'Admin\UserControl\NewInternController@viewCreate');
    Route::get('newIntern/{slug}', 'Admin\UserControl\NewInternController@get');
    Route::post('newIntern', 'Admin\UserControl\NewInternController@create');
    Route::put('newIntern/{slug}', 'Admin\UserControl\NewInternController@update');
    Route::delete('newIntern/{slug}', 'Admin\UserControl\NewInternController@delete');
    Route::get('pendingApproval/view', 'Admin\UserControl\NewInternController@get_pending');
    Route::get('interviews/view', 'Admin\UserControl\NewInternController@interviews')->name('interviews');
    Route::post('acceptInterview', 'Admin\UserControl\NewInternController@accept_interview')->name('acceptInterview');
    Route::get('interview/conference/admin/{slug}', 'Admin\UserControl\NewInternController@interview_room_admin')->name('interview/conference/admin');
    Route::post('interview/admin/video-conference/done', 'Admin\UserControl\NewInternController@interview_done')->name('interviewDone');
    //IndividualInfo
    Route::get('individualInfo', 'Admin\UserControl\IndividualInfoController@get_all');
    Route::post('individualInfo', 'Admin\UserControl\IndividualInfoController@create');
    Route::put('individualInfo/{id}', 'Admin\UserControl\IndividualInfoController@update');
    Route::delete('individualInfo/{id}', 'Admin\UserControl\IndividualInfoController@delete');
    //Prospect
    Route::get('prospect/view', 'Admin\ProspectController@prospect')->name('ProspectTable');
    Route::resource('prospect','Admin\ProspectController');
    Route::put('pendingApproval/prospect/{slug}', 'Admin\ProspectController@update');
    Route::get('lista', 'InviteController@list')->name('lista');
    Route::get('ListByResponsible', 'InviteController@listByResponsible')->name('ListByResponsible');
    //Invite
    Route::get('invite', 'InviteController@invite')->name('invite');

    Route::get('headquarter/admin', 'Admin\HeadquarterAdmin@index')->name('headquarterAdmin');
    Route::post('headquarter/admin/preferences', 'Admin\HeadquarterAdmin@updateHeadquarterPreferences')->name('updateHeadquarterPreferences'); 
});
    Route::get('headquarter','Admin\HeadquarterAdmin@getAllHeadquarter');
//EVERY ROUTE MADE ONLY FOR EDITORS AND ADMINS HERE
Route::group(['prefix'=> '/', 'middleware' => 'editor'], function () {
    Route::resource('tags', 'Admin\TagController');
    Route::resource('categories', 'Admin\CategoryController');
    Route::resource('posts', 'Admin\PostController');
    Route::get('allCategories', 'Admin\CategoryController@allCategories');
    Route::get('postTools', 'Admin\PostController@postTools')->name('postTools');
    Route::post('addPost', 'Admin\PostController@addPost')->name('addPost');
    Route::get('deletePost/{slug}', 'Admin\PostController@deletePost')->name('deletePost');
    Route::get('itemTools/{slug}', 'Admin\ItemController@itemTools')->name('itemTools');
    Route::post('addOneItem', 'Admin\ItemController@addOneItem')->name('addOneItem');
    Route::post('modifyItem', 'Admin\ItemController@modifyItem')->name('modifyItem');
    Route::post('deleteItem', 'Admin\ItemController@deleteItem')->name('deleteItem');
    Route::post('file/upload', 'ImageController@store')->name('upload');    
});
//EVERY ROUTE MADE FOR PROCCESS
Route::group(['prefix'=> '/', 'middleware' => 'proccess'], function () {
	//Index process
	Route::get('process','Process\ProcessController@index')->name('process-index');
});

Auth::routes();
Route::post('invite', 'InviteController@process')->name('process');
Route::get('/', function(){
    if (Auth::check()) return redirect('/home');

    return view("web.init");
});

Route::get('/rellenar', function(){

    return view("emails.invite");
});

Route::get('/editor', function(){

    return view("admin.posts.editor");
});
//Process
Route::post('uploadCV', 'FileUploadController@upload_file')->name('uploadCV');
Route::post('sendEmail', 'Process\ProcessDataController@triggerEmail')->name('triggerEmail');
Route::post('sendScholarshipRequestEmail', 'Process\ProcessEmailController@sendScholarshipRequestEmail')->name('sendScholarshipRequestEmail');
Route::post('setDates', 'Process\ProcessDataController@setInterviewInterval')->name('setDates');
Route::get('process/residencyInternshipForm',function(){
    return view('Process.ResidencyInternshipForm');
});
Route::get('process/intershipForm',function(){
    return view('Process.InternshipForm');
});
Route::post('sendForm', 'FormsController@fillData')->name('sendForm');
Route::post('process/nextStage', 'Process\ProcessController@nextProcessStage')->name('nextProcessStage');
//Date
Route::resource('process/date','Process\ProcessDataController');


Route::resource('/interviews', 'ScheduleController');

//End Process
Route::view('/entrar', 'auth\login');
Route::get('logout',  'Auth\LoginController@logout');
Route::get('individualInfo/{id}', 'Admin\UserControl\IndividualInfoController@get');

//Recording
Route::view('grabarEntrevista', 'Process/recording');

//Edit Profile
Route::get('editarPerfilZ/{tab}', 'UserController@userData')->name('editarPerfil');
Route::post('sendUpdate', 'UserController@update')->name('sendUpdate');

// {token} is a required parameter that will be exposed to us in the controller method
Route::get('accept/{token}', 'InviteController@accept')->name('accept');


/*Route::resource('posts', 'Admin\PostController',['except'=>['posts.update']]);
Route::put('/posts/update/{id}', 'Admin\PostController@update')->name('posts.update');*/

//Notifications
Route::post('readNotification', 'NotificationsController@readNotification')->name('readNotification');
Route::post('readAllNotif', 'NotificationsController@readAllNotif')->name('readAllNotif');

//general 
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'NotificationsController@getNotifications')->name('profile');
Route::get('postByCategory/{categoryId}', 'Admin\PostController@postByCategory');
Route::get('postByCategoryMyCourses/{categoryId}', 'Admin\PostController@postByCategoryMyCourses');
Route::get('addPostCourses/{slug}', 'HomeController@addPostCourses')->name('addPostCourses');
Route::get('minusPostCourses/{slug}', 'HomeController@minusPostCourses')->name('minusPostCourses');
Route::get('tutorialView/{slug}', 'Web\CoursesController@tutorialView')->name('tutorialView');
Route::get('myCoursesView', 'Web\CoursesController@myCoursesView')->name('myCoursesView');
Route::get('interview', 'Web\CoursesController@interview')->name('interview');
Route::get('commentsView/{slug}', 'Web\CoursesController@commentsView')->name('commentsView');
Route::get('repliesView/{slug}', 'Web\CoursesController@repliesView')->name('repliesView');
Route::post('addComment', 'Web\CoursesController@addComment')->name('addComment');
Route::post('addReply', 'Web\CoursesController@addReply')->name('addReply');
Route::post('getItems', 'Admin\ItemController@getItems')->name('getItems');
Route::post('completePostUserStatus/{slug}', 'Web\CoursesController@completePostUserStatus')->name('completePostUserStatus');
Route::get('requestScholarship', 'Process\ProcessController@startProcess')->name('requestScholarship');
Route::post('interviews/request', 'Process\ProcessController@requestInterview')->name('requestInterview');
Route::get('downloadDocument/{type}/{user_id}', 'Admin\ProspectController@downloadProspectData');

//Routes to print will here just for a while
Route::get('generate-pdf','Process\ProcessController@printSignUpForm');
Route::get('generate-pdf2/{id}','Process\ProcessController@printResidencyForm');
Route::get('/pdf', function(){

    return view("Process.print.PrintSignUpForm");
});
Route::get('/pdf2', function(){

    return view("Process.print.PrintResidencyForm");
});
//Test and Quiz Route
Route::resource('test','Test\QuizController');
Route::get('quizes/{id}','Test\QuizController@getAllQuiz');
Route::get('getQuizes/{id}','Test\QuizController@getDataQuizes');
Route::get('question/{id}','Test\QuizController@showAnswers');
Route::get('quiz/{id}','Test\QuizController@showQuiz');
Route::get('testResult/{id}', 'Test\QuizController@getResultOfQuiz');
//Prospect extra data
Route::get('prospectData/{id}','Admin\ProspectController@getProspect');
Route::get('users/admin', 'Admin\AdminController@index');
Route::delete('user/{id}', 'UserController@destroy');
Route::get('users/admin/create', 'Admin\AdminController@create');
Route::post('user', 'UserController@create');
Route::get('users/admin/update/{id}', 'Admin\AdminController@update');
Route::post('users/admin/update/{id}', 'UserController@adminUpdate');

//TestRecord
Route::resource('record','Test\TestRecordController');
